// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.client.boxes;

import com.google.appinventor.client.editor.simple.palette.ComponentHelpWidget;
import com.google.appinventor.client.editor.simple.palette.SimpleComponentDescriptor;
import com.google.appinventor.client.widgets.boxes.Box;
import com.google.gwt.user.client.ui.*;
import static com.google.appinventor.client.Ode.MESSAGES;

/**
 * Box implementation for filters panels.
 *
 */
public final class FiltersBox extends Box {

  // Singleton filters box instance
  private static final FiltersBox INSTANCE = new FiltersBox();
  private final Grid tea;
  private SimpleComponentDescriptor scd;

  /**
   * Return the filters box.
   *
   * @return  filters box
   */
  public static FiltersBox getFiltersBox() {
    return INSTANCE;
  }

  /**
   * Creates new filters box.
   */
  private FiltersBox() {
    super(MESSAGES.filtersBoxCaption(),
        200,       // height
        false,     // minimizable
        false,     // removable
        false,     // startMinimized
        false);
    tea = new Grid(7,1);
    tea.addStyleName("ode-ProjectTable");
    tea.setWidth("100%");
    tea.setCellSpacing(10);


    tea.getRowFormatter().addStyleName(0, "ode-ProjectHeaderRow");
    VerticalPanel gameModels = new VerticalPanel();
    final Label gameModelsLabel = new Label("Modèles de Jeux");
    gameModelsLabel.addStyleName("ode-FiltersLabel");
    gameModels.add(gameModelsLabel);

    HorizontalPanel panel1 = new HorizontalPanel();
      panel1.setStylePrimaryName("ode-SimplePaletteItem");
      Label label = new Label("Scénario linéaire");
      label.setHorizontalAlignment(Label.ALIGN_LEFT);
      label.addStyleName("ode-SimplePaletteItem-caption");
      panel1.add(label);
    ComponentHelpWidget helpImage1 = new ComponentHelpWidget(scd);
    panel1.add(helpImage1);
    panel1.setCellHorizontalAlignment(helpImage1, HorizontalPanel.ALIGN_RIGHT);
      panel1.setWidth("250px");

    HorizontalPanel panel2 = new HorizontalPanel();
      panel2.setStylePrimaryName("ode-SimplePaletteItem");
      Label label2 = new Label("Scénario émergent");
      label2.setHorizontalAlignment(Label.ALIGN_LEFT);
      label2.addStyleName("ode-SimplePaletteItem-caption");
      panel2.add(label2);
    ComponentHelpWidget helpImage2 = new ComponentHelpWidget(scd);
    panel2.add(helpImage2);
    panel2.setCellHorizontalAlignment(helpImage2, HorizontalPanel.ALIGN_RIGHT);
    panel2.setWidth("246px");

    HorizontalPanel panel3 = new HorizontalPanel();
      panel3.setStylePrimaryName("ode-SimplePaletteItem");
      Label label3 = new Label("Hub d'activités");
      label3.setHorizontalAlignment(Label.ALIGN_LEFT);
      label3.addStyleName("ode-SimplePaletteItem-caption");
      panel3.add(label3);
    ComponentHelpWidget helpImage3 = new ComponentHelpWidget(scd);
    panel3.add(helpImage3);
    panel3.setCellHorizontalAlignment(helpImage3, HorizontalPanel.ALIGN_RIGHT);
    panel3.setWidth("252px");

    gameModels.add(panel1);
    gameModels.add(panel2);
    gameModels.add(panel3);
    tea.setWidget(0, 0, gameModels);

    tea.getRowFormatter().addStyleName(1, "ode-ProjectHeaderRow");
    VerticalPanel gameStyles = new VerticalPanel();
    final Label gameStylesLabel = new Label("Types de Jeux");
    gameStylesLabel.addStyleName("ode-FiltersLabel");
    gameStyles.add(gameStylesLabel);

    HorizontalPanel panel4 = new HorizontalPanel();
    CheckBox checkBox1 = new CheckBox();
    panel4.add(checkBox1);
    panel4.setStylePrimaryName("ode-SimplePaletteItem");
    Label label5 = new Label("Jeu de piste");
    label5.setHorizontalAlignment(Label.ALIGN_LEFT);
    label5.addStyleName("ode-SimplePaletteItem-caption");
    panel4.add(label5);
    ComponentHelpWidget helpImage4 = new ComponentHelpWidget(scd);
    panel4.add(helpImage4);
    panel4.setCellHorizontalAlignment(helpImage4, HorizontalPanel.ALIGN_RIGHT);
    panel4.setWidth("252px");
    gameStyles.add(panel4);

    HorizontalPanel panel5 = new HorizontalPanel();
    CheckBox checkBox2 = new CheckBox();
    panel5.add(checkBox2);
    panel5.setStylePrimaryName("ode-SimplePaletteItem");
    Label label6 = new Label("Jeu de role");
    label6.setHorizontalAlignment(Label.ALIGN_LEFT);
    label6.addStyleName("ode-SimplePaletteItem-caption");
    panel5.add(label6);
    ComponentHelpWidget helpImage5 = new ComponentHelpWidget(scd);
    panel5.add(helpImage5);
    panel5.setCellHorizontalAlignment(helpImage5, HorizontalPanel.ALIGN_RIGHT);
    panel5.setWidth("254px");
    gameStyles.add(panel5);

    HorizontalPanel panel6 = new HorizontalPanel();
    CheckBox checkBox3 = new CheckBox();
    panel6.add(checkBox3);
    panel6.setStylePrimaryName("ode-SimplePaletteItem");
    Label label7 = new Label("Chasse au trésor");
    label7.setHorizontalAlignment(Label.ALIGN_LEFT);
    label7.addStyleName("ode-SimplePaletteItem-caption");
    panel6.add(label7);
    ComponentHelpWidget helpImage6 = new ComponentHelpWidget(scd);
    panel6.add(helpImage6);
    panel6.setCellHorizontalAlignment(helpImage6, HorizontalPanel.ALIGN_RIGHT);
    panel6.setWidth("240px");
    gameStyles.add(panel6);

    HorizontalPanel panel7 = new HorizontalPanel();
    CheckBox checkBox4 = new CheckBox();
    panel7.add(checkBox4);
    panel7.setStylePrimaryName("ode-SimplePaletteItem");
    Label label8 = new Label("Résolution d'énigme");
    label8.setHorizontalAlignment(Label.ALIGN_LEFT);
    label8.addStyleName("ode-SimplePaletteItem-caption");
    panel7.add(label8);
    ComponentHelpWidget helpImage7 = new ComponentHelpWidget(scd);
    panel7.add(helpImage7);
    panel7.setCellHorizontalAlignment(helpImage7, HorizontalPanel.ALIGN_RIGHT);
    panel7.setWidth("100%");
    panel7.setWidth("239px");
    gameStyles.add(panel7);

    HorizontalPanel panel8 = new HorizontalPanel();
    CheckBox checkBox5 = new CheckBox();
    panel8.add(checkBox5);
    panel8.setStylePrimaryName("ode-SimplePaletteItem");
    Label label9 = new Label("Balade interactive");
    label9.setHorizontalAlignment(Label.ALIGN_LEFT);
    label9.addStyleName("ode-SimplePaletteItem-caption");
    panel8.add(label9);
    ComponentHelpWidget helpImage8 = new ComponentHelpWidget(scd);
    panel8.add(helpImage8);
    panel8.setCellHorizontalAlignment(helpImage8, HorizontalPanel.ALIGN_RIGHT);
    panel8.setWidth("100%");
    panel8.setWidth("241px");
    gameStyles.add(panel8);

    HorizontalPanel panel9 = new HorizontalPanel();
    CheckBox checkBox6 = new CheckBox();
    panel9.add(checkBox6);
    panel9.setStylePrimaryName("ode-SimplePaletteItem");
    Label label10 = new Label("Parcours de découverte");
    label10.setHorizontalAlignment(Label.ALIGN_LEFT);
    label10.addStyleName("ode-SimplePaletteItem-caption");
    panel9.add(label10);
    ComponentHelpWidget helpImage9 = new ComponentHelpWidget(scd);
    panel9.add(helpImage9);
    panel9.setCellHorizontalAlignment(helpImage9, HorizontalPanel.ALIGN_RIGHT);
    panel9.setWidth("100%");
    panel9.setWidth("235px");
    gameStyles.add(panel9);

    tea.setWidget(1, 0, gameStyles);

    tea.getRowFormatter().addStyleName(2, "ode-ProjectHeaderRow");
    VerticalPanel useContext = new VerticalPanel();
    final Label useContextLabel = new Label("Contexte d'utilisation");
    useContextLabel.addStyleName("ode-FiltersLabel");
    useContext.add(useContextLabel);
    tea.setWidget(2, 0, useContext);

    tea.getRowFormatter().addStyleName(3, "ode-ProjectHeaderRow");
    VerticalPanel placeNature = new VerticalPanel();
    final Label placeNatureLabel = new Label("Nature de lieu");
    placeNatureLabel.addStyleName("ode-FiltersLabel");
    placeNature.add(placeNatureLabel);
    tea.setWidget(3, 0, placeNature);

    tea.getRowFormatter().addStyleName(4, "ode-ProjectHeaderRow");
    VerticalPanel onSpotActivities = new VerticalPanel();
    final Label onSpotActivitiesLabel = new Label("Activités situées");
    onSpotActivitiesLabel.addStyleName("ode-FiltersLabel");
    onSpotActivities.add(onSpotActivitiesLabel);
    tea.setWidget(4, 0, onSpotActivities);

    tea.getRowFormatter().addStyleName(5, "ode-ProjectHeaderRow");
    VerticalPanel integratedTools = new VerticalPanel();
    final Label integratedToolsLabel = new Label("Outils intégrés");
    integratedToolsLabel.addStyleName("ode-FiltersLabel");
    integratedTools.add(integratedToolsLabel);
    tea.setWidget(5, 0, integratedTools);

    tea.getRowFormatter().addStyleName(6, "ode-ProjectHeaderRow");
    VerticalPanel allGames = new VerticalPanel();
    final Label allGamesLabel = new Label("Tous les Jeux");
    allGamesLabel.addStyleName("ode-FiltersLabel");
    allGames.add(allGamesLabel);
    tea.setWidget(6, 0, allGames);

    setContent(tea);
  }
}

// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.client;

import com.google.appinventor.client.boxes.*;
import com.google.appinventor.client.editor.FileEditor;
import com.google.appinventor.client.editor.ProjectEditor;
import com.google.appinventor.client.editor.simple.SimpleEditor;
import com.google.appinventor.client.editor.youngandroid.BlocklyPanel;
import com.google.appinventor.client.editor.youngandroid.YaFormEditor;
import com.google.appinventor.client.editor.youngandroid.YaProjectEditor;
import com.google.appinventor.client.explorer.commands.AddFormCommand;
import com.google.appinventor.client.explorer.commands.ChainableCommand;
import com.google.appinventor.client.explorer.commands.DeleteFileCommand;
import com.google.appinventor.client.output.OdeLog;
import com.google.appinventor.client.tracking.Tracking;
import com.google.appinventor.client.widgets.DropDownButton.DropDownItem;
import com.google.appinventor.client.widgets.Toolbar;
import com.google.appinventor.client.widgets.properties.EditableProperty;
import com.google.appinventor.common.version.AppInventorFeatures;
import com.google.appinventor.shared.rpc.project.ProjectRootNode;
import com.google.appinventor.shared.rpc.project.youngandroid.YoungAndroidSourceNode;
// AOUS Begin
// AOUS End
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.maps.client.overlays.Animation;
import com.google.gwt.maps.client.overlays.Marker;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
//import com.google.maps.gwt.client.LatLng;
//import com.google.maps.gwt.client.Marker;
//import com.google.maps.gwt.client.MouseEvent;

import java.util.*;

import static com.google.appinventor.client.Ode.*;
import static com.google.gwt.user.client.Window.alert;

/**
 * The design toolbar houses command buttons in the Young Android Design
 * tab (for the UI designer (a.k.a, Form Editor) and Blocks Editor).
 *
 */
public class DesignToolbar extends Toolbar {

  /*
   * A Screen groups together the form editor and blocks editor for an
   * application screen. Name is the name of the screen (form) displayed
   * in the screens pull-down.
   */
  public static class Screen {
    public final String screenName;
    public final FileEditor formEditor;
    public final FileEditor blocksEditor;

    public Screen(String name, FileEditor formEditor, FileEditor blocksEditor) {
      this.screenName = name;
      this.formEditor = formEditor;
      this.blocksEditor = blocksEditor;
    }
  }

  /*
   * A project as represented in the DesignToolbar. Each project has a name
   * (as displayed in the DesignToolbar on the left), a set of named screens,
   * and an indication of which screen is currently being edited.
   */
  public static class DesignProject {
    public final String name;
    public static Map<String, Screen> screens; // screen name -> Screen
    public static Map<Integer, String> orderedScreens; // AOUS
    public String currentScreen; // name of currently displayed screen

    public DesignProject(String name, long projectId) {
      this.name = name;
      screens = Maps.newHashMap();
      orderedScreens = new LinkedHashMap();
      // Screen1 is initial screen by default
      currentScreen = YoungAndroidSourceNode.SCREEN1_FORM_NAME;
      // Let BlocklyPanel know which screen to send Yail for
      BlocklyPanel.setCurrentForm(projectId + "_" + currentScreen);
    }

    // Returns true if we added the screen (it didn't previously exist), false otherwise.
    public boolean addScreen(String name, FileEditor formEditor, FileEditor blocksEditor) {
      if (!screens.containsKey(name)) {
        screens.put(name, new Screen(name, formEditor, blocksEditor));
        orderedScreens.put(screens.size(),name);
        return true;
      } else {
        return false;
      }
    }

    public void removeScreen(String name) {
      screens.remove(name);
    }

    public void setCurrentScreen(String name) {
      currentScreen = name;
    }
  }

  private static final String WIDGET_NAME_ADDFORM = "AddForm";
  private static final String WIDGET_NAME_ADD_ACTIVITY = "AddActivity";
  private static final String WIDGET_NAME_EDIT_ACTIVITY = "EditActivity";
  private static final String WIDGET_NAME_REMOVE_ACTIVITY = "RemoveActivity";
  private static final String WIDGET_NAME_REMOVEFORM = "RemoveForm";
  private static final String WIDGET_NAME_SCREENS_DROPDOWN = "ScreensDropdown";
  private static final String WIDGET_NAME_SWITCH_TO_BLOCKS_EDITOR = "SwitchToBlocksEditor";
  private static final String WIDGET_NAME_SWITCH_TO_FORM_EDITOR = "SwitchToFormEditor";
  private final PushButton WIDGET_NAME_SWITCH_TO_STANDARD_MODE;
  private final PushButton WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE;
  private final PushButton WIDGET_NAME_SWITCH_TO_EXPERT_MODE;
  private final PushButton assistantOn;
  private final PushButton assistantOff;
  private static boolean assist = true;

  protected static final Images images = Ode.getImageBundle();

  // Switch language
  private static final String WIDGET_NAME_SWITCH_LANGUAGE = "Language";
  private static final String WIDGET_NAME_SWITCH_LANGUAGE_ENGLISH = "English";
  private static final String WIDGET_NAME_SWITCH_LANGUAGE_CHINESE_CN = "Simplified Chinese";
  private static final String WIDGET_NAME_SWITCH_LANGUAGE_SPANISH_ES = "Spanish-Spain";
  //private static final String WIDGET_NAME_SWITCH_LANGUAGE_GERMAN = "German";
  //private static final String WIDGET_NAME_SWITCH_LANGUAGE_VIETNAMESE = "Vietnamese";

  // Enum for type of view showing in the design tab
  public enum View {
    FORM,   // Form editor view
    BLOCKS  // Blocks editor view
  }
  public View currentView = View.FORM;

  public static Label projectNameLabel;
  public HTML jemInventorMotd;

  // Project currently displayed in designer
  public DesignProject currentProject;

  // Map of project id to project info for all projects we've ever shown
  // in the Designer in this session.
  public Map<Long, DesignProject> projectMap = Maps.newHashMap();

  // Stack of screens switched to from the Companion
  // We implement screen switching in the Companion by having it tell us
  // to switch screens. We then load into the companion the new Screen
  // We save where we were because the companion can have us return from
  // a screen. If we switch projects in the browser UI, we clear this
  // list of screens as we are effectively running a different application
  // on the device.
  public static LinkedList<String> pushedScreens = Lists.newLinkedList();

  private Ode ode=Ode.getInstance();
  private static RpcStatusPopup rpcStatusPopup;

  private static DialogBox dialogBox;

  /**
   * Initializes and assembles all commands into buttons in the toolbar.
   */
  public DesignToolbar() {
    super();

    projectNameLabel = new Label();
    String userName= ode.getUser().getUserEmail().substring(0,ode.getUser().getUserEmail().indexOf("@"));

    // Antoine begin
    String formula = "community";
    int userFormula = ode.getUser().getUserFormula();
    switch (userFormula) {
    case Ode.COMMUNITYPLUS:
      formula = "community+";
    break;
    case Ode.SILVER:
      formula = "silver";
    break;
    case Ode.GOLD:
      formula = "gold";
    break;
    }
    jemInventorMotd = new HTML("Bonjour <b>"+userName+"</b>. " +
        "Vous etes en mode <i>" + formula + "</i>.</br> Vous pouvez activer l'assisant afin de vous guider pour faire un scénario.");
    projectNameLabel.setStyleName("ya-ProjectName");
    HorizontalPanel toolbar = (HorizontalPanel) getWidget();
    toolbar.insert(jemInventorMotd, 0);

    // width of palette minus cellspacing/border of buttons
    toolbar.setCellWidth(jemInventorMotd, "610px");
    toolbar.setCellVerticalAlignment(jemInventorMotd, HorizontalPanel.ALIGN_MIDDLE);

    WIDGET_NAME_SWITCH_TO_STANDARD_MODE = Ode.createPushButton(images.standard(), MESSAGES.standardModeButton(), new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        new SwitchToStandardModeAction().execute();
      }
    });
    WIDGET_NAME_SWITCH_TO_STANDARD_MODE.setPixelSize(142,42);

    WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE = Ode.createPushButton(images.intermediate(), MESSAGES.intermediateModeButton(), new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        new SwitchToIntermediateModeAction().execute();
      }
    });
    WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE.setPixelSize(142,42);

    WIDGET_NAME_SWITCH_TO_EXPERT_MODE = Ode.createPushButton(images.expert(), MESSAGES.expertModeButton(), new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        new SwitchToExpertModeAction().execute();
      }
    });
    WIDGET_NAME_SWITCH_TO_EXPERT_MODE.setPixelSize(142,42);

    leftButtons.add(WIDGET_NAME_SWITCH_TO_STANDARD_MODE);
    leftButtons.add(WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE);
    leftButtons.add(WIDGET_NAME_SWITCH_TO_EXPERT_MODE);
    leftButtons.setVerticalAlignment(HorizontalPanel.ALIGN_TOP);

    addButton(new ToolbarItem(WIDGET_NAME_SWITCH_TO_FORM_EDITOR,
        MESSAGES.switchToFormEditorButton(), new SwitchToFormEditorAction()), true);
    addButton(new ToolbarItem(WIDGET_NAME_SWITCH_TO_BLOCKS_EDITOR,
        MESSAGES.switchToBlocksEditorButton(), new SwitchToBlocksEditorAction()), true);

    if (AppInventorFeatures.allowMultiScreenApplications()) {
//      addButton(new ToolbarItem(WIDGET_NAME_ADDFORM, MESSAGES.addActivityButton(),
//          new AddFormAction()), true);

      List<DropDownItem> screenItems = Lists.newArrayList();
      addDropDownButton(WIDGET_NAME_SCREENS_DROPDOWN, MESSAGES.screensButton(), screenItems);

      addButton(new ToolbarItem(WIDGET_NAME_REMOVEFORM, MESSAGES.removeActivityButton(),
          new RemoveFormAction()), true);
    }
      rightButtons.setVisible(false);  // masquer les deux lignes au dessus

    HorizontalPanel extremeRightButtons= new HorizontalPanel();
    extremeRightButtons.setSpacing(0);
    extremeRightButtons.setHorizontalAlignment(HorizontalPanel.ALIGN_RIGHT);

    /*Image imageAssistant = new Image("/images/assistant.png" + "?t=" + System.currentTimeMillis());
    imageAssistant.setSize("35px", "35px");
    imageAssistant.setStyleName("ode-Logo");
    extremeRightButtons.add(imageAssistant);
    extremeRightButtons.setCellVerticalAlignment(imageAssistant, HorizontalPanel.ALIGN_TOP);*/

    assistantOn = Ode.createPushButton(images.assistantOn(), MESSAGES.expertModeButton(), new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        changeAssist();
      }
    });
    assistantOn.setPixelSize(63,35);
    extremeRightButtons.add(assistantOn);
    // Antoine assistantOn.setVisible(false);
    extremeRightButtons.setCellVerticalAlignment(assistantOn, HorizontalPanel.ALIGN_MIDDLE);

    assistantOff = Ode.createPushButton(images.assistantOff(), MESSAGES.expertModeButton(), new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        changeAssist();
        dialogBox = new DialogBox();
        dialogBox.setStylePrimaryName("ode-DialogBox-"+Ode.getInstance().getCurrentMode());
        dialogBox.setText("Assistant");
        dialogBox.setWidth("400px");
        dialogBox.setGlassEnabled(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.center();
        VerticalPanel DialogBoxContents = new VerticalPanel();
//        HTML message = new HTML("Bonjour, pour un exemple d'utilisation, veuillez consulter cette <a href=\"https://www.youtube.com/watch?v=7ntDVX-yr-M\" onclick=\"window.open(this.href); return false;\" >vidéo</a>.");
        HTML message = new HTML(""+ JemBlocksStrings.INITIAL_FORM);
        HorizontalPanel holder = new HorizontalPanel();
        Button ok = new Button("Fermer");
        ok.addClickHandler(new ClickHandler() {
          @Override
          public void onClick(ClickEvent clickEvent) {
            dialogBox.hide();
          }
        });
        holder.add(ok);
        DialogBoxContents.add(message);
        DialogBoxContents.add(holder);
        dialogBox.setWidget(DialogBoxContents);
        dialogBox.show();
      }
    });
    assistantOff.setPixelSize(63,35);
    extremeRightButtons.add(assistantOff);
    // Antoine assistantOff.setVisible(true);

    extremeRightButtons.setCellVerticalAlignment(assistantOff, HorizontalPanel.ALIGN_MIDDLE);
    Label space = new Label("");
    VerticalPanel spaceContainer = new VerticalPanel();
    spaceContainer.add(space);
    toolbar.add(spaceContainer);
    toolbar.setCellWidth(spaceContainer, "270px");
    toolbar.add(extremeRightButtons);

    // Antoine begin
    // If the user has the free formula, automatically put assistantOn
    switch (userFormula) {
      case Ode.COMMUNITY:
        assistantOn.setVisible(assist);
        assistantOff.setVisible(!assist);
      break;
      default:
        changeAssist();
    }
    // Antoine end

    // Récupère la formule actuelle depuis la base de données
//    toggleModeButton(ode.getUser().getUserFormula());
//    ode.setCurrentMode(ode.getUser().getUserFormula());

    toggleModeButton(ode.STANDARD);
    
    Ode.getInstance().getTopToolbar().updateFileMenuButtons(0);
  }

  private void changeAssist() {
    assist = !assist;
    assistantOn.setVisible(assist);
    assistantOff.setVisible(!assist);
  }

  // AOUS begin
  private class AddFormAction implements Command {

    @Override
    public void execute() {
      Ode ode = Ode.getInstance();
//      MapBox mbx = MapBox.getMapBox();
      if (ode.screensLocked()) {
        return;                 // Don't permit this if we are locked out (saving files)
      }
      ProjectRootNode projectRootNode = ode.getCurrentYoungAndroidProjectRootNode();
      if (projectRootNode != null) {
        //  longitude=pos.lng();


//        marker.setVisible(false);

//        mbx.addClickHandler(new ClickHandler() {
//          @Override
//          public void onClick(ClickEvent event) {
//            alert("coordinates :"+pos);
//                      }
//        });

        ChainableCommand cmd = new AddFormCommand();
        cmd.startExecuteChain(Tracking.PROJECT_ACTION_ADDFORM_YA, projectRootNode);
      }
    }

  }
//  public LatLng getMarkerPos() { return pos;}
  // AOUS end

  private class RemoveFormAction implements Command {
    @Override
    public void execute() {
      Ode ode = Ode.getInstance();
      if (ode.screensLocked()) {
        return;                 // Don't permit this if we are locked out (saving files)
      }
      YoungAndroidSourceNode sourceNode = ode.getCurrentYoungAndroidSourceNode();
      if (sourceNode != null && !sourceNode.isScreen1()) {
        // DeleteFileCommand handles the whole operation, including displaying the confirmation
        // message dialog, closing the form editor and the blocks editor,
        // deleting the files in the server's storage, and deleting the
        // corresponding client-side nodes (which will ultimately trigger the
        // screen deletion in the DesignToolbar).
        final String deleteConfirmationMessage = MESSAGES.reallyDeleteForm(
            sourceNode.getFormName());

        ChainableCommand cmd = new DeleteFileCommand() {
          @Override
          protected boolean deleteConfirmation() {
            return Window.confirm(deleteConfirmationMessage);
          }
        };

        // Adam

        cmd.startExecuteChain(Tracking.PROJECT_ACTION_REMOVEFORM_YA, sourceNode);
      }
    }
  }

  private class SwitchScreenAction implements Command {
    private final long projectId;
    private final String name;  // screen name

    public SwitchScreenAction(long projectId, String screenName) {
      this.projectId = projectId;
      this.name = screenName;
    }

    @Override
    public void execute() {
      doSwitchScreen(projectId, name, currentView);
//      makeMarkerBounce(name);
    }
  }

  private void doSwitchScreen(final long projectId, final String screenName, final View view) {
    Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
      @Override
      public void execute() {
        if (Ode.getInstance().screensLocked()) { // Wait until I/O complete
          Scheduler.get().scheduleDeferred(this);
        } else {
          doSwitchScreen1(projectId, screenName, view);
        }
      }
    });
  }

  private void doSwitchScreen1(long projectId, String screenName, View view) {
    if (!projectMap.containsKey(projectId)) {
      OdeLog.wlog("DesignToolbar: no project with id " + projectId
          + ". Ignoring SwitchScreenAction.execute().");
      return;
    }
    DesignProject project = projectMap.get(projectId);
    if (currentProject != project) {
      // need to switch projects first. this will not switch screens.
      if (!switchToProject(projectId, project.name)) {
        return;
      }
    }
    String newScreenName = screenName;
    if (!currentProject.screens.containsKey(newScreenName)) {
      // Can't find the requested screen in this project. This shouldn't happen, but if it does
      // for some reason, try switching to Screen1 instead.
      OdeLog.wlog("Trying to switch to non-existent screen " + newScreenName +
          " in project " + currentProject.name + ". Trying Screen1 instead.");
      if (currentProject.screens.containsKey(YoungAndroidSourceNode.SCREEN1_FORM_NAME)) {
        newScreenName = YoungAndroidSourceNode.SCREEN1_FORM_NAME;
      } else {
        // something went seriously wrong!
        ErrorReporter.reportError("Something is wrong. Can't find Screen1 for project "
            + currentProject.name);
        return;
      }
    }
    currentView = view;
    Screen screen = currentProject.screens.get(newScreenName);
    ProjectEditor projectEditor = screen.formEditor.getProjectEditor();
    currentProject.setCurrentScreen(newScreenName);
    setDropDownButtonCaption(WIDGET_NAME_SCREENS_DROPDOWN, newScreenName);
    OdeLog.log("Setting currentScreen to " + newScreenName);
    if (currentView == View.FORM) {
      // On remets les boutons switch mode
      WIDGET_NAME_SWITCH_TO_STANDARD_MODE.setVisible(true);
      WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE.setVisible(true);

      projectEditor.selectFileEditor(screen.formEditor);
      // Fonction pour remettre la vue sur indice
      // pour éviter les bugs lors de la navigation entre composants(position indice)
      JemComponents.getInstance().returnToClue(Ode.getInstance().getCurrentFileEditor());
      YaFormEditor currentForm = (YaFormEditor)ode.getCurrentFileEditor();
      currentForm.getForm().updateNavigationButtons(currentForm);

      Ode.getInstance().getTopToolbar().updateFileMenuButtons(1);
    } else {  // must be View.BLOCKS
      projectEditor.selectFileEditor(screen.blocksEditor);
      Ode.getInstance().getTopToolbar().updateFileMenuButtons(1);
      // On cache les boutons switch mode pour éviter les bugs (à améliorer)
        WIDGET_NAME_SWITCH_TO_STANDARD_MODE.setVisible(false);
        WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE.setVisible(false);
    }
    // Inform the Blockly Panel which project/screen (aka form) we are working on
    BlocklyPanel.setCurrentForm(projectId + "_" + newScreenName);
  }

  private class SwitchToStandardModeAction implements Command {
    @Override
    public void execute() {
      if (currentProject == null) {
        OdeLog.wlog("DesignToolbar.currentProject is null. "
            + "Ignoring SwitchToFormEditorAction.execute().");
        return;
      }
      toggleModeButton(Ode.STANDARD); // Antoine
    }
  }

  private class SwitchToIntermediateModeAction implements Command {
    @Override
    public void execute() {
      switchIfFormula(Ode.INTERMEDIATE); // Antoine
    }
  }

  private class SwitchToExpertModeAction implements Command {
    @Override
    public void execute() {
      switchIfFormula(Ode.EXPERT); // Antoine
    }
  }

  // Antoine begin
  // Method which open a dialogBox if the user don't have the required formula to access INTERMEDIATE or EXPERT mode
  // else go on the correct mode
  private void switchIfFormula(int mode) {

    final OdeAsyncCallback<Void> userFormulaUpdateCallback = new OdeAsyncCallback<Void>(
            // failure message
            MESSAGES.galleryError()) {
      @Override
      public void onSuccess(Void arg0) {
        dialogBox.hide();
      }
    };

    int userFormula = ode.getUser().getUserFormula();
    switch (userFormula) {
      case Ode.COMMUNITY:
        if (assist) {
          // Proposer les autres formules par l'intermédiaire de DialogBox
          dialogBox = new DialogBox();
          dialogBox.setStylePrimaryName("ode-DialogBox-" + Ode.getInstance().getCurrentMode());
          dialogBox.setText("Changer de formule");
          dialogBox.setWidth("400px");
          dialogBox.setGlassEnabled(true);
          dialogBox.setAnimationEnabled(true);
          dialogBox.center();
          VerticalPanel DialogBoxContents = new VerticalPanel();

          Button communityPlusButton = new Button("Community+");
          communityPlusButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
              ode.getUserInfoService().storeUserFormula(Ode.COMMUNITYPLUS, userFormulaUpdateCallback);
            }
          });
          Button silverButton = new Button("Silver");
          silverButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
              ode.getUserInfoService().storeUserFormula(Ode.SILVER, userFormulaUpdateCallback);
            }
          });
          Button goldButton = new Button("Gold");
          goldButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
              ode.getUserInfoService().storeUserFormula(Ode.GOLD, userFormulaUpdateCallback);
            }
          });
  //        HTML message = new HTML("Bonjour, pour un exemple d'utilisation, veuillez consulter cette <a href=\"https://www.youtube.com/watch?v=7ntDVX-yr-M\" onclick=\"window.open(this.href); return false;\" >vidéo</a>.");
          HTML message = new HTML("" + JemBlocksStrings.FORMULA);
          DialogBoxContents.add(message);
          DialogBoxContents.add(communityPlusButton);
          DialogBoxContents.add(silverButton);
          DialogBoxContents.add(goldButton);

          HorizontalPanel holder = new HorizontalPanel();
          Button closeButton = new Button("Fermer");
          closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
              dialogBox.hide();
            }
          });
          holder.add(closeButton);
          DialogBoxContents.add(holder);
          dialogBox.setWidget(DialogBoxContents);
          dialogBox.show();
        }
        break;
      default:
        toggleModeButton(mode);
        break;
    }
  }
  // Antoine end

  private class SwitchToBlocksEditorAction implements Command {
    @Override
    public void execute() {
      if (currentProject == null) {
        OdeLog.wlog("DesignToolbar.currentProject is null. "
            + "Ignoring SwitchToBlocksEditorAction.execute().");
        return;
      }
      if (currentView != View.BLOCKS) {
        long projectId = Ode.getInstance().getCurrentYoungAndroidProjectRootNode().getProjectId();
        switchToScreen(projectId, currentProject.currentScreen, View.BLOCKS);
        toggleEditor(true);       // Gray out the blocks button and enable the designer button
        Ode.getInstance().getTopToolbar().updateFileMenuButtons(1);
      }
    }
  }

  private class SwitchToFormEditorAction implements Command {
    @Override
    public void execute() {
      if (currentProject == null) {
        OdeLog.wlog("DesignToolbar.currentProject is null. "
            + "Ignoring SwitchToFormEditorAction.execute().");
        return;
      }
      if (currentView != View.FORM) {
        long projectId = Ode.getInstance().getCurrentYoungAndroidProjectRootNode().getProjectId();
        switchToScreen(projectId, currentProject.currentScreen, View.FORM);
        toggleEditor(false);      // Gray out the Designer button and enable the blocks button
        Ode.getInstance().getTopToolbar().updateFileMenuButtons(1);
      }
    }
  }

  public void addProject(long projectId, String projectName) {
    if (!projectMap.containsKey(projectId)) {
      projectMap.put(projectId, new DesignProject(projectName, projectId));
      OdeLog.log("DesignToolbar added project " + projectName + " with id " + projectId);
    } else {
      OdeLog.wlog("DesignToolbar ignoring addProject for existing project " + projectName
          + " with id " + projectId);
    }
  }

  // Switch to an existing project. Note that this does not switch screens.
  // TODO(sharon): it might be better to throw an exception if the
  // project doesn't exist.
  private boolean switchToProject(long projectId, String projectName) {
    if (projectMap.containsKey(projectId)) {
      DesignProject project = projectMap.get(projectId);
      if (project == currentProject) {
        OdeLog.wlog("DesignToolbar: ignoring call to switchToProject for current project");
        return true;
      }
      pushedScreens.clear();    // Effectively switching applications clear stack of screens
      clearDropDownMenu(WIDGET_NAME_SCREENS_DROPDOWN);
      OdeLog.log("DesignToolbar: switching to existing project " + projectName + " with id "
          + projectId);
      currentProject = projectMap.get(projectId);
      // TODO(sharon): add screens to drop-down menu in the right order
      for (Screen screen : currentProject.screens.values()) {
        addDropDownButtonItem(WIDGET_NAME_SCREENS_DROPDOWN, new DropDownItem(screen.screenName,
            screen.screenName, new SwitchScreenAction(projectId, screen.screenName)));
      }
      projectNameLabel.setText(projectName);
    } else {
      ErrorReporter.reportError("Design toolbar doesn't know about project " + projectName +
          " with id " + projectId);
      OdeLog.wlog("Design toolbar doesn't know about project " + projectName + " with id "
          + projectId);
      return false;
    }
    return true;
  }

  /*
   * Add a screen name to the drop-down for the project with id projectId.
   * name is the form name, formEditor is the file editor for the form UI,
   * and blocksEditor is the file editor for the form's blocks.
   */
  public void addScreen(long projectId, String name, FileEditor formEditor,
                        FileEditor blocksEditor) {
    if (!projectMap.containsKey(projectId)) {
      OdeLog.wlog("DesignToolbar can't find project " + name + " with id " + projectId
          + ". Ignoring addScreen().");
      return;
    }
    DesignProject project = projectMap.get(projectId);
    if (project.addScreen(name, formEditor, blocksEditor)) {
      if (currentProject == project) {
        addDropDownButtonItem(WIDGET_NAME_SCREENS_DROPDOWN, new DropDownItem(name,
            name, new SwitchScreenAction(projectId, name)));
      }
    }
  }

  /*
   * PushScreen -- Static method called by Blockly when the Companion requests
   * That we switch to a new screen. We keep track of the Screen we were on
   * and push that onto a stack of Screens which we pop when requested by the
   * Companion.
   */
  public static boolean pushScreen(String screenName) {
    DesignToolbar designToolbar = Ode.getInstance().getDesignToolbar();
    long projectId = Ode.getInstance().getCurrentYoungAndroidProjectId();
    String currentScreen = designToolbar.currentProject.currentScreen;
    if (!designToolbar.currentProject.screens.containsKey(screenName)) // No such screen -- can happen
      return false;                                                    // because screen is user entered here.
    pushedScreens.addFirst(currentScreen);
    designToolbar.doSwitchScreen(projectId, screenName, View.BLOCKS);
    return true;
  }

  public static void popScreen() {
    DesignToolbar designToolbar = Ode.getInstance().getDesignToolbar();
    long projectId = Ode.getInstance().getCurrentYoungAndroidProjectId();
    String newScreen;
    if (pushedScreens.isEmpty()) {
      return;                   // Nothing to do really
    }
    newScreen = pushedScreens.removeFirst();
    designToolbar.doSwitchScreen(projectId, newScreen, View.BLOCKS);
  }


  // Called from Javascript when Companion is disconnected
  public static void clearScreens() {
    pushedScreens.clear();
  }


  /*
   * Switch to screen name in project projectId. Also switches projects if
   * necessary.
   */
  public void switchToScreen(long projectId, String screenName, View view) {
    doSwitchScreen(projectId, screenName, view);
    makeMarkerBounce(screenName);
  }


  /*
   * Remove screen name (if it exists) from project projectId
   */
  public void removeScreen(long projectId, String name) {
    if (!projectMap.containsKey(projectId)) {
      OdeLog.wlog("DesignToolbar can't find project " + name + " with id " + projectId
          + " Ignoring removeScreen().");
      return;
    }
    OdeLog.log("DesignToolbar: got removeScreen for project " + projectId
        + ", screen " + name);
    DesignProject project = projectMap.get(projectId);
    if (!project.screens.containsKey(name)) {
      // already removed this screen
      return;
    }
    if (currentProject == project) {
      // if removing current screen, choose a new screen to show
      if (currentProject.currentScreen.equals(name)) {
        // TODO(sharon): maybe make a better choice than screen1, but for now
        // switch to screen1 because we know it is always there
        switchToScreen(projectId, YoungAndroidSourceNode.SCREEN1_FORM_NAME, View.FORM);
      }
      removeDropDownButtonItem(WIDGET_NAME_SCREENS_DROPDOWN, name);
    }
    project.removeScreen(name);
    removeMarker(name);
    MapBox.getMapBox().draw();
    MapBox.getMapBox().setContent(MapBox.getMapBox().getMapWidget());
//    MapBox.getMapBox().getMapWidget().panTo(MapBox.getMapBox().getFirstScreenPosition());
  }


  private void toggleEditor(boolean blocks) {
    setButtonEnabled(WIDGET_NAME_SWITCH_TO_BLOCKS_EDITOR, !blocks);
    setButtonEnabled(WIDGET_NAME_SWITCH_TO_FORM_EDITOR, blocks);

    if (AppInventorFeatures.allowMultiScreenApplications()) {
      if (getCurrentProject() == null || getCurrentProject().currentScreen == "Screen1") {
        setButtonEnabled(WIDGET_NAME_REMOVEFORM, false);
      } else {
        setButtonEnabled(WIDGET_NAME_REMOVEFORM, true);
      }
    }
  }

  // Aous : Fonction OnClick Mode button
  private void toggleModeButton(int modeToSet) {
      switch (modeToSet){
      case Ode.STANDARD:
        // Si on est dans les blocs, on revient au Designer quand meme
        checkTheView();

        // Dire à ODE que l'on a changé de mode
        ode.setCurrentMode(Ode.STANDARD);
        // Masquer l'accès au blocs

        if (ode.getCurrentFileEditor() instanceof YaFormEditor) {
          YaFormEditor edi = (YaFormEditor) ode.getCurrentFileEditor();

          updateAllScreens(STANDARD);
          // Mise à jour des éléments graphiques globaux
          updateBoxStyle(Ode.STANDARD);

          //updateModePropertiesPanel
          edi.getForm().getSelectedComponent().updateSelectedComponentsProperties(edi.getDesignProperties());

          rightButtons.setVisible(false);
          //DOM.setStyleAttribute(MapBox.getMapBox().getPlusWidget().getElement(), "border", "3px solid #5DBB46");
          MapBox.getMapBox().getPlusWidget().getElement().getStyle().setProperty("border","3px solid #5DBB46");

          ode.getStructureAndAssets().setVisible(false) ;// Antoine
        }
        // Boutons latéraux
        updateButtonStyle(false,true,true);
        break;

      case Ode.INTERMEDIATE:
        checkTheView();
        ode.setCurrentMode(Ode.INTERMEDIATE);

        if (ode.getCurrentFileEditor() instanceof YaFormEditor) {
          YaFormEditor edi = (YaFormEditor) ode.getCurrentFileEditor();

          updateAllScreens(INTERMEDIATE);
          // Mise à jour des éléments graphiques globaux
          updateBoxStyle(Ode.INTERMEDIATE);

          //updateModePropertiesPanel
          edi.getForm().getSelectedComponent().updateSelectedComponentsProperties(edi.getDesignProperties());

          rightButtons.setVisible(false); // Masquer l'accès au blocs
          //DOM.setStyleAttribute(MapBox.getMapBox().getPlusWidget().getElement(), "border", "3px solid #22A0DA");
          MapBox.getMapBox().getPlusWidget().getElement().getStyle().setProperty("border","3px solid #22A0DA");

          ode.getStructureAndAssets().setVisible(true) ;// Antoine
        }
        updateButtonStyle(true,false,true);
        break;

      case Ode.EXPERT:
        ode.setCurrentMode(Ode.EXPERT);
        rightButtons.setVisible(true);   // Afficher l'accès aux blocs
        toggleEditor(false);      // Gray out the Designer button and enable the blocks button
        if (ode.getCurrentFileEditor() instanceof YaFormEditor) {
          YaFormEditor edi = (YaFormEditor) ode.getCurrentFileEditor();

          updateAllScreens(EXPERT);
          // Mise à jour des éléments graphiques globaux
          updateBoxStyle(Ode.EXPERT);

          //updateModePropertiesPanel
          edi.getForm().getSelectedComponent().updateSelectedComponentsProperties(edi.getDesignProperties());

          //DOM.setStyleAttribute(MapBox.getMapBox().getPlusWidget().getElement(), "border", "3px solid #F06626");
          MapBox.getMapBox().getPlusWidget().getElement().getStyle().setProperty("border","3px solid #F06626");

          ode.getStructureAndAssets().setVisible(true) ;// Antoine
        }
        updateButtonStyle(true,true,false);
        break;
    }

    if (AppInventorFeatures.allowMultiScreenApplications()) {
      if (getCurrentProject() == null || getCurrentProject().currentScreen == "Screen1") {
        setButtonEnabled(WIDGET_NAME_REMOVEFORM, false);
      } else {
        setButtonEnabled(WIDGET_NAME_REMOVEFORM, true);
      }
    }
  }

  private void makeMarkerBounce(String name){
    List<Marker>markers= MapBox.getMapBox().getMarkerList();
    if (markers!=null)
    {for (Marker listmarker: markers) {
      listmarker.setAnimation(Animation.STOPANIMATION);
      if (listmarker.getTitle().equals(name))
        listmarker.setAnimation(Animation.BOUNCE);
      }}
  }

  private void removeMarker(String name){
    MapBox mbx = MapBox.getMapBox();
    List<Marker>markers= mbx.getMarkerList();
    if (markers!=null)
    {for (Marker listmarker: markers) {
      if (listmarker.getTitle().equals(name))
      {  markers.remove(listmarker);
        mbx.setMarkerList(markers);}
    }}
  }

  public DesignProject getCurrentProject() {
    return currentProject;
  }

  private void checkTheView() {
    if (currentView != View.FORM) { // View.FORM == View.Blocks
      long projectId = Ode.getInstance().getCurrentYoungAndroidProjectRootNode().getProjectId();
      switchToScreen(projectId, currentProject.currentScreen, View.FORM);
      Ode.getInstance().getTopToolbar().updateFileMenuButtons(1);
      return;
    }
  }

  private void updateBoxStyle(int mode) {
    MapBox.getMapBox().setHeaderColor(mode);
    PaletteBox.getPaletteBox().setHeaderColor(mode);
    PropertiesBox.getPropertiesBox().setHeaderColor(mode);
    SourceStructureBox.getSourceStructureBox().setHeaderColor(mode);
    ViewerBox.getViewerBox().setHeaderColor(mode);
    BlockSelectorBox.getBlockSelectorBox().setHeaderColor(mode);
    AssetListBox.getAssetListBox().setHeaderColor(mode); // Antoine
  }

  private void updateButtonStyle(boolean standard,boolean intermediate,boolean expert) {

    // Activer les boutons des modes
    WIDGET_NAME_SWITCH_TO_STANDARD_MODE.setEnabled(standard);
    WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE.setEnabled(intermediate);
    WIDGET_NAME_SWITCH_TO_EXPERT_MODE.setEnabled(expert);

    // Changer le style des boutons des modes
    if (!standard && intermediate && expert){
      WIDGET_NAME_SWITCH_TO_STANDARD_MODE.setStyleName("Aous-ActiveMode-button");
      WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE.setStyleName("Aous-NonActiveMode-button");
      WIDGET_NAME_SWITCH_TO_EXPERT_MODE.setStyleName("Aous-NonActiveMode-button");
    }
    else if (!intermediate &&  standard && expert){
      WIDGET_NAME_SWITCH_TO_STANDARD_MODE.setStyleName("Aous-NonActiveMode-button");
      WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE.setStyleName("Aous-ActiveMode-button");
      WIDGET_NAME_SWITCH_TO_EXPERT_MODE.setStyleName("Aous-NonActiveMode-button");
    }
    else if (!expert && standard && intermediate){
      WIDGET_NAME_SWITCH_TO_STANDARD_MODE.setStyleName("Aous-NonActiveMode-button");
      WIDGET_NAME_SWITCH_TO_INTERMEDIATE_MODE.setStyleName("Aous-NonActiveMode-button");
      WIDGET_NAME_SWITCH_TO_EXPERT_MODE.setStyleName("Aous-ActiveMode-button");
    }
  }

  // Au chargement de la page (F5)
  private void updateAllScreens(int mode){
    for (final DesignToolbar.Screen screen : currentProject.screens.values()) {
      final YaFormEditor thisFormEditor= (YaFormEditor)screen.formEditor;
      if (thisFormEditor.isLoadComplete()) {
        // Afficher le Panel des composants invisibles
        if (mode==STANDARD){
          thisFormEditor.getNonVisibleComponentsPanel().setVisible(false);
          for (Map.Entry mapEntry : thisFormEditor.getForm().getPointsMap().entrySet())
            ((PushButton)mapEntry.getValue()).getUpFace().setImage(new Image(images.pointStandard()));
          thisFormEditor.getForm().getRemoveScreenPanel().setVisible(false);
          thisFormEditor.getForm().getAddScreenPanel().setVisible(false);
        }
        else if (mode==INTERMEDIATE){
          thisFormEditor.getNonVisibleComponentsPanel().setVisible(false);
          for (Map.Entry mapEntry : thisFormEditor.getForm().getPointsMap().entrySet())
            ((PushButton)mapEntry.getValue()).getUpFace().setImage(new Image(images.pointInter()));
          thisFormEditor.getNonVisibleComponentsPanel().setVisible(true);
          thisFormEditor.getForm().getRemoveScreenPanel().setVisible(true);
          thisFormEditor.getForm().getAddScreenPanel().setVisible(true);
        }
        else if (mode==EXPERT){
          thisFormEditor.getNonVisibleComponentsPanel().setVisible(false);
          for (Map.Entry mapEntry : thisFormEditor.getForm().getPointsMap().entrySet())
            ((PushButton)mapEntry.getValue()).getUpFace().setImage(new Image(images.pointExpert()));
          thisFormEditor.getNonVisibleComponentsPanel().setVisible(true);
          thisFormEditor.getForm().getRemoveScreenPanel().setVisible(true);
          thisFormEditor.getForm().getAddScreenPanel().setVisible(true);
        }
        // Mise à jour des éléments graphiques globaux pour chaque Screen
        thisFormEditor.getForm().updateMockFormButtonsColor(mode);
      }
    }
  }
}

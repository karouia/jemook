package com.google.appinventor.client;

/**
 * Created by akaroui on 29/12/2016.
 */

import com.google.appinventor.client.editor.youngandroid.YaFormEditor;
import com.google.gwt.user.client.Timer;

import static com.google.appinventor.client.Ode.INTERMEDIATE;
import static com.google.appinventor.client.Ode.STANDARD;

/**
 * Class used to determine whether a new feature should be visible to the user.
 *
 */
public final class JemInventorFeatures {

  public static JemInventorFeatures instance;
  public Ode ode;
  int currentMode;

  private JemInventorFeatures() {
//    Timer t = new Timer() {
//      @Override
//      public void run() {
//        currentMode=ode.getInstance().getCurrentMode();
//      }
//    };
//      t.schedule(2000);
  }

  private boolean clueContainerCreated() {
    YaFormEditor formEditor = (YaFormEditor) Ode.getInstance().getCurrentFileEditor();
    if (formEditor.componentExist("MockPoiLearningContent"))
      return true;
    else
      return false;
  }

//  public boolean showSimpleResourcesComponentsCategory() {
////    if (currentMode==STANDARD && !clueContainerCreated())
////      return false;
////    else if (currentMode==INTERMEDIATE)
////      return true;
////    else
//      return true;
//  }

  public static JemInventorFeatures getInstance(){
    return instance;
  }
}

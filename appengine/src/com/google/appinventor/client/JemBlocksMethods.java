package com.google.appinventor.client;

import com.google.appinventor.client.editor.FileEditor;
import com.google.appinventor.client.editor.youngandroid.YaBlocksEditor;

import static com.google.gwt.user.client.Window.alert;

//import java.util.regex.*;
import com.google.appinventor.client.editor.youngandroid.YaFormEditor;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;

/**
 * Created by akaroui on 19/05/2017.
 */
public class JemBlocksMethods {

//  private static Pattern pattern;
//  private static Matcher matcher;

  public static void testRegex(){
    RegExp myRegex = RegExp.compile("<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"-[0-9]+\">");
    MatchResult matcher = myRegex.exec(JemBlocksStrings.INITIAL_FORM);
    if (myRegex.test(JemBlocksStrings.INITIAL_FORM)){
      alert("j'ai trouvé component event "+matcher);
    }
  }

  /**
   * Ajouter des nouveaux blocs à ceux deja existant dans une activité
   * @param blocksEditor éditeur de l'activité actuelle
   * @param block chaine du nouveau bloc à ajouter
   */
  public static String addNewBlocks(YaBlocksEditor blocksEditor, String block){
    // récupérer les blocs existants
    String oldBlocks = blocksEditor.getBlocksArea().getBlocksContent();
    // Remplace la "fin constante" par le nouveau bloc + la fin constante
    String newBlocks =oldBlocks.replaceAll(JemBlocksStrings.BLOCKS_ENDING,block+ JemBlocksStrings.BLOCKS_ENDING);
    return newBlocks;
  }

  /**
   * Remplace le mot-clé variable par le mot correspondant
   * @param block ancienne chaine
   * @param keyWord mot-clé à remplacer
   * @param componentName nom de remplacement
   */
  public static String updateBlock(String block, String keyWord, String componentName){
    return block.replaceAll(keyWord,componentName);
  }

  /**
   * Remplace le mot-clé NEXT_FORM par le nom de l'activité suivante
   * @param thisFormName nom de l'activité actuelle
   * @param blocks_core blocs de l'activité à mettre à jour
   */
  public static String tellNextActivity(String thisFormName, String blocks_core){
    String nextActivity;

    if (thisFormName.equals("Screen1")){
      nextActivity="Activite_1";
    }
    else if (thisFormName.startsWith("Activite_")) {
      Ode ode = Ode.getInstance();
      int compteur = -2;
      for (FileEditor fileEditor : ode.getCurrentFileEditor().getProjectEditor().getOpenFileEditors()) {
        if (fileEditor instanceof YaFormEditor) {

          compteur++;
        }}
      int activityIndex = Integer.parseInt(thisFormName.substring(9));
      if (activityIndex == compteur){
        nextActivity= "Fin";
      }else
      nextActivity = "Activite_"+(activityIndex+1);
    }
    else
      nextActivity ="inconnu";
    blocks_core = tellnomparcours(blocks_core);
    return blocks_core.replaceAll("NEXT_FORM",nextActivity);
  }

  public static String formatString(String string){
        return string.replaceAll("[\r\n]+", "");
  }

  // retourne vrai si keyWord existe dans blocks
  public static boolean blocsExist(String blocks, String keyWord){
    RegExp myRegex = RegExp.compile(keyWord);
//    MatchResult matcher = myRegex.exec(JemBlocksStrings.INITIAL_FORM);
    if (myRegex.test(blocks)){
      return (true);
    }
    else{
      return (false);
    }
  }
  public static  String tellnomparcours( String blocks_core) {
    Ode ode = Ode.getInstance();
    String projectName = ode.getCurrentYoungAndroidProjectRootNode().getName();
    return blocks_core.replaceAll("nomparcours",projectName);
  }
}

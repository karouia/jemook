package com.google.appinventor.client;

/**
 * Created by akaroui on 20/05/2017.
 */
public class JemBlocksRegex {
  // Key Words
  public static final String CURRENT_FORM = "CURRENT_FORM";


  /**
   * Chaines de départ et de fin pour les blocs
   */

  public static String WEB_VIEWER_FIRST_KEY_STRING ="</field>\n" +
      "\" +\n" +
      "      \"            <field name=\\\"PROP\\\">DataUri</field>\\n\" +\n" +
      "      \"            <value name=\\\"VALUE\\\">\\n\" +\n" +
      "      \"              <block type=\\\"text\\\" id=\\\"227\\\">\\n\" +\n" +
      "      \"                <field name=\"TEXT\">WEB_LINK";
  public static String WEB_VIEWER_SECOND_KEY_STRING ="</field>\n" +
      "\" +\n" +
      "      \"            <field name=\\\"PROP\\\">DataUri</field>\\n\" +\n" +
      "      \"            <value name=\\\"VALUE\\\">\\n\" +\n" +
      "      \"              <block type=\\\"text\\\" id=\\\"227\\\">\\n\" +\n" +
      "      \"                <field name=\"TEXT\">";
  public static String BLOCKS_STARTING ="<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n";
  public static String BLOCKS_ENDING =
      "<yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  /**
   * Chaine initialement vide du corps des blocs
   */
  public static String INITIAL_BLOCKS_CORE ="";

  public static String SCORE_START_VALUE =
      "  <block type=\"global_declaration\" id=\"[0-9]+\" inline=\"false\" x=\"-[0-9]+\" y=\"-[0-9]+\">\n" +
          "    <field name=\"NAME\">score</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"controls_getStartValue\" id=\"[0-9]+\"></block>\n" +
          "    </value>\n" +
          "  </block>";

  public static String SCORE_NULL ="<block type=\"global_declaration\" id=\"4\" inline=\"false\" x=\"-3\" y=\"-306\">\n" +
      "    <field name=\"NAME\">score</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"math_number\" id=\"115\">\n" +
      "        <field name=\"NUM\">0</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>";



  /**
   * Chaine du Screen1 par défaut
   */
  public static String SCREEN1 = "<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
      "  <block type=\"component_event\" id=\"1\" x=\"-2\" y=\"-621\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"StartButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">StartButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"13\">\n" +
      "        <mutation name=\"FermeEcran\"></mutation>\n" +
      "        <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "        <next>\n" +
      "          <block type=\"controls_openAnotherScreen\" id=\"2\" inline=\"false\">\n" +
      "            <value name=\"SCREEN\">\n" +
      "              <block type=\"text\" id=\"3\">\n" +
      "                <field name=\"TEXT\">Activite_1</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"procedures_defnoreturn\" id=\"8\" x=\"539\" y=\"-618\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">FermeEcran</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_closeScreen\" id=\"9\"></block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"component_event\" id=\"4\" x=\"-2\" y=\"-502\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"QuitButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">QuitButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"controls_closeApplication\" id=\"5\"></block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  /**
   * Chaine initiale d'un écran
   * */

  public static String INITIAL_FORM =
      "<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
      "<block type=\"component_event\" id=\"1\" x=\"-714\" y=\"-325\">\n" +
      "<mutation component_type=\"Form\" instance_name=\"CURRENT_FORM\" event_name=\"Initialize\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">CURRENT_FORM</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"component_set_get\" id=\"2\" inline=\"false\">\n" +
      "<mutation component_type=\"Label\" set_or_get=\"set\" property_name=\"Text\" is_generic=\"false\" instance_name=\"PointsLabel\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">PointsLabel</field>\n" +
      "<field name=\"PROP\">Text</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"controls_getStartValue\" id=\"3\"></block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"global_declaration\" id=\"4\" inline=\"false\" x=\"-20\" y=\"-295\">\n" +
      "<field name=\"NAME\">score</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"controls_getStartValue\" id=\"5\"></block>\n" +
      "</value>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"6\" x=\"-449\" y=\"-241\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"ClueMenuButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">ClueMenuButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"7\">\n" +
      "<mutation name=\"FermeEcran\"></mutation>\n" +
      "<field name=\"PROCNAME\">FermeEcran</field>\n" +
      "<next>\n" +
      "<block type=\"controls_openAnotherScreen\" id=\"8\" inline=\"false\">\n" +
      "<value name=\"SCREEN\">\n" +
      "<block type=\"text\" id=\"9\">\n" +
      "<field name=\"TEXT\">Screen1</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"10\" x=\"-451\" y=\"-146\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"LearningContentMenuButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">LearningContentMenuButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"11\">\n" +
      "<mutation name=\"FermeEcran\"></mutation>\n" +
      "<field name=\"PROCNAME\">FermeEcran</field>\n" +
      "<next>\n" +
      "<block type=\"controls_openAnotherScreen\" id=\"12\" inline=\"false\">\n" +
      "<value name=\"SCREEN\">\n" +
      "<block type=\"text\" id=\"13\">\n" +
      "<field name=\"TEXT\">Screen1</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"procedures_defnoreturn\" id=\"14\" x=\"335\" y=\"-116\">\n" +
      "<mutation></mutation>\n" +
      "<field name=\"NAME\">displayClue</field>\n" +
      "<statement name=\"STACK\">\n" +
      "<block type=\"component_set_get\" id=\"15\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"16\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"17\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"18\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"19\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"20\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"21\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"22\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"23\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"24\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"25\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"26\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"27\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"28\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"29\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"30\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"31\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"32\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"33\" x=\"-449\" y=\"-49\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"TaskMenuButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TaskMenuButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"34\">\n" +
      "<mutation name=\"FermeEcran\"></mutation>\n" +
      "<field name=\"PROCNAME\">FermeEcran</field>\n" +
      "<next>\n" +
      "<block type=\"controls_openAnotherScreen\" id=\"35\" inline=\"false\">\n" +
      "<value name=\"SCREEN\">\n" +
      "<block type=\"text\" id=\"36\">\n" +
      "<field name=\"TEXT\">Screen1</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"37\" x=\"-9\" y=\"-52\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"LearningContentPreviousButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">LearningContentPreviousButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"38\">\n" +
      "<mutation name=\"displayClue\"></mutation>\n" +
      "<field name=\"PROCNAME\">displayClue</field>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"83\" x=\"-9\" y=\"17\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"LearningContentNextButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">LearningContentNextButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"84\">\n" +
      "<mutation name=\"displayTask\"></mutation>\n" +
      "<field name=\"PROCNAME\">displayTask</field>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"39\" x=\"-7\" y=\"89\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"TaskPreviousButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TaskPreviousButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"40\">\n" +
      "<mutation name=\"displayLearningContent\"></mutation>\n" +
      "<field name=\"PROCNAME\">displayLearningContent</field>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"procedures_defnoreturn\" id=\"41\" x=\"334\" y=\"165\">\n" +
      "<mutation></mutation>\n" +
      "<field name=\"NAME\">displayLearningContent</field>\n" +
      "<statement name=\"STACK\">\n" +
      "<block type=\"component_set_get\" id=\"42\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"43\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"44\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"45\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"46\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"47\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"48\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"49\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"50\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"51\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"52\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"53\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"54\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"55\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"56\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"57\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"58\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"59\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"procedures_defnoreturn\" id=\"60\" x=\"336\" y=\"444\">\n" +
      "<mutation></mutation>\n" +
      "<field name=\"NAME\">displayTask</field>\n" +
      "<statement name=\"STACK\">\n" +
      "<block type=\"component_set_get\" id=\"61\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"62\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"63\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"64\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"65\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"66\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"67\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"68\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"69\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"70\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"71\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"72\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"73\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"74\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"75\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"76\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"77\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"78\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"procedures_defnoreturn\" id=\"79\" x=\"333\" y=\"741\">\n" +
      "<mutation></mutation>\n" +
      "<field name=\"NAME\">FermeEcran</field>\n" +
      "<statement name=\"STACK\">\n" +
      "<block type=\"controls_closeScreen\" id=\"80\"></block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  public static String SOUND_COMPONENT = "  <block type=\"component_event\" id=\"60\" x=\"-10\" y=\"90\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"SoundButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">SoundButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"component_method\" id=\"61\">\n" +
      "        <mutation component_type=\"Sound\" method_name=\"Play\" is_generic=\"false\" instance_name=\"Son1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">HPA_98</field>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n";

  public static String QCM_COMPONENT =  "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>"+
          "(\\n|.)*"+
      "            <value name=\"ARG2\">\n" +
      "              <block type=\"text\" id=\"[0-9]+\">\n" +
      "                <field name=\"TEXT\">Ok</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n";


  public static String FAIL_MSG =
      "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">.*</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" ;

  public static String SUCCESS_MSG =
      "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"[0-9]+\">\n" +
      "            <field name=\"TEXT\">.*</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"[0-9]+\">\n" +
      "            <field name=\"TEXT\">Bonne réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"[0-9]+\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG3\">\n" +
      "          <block type=\"text\" id=\"[0-9]+\">\n" +
      "            <field name=\"TEXT\">Retour</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG4\">\n" +
      "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
      "            <field name=\"BOOL\">FALSE</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n";

  public static String OPEN_QUESTION_COMPONENT =
      "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"calculer_Score\"></mutation>\n" +
          "        <field name=\"PROCNAME\">calculer_Score</field>\n" +
          "        <next>\n" +
          "          <block type=\"lexical_variable_set\" id=\"[0-9]+\" inline=\"false\">\n" +
          "            <field name=\"VAR\">global reponses</field>\n" +
          "            <value name=\"VALUE\">\n" +
          "              <block type=\"math_add\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                <mutation items=\"[0-9]+\"></mutation>\n" +
          "                <value name=\"NUM0\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                    <field name=\"VAR\">global reponses</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"NUM1\">\n" +
          "                  <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                    <field name=\"NUM\">[0-9]+</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"[0-9]+\" inline=\"false\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <field name=\"NAME\">reponses</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "        <field name=\"NUM\">[0-9]+</field>\n" +
          "      </block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Essaies encore de trouver la réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">.*</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Bonne réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">calculer_Score</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation elseif=\"[0-9]+\"></mutation>\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                <field name=\"NUM\">[0-9]+</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "            <mutation else=\"[0-9]+\"></mutation>\n" +
          "            <value name=\"IF[0-9]+\">\n" +
          "              <block type=\"text_contains\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO[0-9]+\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                    <mutation items=\"[0-9]+\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                        <field name=\"NUM\">[0-9]+</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                <field name=\"NUM\">[0-9]+</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "            <mutation else=\"[0-9]+\"></mutation>\n" +
          "            <value name=\"IF[0-9]+\">\n" +
          "              <block type=\"text_contains\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO[0-9]+\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                    <mutation items=\"2\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                        <field name=\"NUM\">[0-9]+</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                <field name=\"NUM\">[0-9]+</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "            <mutation else=\"[0-9]+\"></mutation>\n" +
          "            <value name=\"IF[0-9]+\">\n" +
          "              <block type=\"text_contains\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO[0-9]+\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                    <mutation items=\"[0-9]+\"></mutation>\n" +
          "                    <value name=\"NUM[0-9]+\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM[0-9]+\">\n" +
          "                      <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                        <field name=\"NUM\">[0-9]+</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                <field name=\"NUM\">[0-9]+</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"[0-9]+\">\n" +
          "                    <field name=\"TEXT\">Activite_[0-9]+</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"logic_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"[0-9]+\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"[0-9]+\">\n" +
          "                    <field name=\"TEXT\">Activite_[0-9]+</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n"
 ;

  public static String WEB_VIEWER_COMPONENT= "<block type=\"component_event\" id=\"120\" x=\"-705\" y=\"88\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"BUTTON_NAME\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">BUTTON_NAME</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"component_set_get\" id=\"148\" inline=\"false\">\n" +
      "        <mutation component_type=\"ActivityStarter\" set_or_get=\"set\" property_name=\"Action\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "        <field name=\"PROP\">Action</field>\n" +
      "        <value name=\"VALUE\">\n" +
      "          <block type=\"text\" id=\"226\">\n" +
      "            <field name=\"TEXT\">android.intent.action.VIEW</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <next>\n" +
      "          <block type=\"component_set_get\" id=\"176\" inline=\"false\">\n" +
      "            <mutation component_type=\"ActivityStarter\" set_or_get=\"set\" property_name=\"DataUri\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "            <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "            <field name=\"PROP\">DataUri</field>\n" +
      "            <value name=\"VALUE\">\n" +
      "              <block type=\"text\" id=\"227\">\n" +
      "                <field name=\"TEXT\">WEB_LINK</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <next>\n" +
      "              <block type=\"component_method\" id=\"211\">\n" +
      "                <mutation component_type=\"ActivityStarter\" method_name=\"StartActivity\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "                <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>";

  public static String CLUE_NEXT_MANUAL = // AA
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"-[0-9]+\">\n" +
                  "  <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
                  "(\\n|.)*" +
                  "    </block>\n" +
                  "  </statement>\n" +
                  "</block>";

  public static String GEOLOCATION_POSITION_UPDATE = // AA
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"-[0-9]+\">\n"
                  +
                  "    <mutation component_type=\"LocationSensor\" instance_name=\"Capteur_position1\" event_name=\"LocationChanged\"></mutation>\n" +
                  "    <field name=\"COMPONENT_SELECTOR\">Capteur_position1</field>\n" +
                  "(\\n|.)*" +
                 " <statement name=\"DO0\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"16\">\n" +
          "            <mutation name=\"displayLearningContent\"></mutation>\n" +
          "            <field name=\"PROCNAME\">displayLearningContent</field>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>";

  public static String GEOLOCATION_CLUE_NEXT_BUTTON = // Colin
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
                  "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
                  "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
                  "    <statement name=\"DO\">\n" +
                  "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n"  +
                  "(\\n|.)*" +

          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n";

  public static String QR_CODE_CLUE_NEXT_BUTTON = // Colin
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"-[0-9]+\">\n" +
                  "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
                  "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
                  "    <statement name=\"DO\">\n"  +
                  "(\\n|.)*" +
                  "                    <field name=\"COMPONENT_SELECTOR\">Scanneur_code_à_barre1</field>\n" +
                  "                </block>\n" +
                  "            </statement>\n" +
                  "        </block>\n" +
                  "    </statement>\n" +
                  "</block>\n";

  public static String QR_CODE_SCANNER = // Colin
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
                  "    <mutation component_type=\"BarcodeScanner\" instance_name=\"Scanneur_code_à_barre1\" event_name=\"AfterScan\"></mutation>\n" +
                  "(\\n|.)*" +

          "                            <field name=\"TEXT\">OK</field>\n" +
          "                        </block>\n" +
          "                    </value>\n" +
          "                </block>\n" +
          "            </statement>\n" +
          "        </block>\n" +
          "    </statement>\n" +
          "</block>";

  public static String QR_CODE_DECLARATION_1 = // Colin
          "<block type=\"global_declaration\" id=\"[0-9]+\" inline=\"false\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
                  "    <field name=\"NAME\">qrcodeScanne</field>\n"+
                  "(\\n|.)*" +
                  "            <field name=\"BOOL\">FALSE</field>\n" +
                  "        </block>\n" +
                  "    </value>\n" +
                  "</block>";

  public static String QR_CODE_DECLARATION_2 = // Colin
          "<block type=\"global_declaration\" id=\"[0-9]+\" inline=\"false\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
                  "    <field name=\"NAME\">codeATrouver</field>\n" +
                  "(\\n|.)*" +
                  "            <field name=\"TEXT\">bonCode</field>\n" +
                  "        </block>\n" +
                  "    </value>\n" +
                  "</block>\n";

  public static String QR_CODE_ERROR = // Colin
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
                  "    <mutation component_type=\"Form\" instance_name=" +
                  "(\\n|.)*" +
                  "                <block type=\"text\" id=\"[0-9]+\">\n" +
                  "                    <field name=\"TEXT\">OK</field>\n" +
                  "                </block>\n" +
                  "            </value>\n" +
                  "        </block>\n" +
                  "    </statement>\n" +
                  "</block>\n";


  public static String TEST =
      "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"CameraButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">CameraButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\">\n" +
          "        <mutation component_type=\"Camera\" method_name=\"TakePicture\" is_generic=\"false\" instance_name=\"Prise_d_image1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Camera\" instance_name=\"Prise_d_image1\" event_name=\"AfterPicture\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"bonneReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Veuillez prendre la photo d'abord !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Erreur</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Vous allez passer à l'activité suivante</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Bravo !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"[0-9]+\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"[0-9]+\">\n" +
          "                    <field name=\"TEXT\">Activite_6</field>\n" +
          "                  </block>\n" +
          "                </value>\n"
      ;
  public static String TAKE_PICTURE_COMPONENT =
      "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"CameraButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">CameraButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\">\n" +
          "        <mutation component_type=\"Camera\" method_name=\"TakePicture\" is_generic=\"false\" instance_name=\"Prise_d_image1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Camera\" instance_name=\"Prise_d_image1\" event_name=\"AfterPicture\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"bonneReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Veuillez prendre la photo d'abord !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Erreur</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Vous allez passer à l'activité suivante</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Bravo !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"[0-9]+\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"[0-9]+\">\n" +
          "                    <field name=\"TEXT\">Activite_[0-9]+</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" ;

  public static String OPEN_QUESTION_COMP = "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"-[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>"+
          "(\\n|.)*" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>";

  public static String QCM_COMP = /*"<block type=\"global_declaration\" id=\"35\" inline=\"false\" x=\"-20\" y=\"-259\">\n" +
      "    <field name=\"NAME\">coché</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"logic_false\" id=\"36\">\n" +
      "        <field name=\"BOOL\">FALSE</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"17\" x=\"-1620\" y=\"-350\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">bonneReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"18\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"19\">\n" +
      "            <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"20\">\n" +
      "            <field name=\"TEXT\">Bonne réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"21\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"22\">\n" +
      "            <mutation name=\"FermeEcran\"></mutation>\n" +
      "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "            <next>\n" +
      "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"23\" inline=\"false\">\n" +
      "                <value name=\"SCREENNAME\">\n" +
      "                  <block type=\"text\" id=\"24\">\n" +
      "                    <field name=\"TEXT\">Activite_4</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"STARTVALUE\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"25\">\n" +
      "                    <field name=\"VAR\">global score</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"global_declaration\" id=\"37\" inline=\"false\" x=\"-23\" y=\"-221\">\n" +
      "    <field name=\"NAME\">QCM</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"lists_create_with\" id=\"38\">\n" +
      "        <mutation items=\"0\"></mutation>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"global_declaration\" id=\"39\" inline=\"false\" x=\"-30\" y=\"-186\">\n" +
      "    <field name=\"NAME\">reponses</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"math_number\" id=\"40\">\n" +
      "        <field name=\"NUM\">0</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"45\" x=\"-1599\" y=\"-65\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"46\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"47\">\n" +
      "            <field name=\"TEXT\">Tu es sur ? Essaies encore de trouver la réponse.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"48\">\n" +
      "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"49\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"50\" x=\"-9\" y=\"-118\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"51\">\n" +
      "        <mutation name=\"displayLearningContent\"></mutation>\n" +
      "        <field name=\"PROCNAME\">displayLearningContent</field>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"71\" x=\"-797\" y=\"-33\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"72\">\n" +
      "        <mutation name=\"vérifier_coché\"></mutation>\n" +
      "        <field name=\"PROCNAME\">vérifier_coché</field>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"73\">\n" +
      "            <mutation name=\"calculer_Score\"></mutation>\n" +
      "            <field name=\"PROCNAME\">calculer_Score</field>\n" +
      "            <next>\n" +
      "              <block type=\"lexical_variable_set\" id=\"74\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global coché</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"logic_boolean\" id=\"75\">\n" +
      "                    <field name=\"BOOL\">FALSE</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"lexical_variable_set\" id=\"76\" inline=\"false\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                    <value name=\"VALUE\">\n" +
      "                      <block type=\"math_add\" id=\"77\" inline=\"true\">\n" +
      "                        <mutation items=\"2\"></mutation>\n" +
      "                        <value name=\"NUM0\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"78\">\n" +
      "                            <field name=\"VAR\">global reponses</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <value name=\"NUM1\">\n" +
      "                          <block type=\"math_number\" id=\"79\">\n" +
      "                            <field name=\"NUM\">1</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"88\" x=\"-1036\" y=\"162\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">vérifier_coché</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_forEach\" id=\"89\" inline=\"false\">\n" +
      "        <field name=\"VAR\">élément</field>\n" +
      "        <value name=\"LIST\">\n" +
      "          <block type=\"lexical_variable_get\" id=\"90\">\n" +
      "            <field name=\"VAR\">global QCM</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO\">\n" +
      "          <block type=\"controls_if\" id=\"91\" inline=\"false\">\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"component_set_get\" id=\"92\" inline=\"false\">\n" +
      "                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                <field name=\"PROP\">Checked</field>\n" +
      "                <value name=\"COMPONENT\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"93\">\n" +
      "                    <field name=\"VAR\">élément</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"94\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global coché</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"logic_boolean\" id=\"95\">\n" +
      "                    <field name=\"BOOL\">TRUE</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"122\" x=\"-1304\" y=\"416\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">calculer_Score</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_if\" id=\"123\" inline=\"false\">\n" +
      "        <mutation else=\"1\"></mutation>\n" +
      "        <value name=\"IF0\">\n" +
      "          <block type=\"lexical_variable_get\" id=\"124\">\n" +
      "            <field name=\"VAR\">global coché</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO0\">\n" +
      "          <block type=\"controls_if\" id=\"125\" inline=\"false\">\n" +
      "            <mutation elseif=\"3\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"math_compare\" id=\"126\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"127\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"128\">\n" +
      "                    <field name=\"NUM\">0</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"controls_forEach\" id=\"129\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"130\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"131\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"132\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"133\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"134\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"135\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"136\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"137\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"138\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"139\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"140\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"141\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"142\">\n" +
      "                                    <field name=\"NUM\">50</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"143\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"144\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF1\">\n" +
      "              <block type=\"math_compare\" id=\"145\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"146\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"147\">\n" +
      "                    <field name=\"NUM\">1</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO1\">\n" +
      "              <block type=\"controls_forEach\" id=\"148\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"149\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"150\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"151\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"152\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"153\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"154\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"155\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"156\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"157\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"158\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"159\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"160\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"161\">\n" +
      "                                    <field name=\"NUM\">30</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"162\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"163\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF2\">\n" +
      "              <block type=\"math_compare\" id=\"164\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"165\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"166\">\n" +
      "                    <field name=\"NUM\">2</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO2\">\n" +
      "              <block type=\"controls_forEach\" id=\"167\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"168\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"169\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"170\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"171\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"172\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"173\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"174\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"175\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"176\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"177\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"178\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"179\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"180\">\n" +
      "                                    <field name=\"NUM\">10</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"181\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"182\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF3\">\n" +
      "              <block type=\"math_compare\" id=\"183\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"184\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"185\">\n" +
      "                    <field name=\"NUM\">3</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO3\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"186\">\n" +
      "                <mutation name=\"FermeEcran\"></mutation>\n" +
      "                <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "                <next>\n" +
      "                  <block type=\"controls_openAnotherScreenWithStartValue\" id=\"187\" inline=\"false\">\n" +
      "                    <value name=\"SCREENNAME\">\n" +
      "                      <block type=\"text\" id=\"188\">\n" +
      "                        <field name=\"TEXT\">Activite_4</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"STARTVALUE\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"189\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <statement name=\"ELSE\">\n" +
      "          <block type=\"component_method\" id=\"190\" inline=\"false\">\n" +
      "            <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "            <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "            <value name=\"ARG0\">\n" +
      "              <block type=\"text\" id=\"191\">\n" +
      "                <field name=\"TEXT\">Il faut choisir une réponse pour passer à l'activité suivante !</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"ARG1\">\n" +
      "              <block type=\"text\" id=\"192\">\n" +
      "                <field name=\"TEXT\">Information</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"ARG2\">\n" +
      "              <block type=\"text\" id=\"193\">\n" +
      "                <field name=\"TEXT\">Ok</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>"; */
  "<xml xmlns=\"http://www.w3.org/1999/xhtml\">\r\n\r\n<block type=\"global_declaration\" id=\"2\" inline=\"false\" x=\"-335\" y=\"113\">\r\n<field name=\"NAME\">QCM</field>\r\n<value name=\"VALUE\">\r\n<block type=\"lists_create_with\" id=\"3\">\r\n<mutation items=\"0\"></mutation>\r\n</block>\r\n</value>\r\n</block>\r\n\r\n<block type=\"global_declaration\" id=\"5\" inline=\"false\" x=\"-337\" y=\"140\">\r\n<field name=\"NAME\">coch\u00E9</field>\r\n<value name=\"VALUE\">\r\n<block type=\"logic_false\" id=\"6\">\r\n<field name=\"BOOL\">FALSE</field>\r\n</block>\r\n</value>\r\n</block>\r\n\r\n<block type=\"controls_if\" id=\"9\" inline=\"false\" x=\"857\" y=\"187\"></block>\r\n<block type=\"component_event\" id=\"10\" x=\"-924\" y=\"402\">\r\n<mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\r\n<statement name=\"DO\">\r\n<block type=\"lexical_variable_set\" id=\"11\" inline=\"false\">\r\n<field name=\"VAR\">global QCM</field>\r\n<value name=\"VALUE\">\r\n<block type=\"lists_create_with\" id=\"12\" inline=\"false\">\r\n<mutation items=\"4\"></mutation>\r\n<value name=\"ADD0\">\r\n<block type=\"component_component_block\" id=\"13\">\r\n<mutation component_type=\"CheckBox\" instance_name=\"Case_\u00E0_cocher1\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Case_\u00E0_cocher1</field>\r\n</block>\r\n</value>\r\n<value name=\"ADD1\">\r\n<block type=\"component_component_block\" id=\"14\">\r\n<mutation component_type=\"CheckBox\" instance_name=\"Case_\u00E0_cocher2\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Case_\u00E0_cocher2</field>\r\n</block>\r\n</value>\r\n<value name=\"ADD2\">\r\n<block type=\"component_component_block\" id=\"15\">\r\n<mutation component_type=\"CheckBox\" instance_name=\"Case_\u00E0_cocher3\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Case_\u00E0_cocher3</field>\r\n</block>\r\n</value>\r\n<value name=\"ADD3\">\r\n<block type=\"component_component_block\" id=\"16\">\r\n<mutation component_type=\"CheckBox\" instance_name=\"Case_\u00E0_cocher4\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Case_\u00E0_cocher4</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<next>\r\n<block type=\"procedures_callnoreturn\" id=\"17\">\r\n<mutation name=\"v\u00E9rifier_coch\u00E9\"></mutation>\r\n<field name=\"PROCNAME\">v\u00E9rifier_coch\u00E9</field>\r\n<next>\r\n<block type=\"procedures_callnoreturn\" id=\"18\">\r\n<mutation name=\"calculer_Score\"></mutation>\r\n<field name=\"PROCNAME\">calculer_Score</field>\r\n<next>\r\n<block type=\"lexical_variable_set\" id=\"19\" inline=\"false\">\r\n<field name=\"VAR\">global coch\u00E9</field>\r\n<value name=\"VALUE\">\r\n<block type=\"logic_boolean\" id=\"20\">\r\n<field name=\"BOOL\">FALSE</field>\r\n</block>\r\n</value>\r\n<next>\r\n<block type=\"lexical_variable_set\" id=\"21\" inline=\"false\">\r\n<field name=\"VAR\">global reponses</field>\r\n<value name=\"VALUE\">\r\n<block type=\"math_add\" id=\"22\" inline=\"true\">\r\n<mutation items=\"2\"></mutation>\r\n<value name=\"NUM0\">\r\n<block type=\"lexical_variable_get\" id=\"23\">\r\n<field name=\"VAR\">global reponses</field>\r\n</block>\r\n</value>\r\n<value name=\"NUM1\">\r\n<block type=\"math_number\" id=\"24\">\r\n<field name=\"NUM\">1</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n</block>\r\n</next>\r\n</block>\r\n</next>\r\n</block>\r\n</next>\r\n</block>\r\n</next>\r\n</block>\r\n</statement>\r\n</block>\r\n<block type=\"component_event\" id=\"25\" x=\"-295\" y=\"393\">\r\n<mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\r\n<statement name=\"DO\">\r\n<block type=\"controls_if\" id=\"26\" inline=\"false\">\r\n<value name=\"IF0\">\r\n<block type=\"logic_compare\" id=\"27\" inline=\"true\">\r\n<field name=\"OP\">EQ</field>\r\n<value name=\"A\">\r\n<block type=\"lexical_variable_get\" id=\"28\">\r\n<mutation>\r\n<eventparam name=\"choice\"></eventparam>\r\n</mutation>\r\n<field name=\"VAR\">Choix</field>\r\n</block>\r\n</value>\r\n<value name=\"B\">\r\n<block type=\"text\" id=\"29\">\r\n<field name=\"TEXT\">Ok</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"procedures_callnoreturn\" id=\"30\">\r\n<mutation name=\"Passage_\u00E0_la_suite\"></mutation>\r\n<field name=\"PROCNAME\">Passage_\u00E0_la_suite</field>\r\n<next>\r\n<block type=\"procedures_callnoreturn\" id=\"31\">\r\n<mutation name=\"FermeEcran\"></mutation>\r\n<field name=\"PROCNAME\">FermeEcran</field>\r\n<next>\r\n<block type=\"controls_openAnotherScreenWithStartValue\" id=\"32\" inline=\"false\">\r\n<value name=\"SCREENNAME\">\r\n<block type=\"text\" id=\"33\">\r\n<field name=\"TEXT\">Activite_4</field>\r\n</block>\r\n</value>\r\n<value name=\"STARTVALUE\">\r\n<block type=\"lexical_variable_get\" id=\"34\">\r\n<field name=\"VAR\">global User</field>\r\n</block>\r\n</value>\r\n</block>\r\n</next>\r\n</block>\r\n</next>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n<block type=\"procedures_defnoreturn\" id=\"35\" x=\"-293\" y=\"612\">\r\n<mutation></mutation>\r\n<field name=\"NAME\">v\u00E9rifier_coch\u00E9</field>\r\n<statement name=\"STACK\">\r\n<block type=\"controls_forEach\" id=\"36\" inline=\"false\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n<value name=\"LIST\">\r\n<block type=\"lexical_variable_get\" id=\"37\">\r\n<field name=\"VAR\">global QCM</field>\r\n</block>\r\n</value>\r\n<statement name=\"DO\">\r\n<block type=\"controls_if\" id=\"38\" inline=\"false\">\r\n<value name=\"IF0\">\r\n<block type=\"component_set_get\" id=\"39\" inline=\"false\">\r\n<mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\r\n<field name=\"PROP\">Checked</field>\r\n<value name=\"COMPONENT\">\r\n<block type=\"lexical_variable_get\" id=\"40\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"lexical_variable_set\" id=\"41\" inline=\"false\">\r\n<field name=\"VAR\">global coch\u00E9</field>\r\n<value name=\"VALUE\">\r\n<block type=\"logic_boolean\" id=\"42\">\r\n<field name=\"BOOL\">TRUE</field>\r\n</block>\r\n</value>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n<block type=\"procedures_defnoreturn\" id=\"43\" x=\"-311\" y=\"791\">\r\n<mutation></mutation>\r\n<field name=\"NAME\">calculer_Score</field>\r\n<statement name=\"STACK\">\r\n<block type=\"controls_if\" id=\"44\" inline=\"false\">\r\n<mutation else=\"1\"></mutation>\r\n<value name=\"IF0\">\r\n<block type=\"lexical_variable_get\" id=\"45\">\r\n<field name=\"VAR\">global coch\u00E9</field>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"controls_if\" id=\"46\" inline=\"false\">\r\n<mutation elseif=\"3\"></mutation>\r\n<value name=\"IF0\">\r\n<block type=\"math_compare\" id=\"47\" inline=\"true\">\r\n<field name=\"OP\">EQ</field>\r\n<value name=\"A\">\r\n<block type=\"lexical_variable_get\" id=\"48\">\r\n<field name=\"VAR\">global reponses</field>\r\n</block>\r\n</value>\r\n<value name=\"B\">\r\n<block type=\"math_number\" id=\"49\">\r\n<field name=\"NUM\">0</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"controls_forEach\" id=\"50\" inline=\"false\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n<value name=\"LIST\">\r\n<block type=\"lexical_variable_get\" id=\"51\">\r\n<field name=\"VAR\">global QCM</field>\r\n</block>\r\n</value>\r\n<statement name=\"DO\">\r\n<block type=\"controls_if\" id=\"52\" inline=\"false\">\r\n<value name=\"IF0\">\r\n<block type=\"component_set_get\" id=\"53\" inline=\"false\">\r\n<mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\r\n<field name=\"PROP\">Checked</field>\r\n \r\n <value name=\"COMPONENT\">\r\n<block type=\"lexical_variable_get\" id=\"54\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"controls_if\" id=\"55\" inline=\"false\">\r\n<mutation else=\"1\"></mutation>\r\n<value name=\"IF0\">\r\n<block type=\"logic_compare\" id=\"56\" inline=\"true\">\r\n<field name=\"OP\">EQ</field>\r\n<value name=\"A\">\r\n<block type=\"component_set_get\" id=\"57\" inline=\"false\">\r\n<mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\r\n<field name=\"PROP\">Text</field>\r\n<value name=\"COMPONENT\">\r\n<block type=\"lexical_variable_get\" id=\"58\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<value name=\"B\">\r\n<block type=\"component_set_get\" id=\"59\">\r\n<mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\r\n<field name=\"PROP\">Text</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"lexical_variable_set\" id=\"60\" inline=\"false\">\r\n<field name=\"VAR\">global score</field>\r\n<value name=\"VALUE\">\r\n<block type=\"math_add\" id=\"61\" inline=\"true\">\r\n<mutation items=\"2\"></mutation>\r\n<value name=\"NUM0\">\r\n<block type=\"lexical_variable_get\" id=\"62\">\r\n<field name=\"VAR\">global score</field>\r\n</block>\r\n</value>\r\n<value name=\"NUM1\">\r\n<block type=\"math_number\" id=\"63\">\r\n<field name=\"NUM\">50</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<next>\r\n<block type=\"procedures_callnoreturn\" id=\"64\">\r\n<mutation name=\"bonneReponse\"></mutation>\r\n<field name=\"PROCNAME\">bonneReponse</field>\r\n</block>\r\n</next>\r\n</block>\r\n</statement>\r\n<statement name=\"ELSE\">\r\n<block type=\"procedures_callnoreturn\" id=\"65\">\r\n<mutation name=\"mauvaiseReponse\"></mutation>\r\n<field name=\"PROCNAME\">mauvaiseReponse</field>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n<value name=\"IF1\">\r\n<block type=\"math_compare\" id=\"66\" inline=\"true\">\r\n<field name=\"OP\">EQ</field>\r\n<value name=\"A\">\r\n<block type=\"lexical_variable_get\" id=\"67\">\r\n<field name=\"VAR\">global reponses</field>\r\n</block>\r\n</value>\r\n<value name=\"B\">\r\n<block type=\"math_number\" id=\"68\">\r\n<field name=\"NUM\">1</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO1\">\r\n<block type=\"controls_forEach\" id=\"69\" inline=\"false\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n<value name=\"LIST\">\r\n<block type=\"lexical_variable_get\" id=\"70\">\r\n<field name=\"VAR\">global QCM</field>\r\n</block>\r\n</value>\r\n<statement name=\"DO\">\r\n<block type=\"controls_if\" id=\"71\" inline=\"false\">\r\n<value name=\"IF0\">\r\n<block type=\"component_set_get\" id=\"72\" inline=\"false\">\r\n<mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\r\n<field name=\"PROP\">Checked</field>\r\n<value name=\"COMPONENT\">\r\n<block type=\"lexical_variable_get\" id=\"73\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"controls_if\" id=\"74\" inline=\"false\">\r\n<mutation else=\"1\"></mutation>\r\n<value name=\"IF0\">\r\n<block type=\"logic_compare\" id=\"75\" inline=\"true\">\r\n<field name=\"OP\">EQ</field>\r\n<value name=\"A\">\r\n<block type=\"component_set_get\" id=\"76\" inline=\"false\">\r\n<mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\r\n<field name=\"PROP\">Text</field>\r\n<value name=\"COMPONENT\">\r\n<block type=\"lexical_variable_get\" id=\"77\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<value name=\"B\">\r\n<block type=\"component_set_get\" id=\"78\">\r\n<mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\r\n<field name=\"PROP\">Text</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"lexical_variable_set\" id=\"79\" inline=\"false\">\r\n<field name=\"VAR\">global score</field>\r\n<value name=\"VALUE\">\r\n<block type=\"math_add\" id=\"80\" inline=\"true\">\r\n<mutation items=\"2\"></mutation>\r\n<value name=\"NUM0\">\r\n<block type=\"lexical_variable_get\" id=\"81\">\r\n<field name=\"VAR\">global score</field>\r\n</block>\r\n</value>\r\n<value name=\"NUM1\">\r\n<block type=\"math_number\" id=\"82\">\r\n<field name=\"NUM\">30</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<next>\r\n<block type=\"procedures_callnoreturn\" id=\"83\">\r\n<mutation name=\"bonneReponse\"></mutation>\r\n<field name=\"PROCNAME\">bonneReponse</field>\r\n</block>\r\n</next>\r\n</block>\r\n</statement>\r\n<statement name=\"ELSE\">\r\n<block type=\"procedures_callnoreturn\" id=\"84\">\r\n<mutation name=\"mauvaiseReponse\"></mutation>\r\n<field name=\"PROCNAME\">mauvaiseReponse</field>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n<value name=\"IF2\">\r\n<block type=\"math_compare\" id=\"85\" inline=\"true\">\r\n<field name=\"OP\">EQ</field>\r\n<value name=\"A\">\r\n<block type=\"lexical_variable_get\" id=\"86\">\r\n<field name=\"VAR\">global reponses</field>\r\n</block>\r\n</value>\r\n<value name=\"B\">\r\n<block type=\"math_number\" id=\"87\">\r\n<field name=\"NUM\">2</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO2\">\r\n<block type=\"controls_forEach\" id=\"88\" inline=\"false\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n<value name=\"LIST\">\r\n<block type=\"lexical_variable_get\" id=\"89\">\r\n\r\n<field name=\"VAR\">global QCM</field>\r\n</block>\r\n</value>\r\n<statement name=\"DO\">\r\n<block type=\"controls_if\" id=\"90\" inline=\"false\">\r\n<value name=\"IF0\">\r\n<block type=\"component_set_get\" id=\"91\" inline=\"false\">\r\n<mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\r\n<field name=\"PROP\">Checked</field>\r\n<value name=\"COMPONENT\">\r\n<block type=\"lexical_variable_get\" id=\"92\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"controls_if\" id=\"93\" inline=\"false\">\r\n<mutation else=\"1\"></mutation>\r\n<value name=\"IF0\">\r\n<block type=\"logic_compare\" id=\"94\" inline=\"true\">\r\n<field name=\"OP\">EQ</field>\r\n<value name=\"A\">\r\n<block type=\"component_set_get\" id=\"95\" inline=\"false\">\r\n<mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\r\n<field name=\"PROP\">Text</field>\r\n<value name=\"COMPONENT\">\r\n<block type=\"lexical_variable_get\" id=\"96\">\r\n<field name=\"VAR\">\u00E9l\u00E9ment</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<value name=\"B\">\r\n<block type=\"component_set_get\" id=\"97\">\r\n<mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\r\n<field name=\"PROP\">Text</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO0\">\r\n<block type=\"lexical_variable_set\" id=\"98\" inline=\"false\">\r\n<field name=\"VAR\">global score</field>\r\n<value name=\"VALUE\">\r\n<block type=\"math_add\" id=\"99\" inline=\"true\">\r\n<mutation items=\"2\"></mutation>\r\n<value name=\"NUM0\">\r\n<block type=\"lexical_variable_get\" id=\"100\">\r\n<field name=\"VAR\">global score</field>\r\n</block>\r\n</value>\r\n<value name=\"NUM1\">\r\n<block type=\"math_number\" id=\"101\">\r\n<field name=\"NUM\">10</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<next>\r\n<block type=\"procedures_callnoreturn\" id=\"102\">\r\n<mutation name=\"bonneReponse\"></mutation>\r\n<field name=\"PROCNAME\">bonneReponse</field>\r\n</block>\r\n</next>\r\n</block>\r\n</statement>\r\n<statement name=\"ELSE\">\r\n<block type=\"procedures_callnoreturn\" id=\"103\">\r\n<mutation name=\"mauvaiseReponse\"></mutation>\r\n<field name=\"PROCNAME\">mauvaiseReponse</field>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n<value name=\"IF3\">\r\n<block type=\"math_compare\" id=\"104\" inline=\"true\">\r\n<field name=\"OP\">EQ</field>\r\n<value name=\"A\">\r\n<block type=\"lexical_variable_get\" id=\"105\">\r\n<field name=\"VAR\">global reponses</field>\r\n</block>\r\n</value>\r\n<value name=\"B\">\r\n<block type=\"math_number\" id=\"106\">\r\n<field name=\"NUM\">3</field>\r\n</block>\r\n</value>\r\n</block>\r\n</value>\r\n<statement name=\"DO3\">\r\n<block type=\"procedures_callnoreturn\" id=\"107\">\r\n<mutation name=\"FermeEcran\"></mutation>\r\n<field name=\"PROCNAME\">FermeEcran</field>\r\n<next>\r\n<block type=\"controls_openAnotherScreenWithStartValue\" id=\"108\" inline=\"false\">\r\n<value name=\"SCREENNAME\">\r\n<block type=\"text\" id=\"109\">\r\n<field name=\"TEXT\">Activite_4</field>\r\n</block>\r\n</value>\r\n<value name=\"STARTVALUE\">\r\n<block type=\"lexical_variable_get\" id=\"110\">\r\n<field name=\"VAR\">global score</field>\r\n</block>\r\n</value>\r\n</block>\r\n</next>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n<statement name=\"ELSE\">\r\n<block type=\"component_method\" id=\"111\" inline=\"false\">\r\n<mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\r\n<field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\r\n<value name=\"ARG0\">\r\n<block type=\"text\" id=\"112\">\r\n<field name=\"TEXT\">Il faut choisir une r\u00E9ponse pour passer \u00E0 l'activit\u00E9 suivante !</field>\r\n</block>\r\n</value>\r\n<value name=\"ARG1\">\r\n<block type=\"text\" id=\"113\">\r\n<field name=\"TEXT\">Information</field>\r\n</block>\r\n</value>\r\n<value name=\"ARG2\">\r\n<block type=\"text\" id=\"114\">\r\n<field name=\"TEXT\">Ok</field>\r\n</block>\r\n</value>\r\n</block>\r\n</statement>\r\n</block>\r\n</statement>\r\n</block>\r\n<yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\r\n</xml>";

  public  static String TVB =
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"[0-9]+\" y=\"-[0-9]+\">\n" +
                  "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
                  "(\\n|.)*" +
                  "    </statement>\n" +
                  "  </block>";

  public static String Take_Picture_Component =
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
                  "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>" +
                  "(\\n|.)*" +
                  "                    <field name=\"VAR\">global User</field>\n" +
                  "                  </block>\n" +
                  "                </value>\n" +
                  "              </block>\n" +
                  "            </next>\n" +
                  "          </block>\n" +
                  "        </statement>\n" +
                  "      </block>\n" +
                  "    </statement>\n" +
                  "  </block>";
  public static String A_remplacer =         "<block type=\"text\" id=\"[0-9]+\">\n" +
          " +<field name=\"TEXT\">Fin</field>\n" +
          " +</block>";
  public static String A_remplacer2 = "<block type=\"text\" id=\"[0-9]+\">\n" +
          " +<field name=\"TEXT\">Activite_[0-9]</field>\n" +
          " +</block>";
}



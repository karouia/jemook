// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.client.explorer.commands;

import com.google.appinventor.client.JemBlocksMethods;
import com.google.appinventor.client.JemBlocksRegex;
import com.google.appinventor.client.Ode;
import com.google.appinventor.client.OdeAsyncCallback;
import com.google.appinventor.client.editor.FileEditor;
import com.google.appinventor.client.editor.simple.SimpleEditor;
import com.google.appinventor.client.editor.youngandroid.YaBlocksEditor;
import com.google.appinventor.client.editor.youngandroid.YaFormEditor;
import com.google.appinventor.client.editor.youngandroid.YaProjectEditor;
import com.google.appinventor.client.explorer.project.Project;

import static com.google.appinventor.client.JemBlocksStrings.Next_Activite;
import static com.google.appinventor.client.Ode.MESSAGES;
import com.google.appinventor.shared.rpc.project.ProjectNode;
import com.google.appinventor.shared.rpc.project.youngandroid.YoungAndroidBlocksNode;
import com.google.appinventor.shared.rpc.project.youngandroid.YoungAndroidFormNode;
import com.google.appinventor.shared.rpc.project.youngandroid.YoungAndroidSourceNode;
import com.google.appinventor.shared.rpc.project.youngandroid.YoungAndroidYailNode;
import com.google.gwt.user.client.Window;

import java.util.ArrayList;
import java.util.List;

/**
 * Command for deleting files.
 *
 */
public class DeleteFileCommand extends ChainableCommand {
  /**
   * Creates a new command for deleting a file.
   */
  public DeleteFileCommand() {
    super(null); // no next command
  }

  @Override
  public boolean willCallExecuteNextCommand() {
    return true;
  }

  @Override
  public void execute(final ProjectNode node) {
    if (deleteConfirmation()) {
      final Ode ode = Ode.getInstance();


      if (node instanceof YoungAndroidSourceNode) {
        // node could be either a YoungAndroidFormNode or a YoungAndroidBlocksNode.
        // Before we delete the form, we need to close both the form editor and the blocks editor
        // (in the browser).
        int compteur =-2;
        String activiteavant="";
        String activiteapres="";
        String stract= ((YoungAndroidSourceNode) node).getQualifiedName();
        stract = stract.substring(stract.length()-10);
        List<String> myArrayList = new ArrayList<String>();
        for (FileEditor fileEditor : ode.getCurrentFileEditor().getProjectEditor().getOpenFileEditors()) {
          if (fileEditor instanceof YaFormEditor) {
            YaFormEditor formEditor= (YaFormEditor) fileEditor;
            myArrayList.add(formEditor.getForm().getName());
            compteur++;
          }
        }
        int activityIndex = Integer.parseInt(stract.substring(9));

        //Déterminer le screen d'avant
        for (int i=activityIndex-1; i>=0; i-- ){
          if(i!=0) {
            String tamponactiviteavant = "Activite_" + i;
            if (myArrayList.contains(tamponactiviteavant)){
              activiteavant= tamponactiviteavant;
              break;
            }
          }
          //Screen 1
          else {
            activiteavant = "Screen1";
          }
        }
        //Déterminer le screen d'après
        for (int i=activityIndex+1; i<=10; i++ ){
          if(i!=10) {
            String tamponactiviteapres = "Activite_" + i;
            if(myArrayList.contains(tamponactiviteapres)){
              activiteapres= tamponactiviteapres;
              break;
            }
          }
          // Pointe vers fin
          else {
            activiteapres = "Fin";
          }
        }

        SimpleEditor simpleEditor= (SimpleEditor)Ode.getInstance().getCurrentFileEditor();
        YaProjectEditor projectEditor = (YaProjectEditor) simpleEditor.getProjectEditor();
        YaProjectEditor yaProjectEditor = (YaProjectEditor) projectEditor;
        YaBlocksEditor blocksEditor = yaProjectEditor.getBlocksFileEditor(activiteavant);
        String blocksContent = blocksEditor.getBlocksString();
        String nouveau_block = Next_Activite +activiteapres+ "</field>\n" +"</block>\n";
        if (JemBlocksMethods.blocsExist(blocksContent, JemBlocksRegex.A_remplacer)){
          blocksContent = JemBlocksMethods.updateBlock(blocksContent,JemBlocksRegex.A_remplacer, nouveau_block);
        }
        if (JemBlocksMethods.blocsExist(blocksContent, JemBlocksRegex.A_remplacer2)){
          blocksContent = JemBlocksMethods.updateBlock(blocksContent,JemBlocksRegex.A_remplacer2, nouveau_block);
        }
        blocksEditor.reloadBlocks(blocksContent);





        final String qualifiedFormName = ((YoungAndroidSourceNode) node).getQualifiedName();
        final String formFileId = YoungAndroidFormNode.getFormFileId(qualifiedFormName);
        final String blocksFileId = YoungAndroidBlocksNode.getBlocklyFileId(qualifiedFormName);
        final String yailFileId = YoungAndroidYailNode.getYailFileId(qualifiedFormName);
        final long projectId = node.getProjectId();
        String fileIds[] = new String[2];
        fileIds[0] = formFileId;
        fileIds[1] = blocksFileId;
        ode.getEditorManager().closeFileEditors(projectId, fileIds);

        // When we tell the project service to delete either the form (.scm) file or the blocks
        // (.bky) file, it will delete both of them, and also the yail (.yail) file.
        ode.getProjectService().deleteFile(ode.getSessionId(), projectId, node.getFileId(),
            new OdeAsyncCallback<Long>(
        // message on failure
            MESSAGES.deleteFileError()) {
          @Override
          public void onSuccess(Long date) {
            // Remove all related nodes (form, blocks, yail) from the project.
            Project project = getProject(node);
            for (ProjectNode sourceNode : node.getProjectRoot().getAllSourceNodes()) {
              if (sourceNode.getFileId().equals(formFileId) ||
                  sourceNode.getFileId().equals(blocksFileId) ||
                  sourceNode.getFileId().equals(yailFileId)) {
                project.deleteNode(sourceNode);
              }
            }
            ode.getDesignToolbar().removeScreen(project.getProjectId(), 
                ((YoungAndroidSourceNode) node).getFormName());
            ode.updateModificationDate(projectId, date);
            executeNextCommand(node);
          }

          @Override
          public void onFailure(Throwable caught) {
            super.onFailure(caught);
            executionFailedOrCanceled();
          }
        });
      } else {  // asset file
        ode.getProjectService().deleteFile(ode.getSessionId(),
            node.getProjectId(), node.getFileId(),
            new OdeAsyncCallback<Long>(
                // message on failure
                MESSAGES.deleteFileError()) {
              @Override
              public void onSuccess(Long date) {
                getProject(node).deleteNode(node);
                ode.updateModificationDate(node.getProjectId(), date);
                executeNextCommand(node);
              }

              @Override
              public void onFailure(Throwable caught) {
                super.onFailure(caught);
                executionFailedOrCanceled();
              }
            });
      }
    } else {
      executionFailedOrCanceled();
    }
  }

  /**
   * Shows a confirmation dialog.
   *
   * @return {@code true} if the delete file command should be executed or
   *         {@code false} if it should be canceled
   */
  protected boolean deleteConfirmation() {
    return Window.confirm(MESSAGES.reallyDeleteFile());
  }
}

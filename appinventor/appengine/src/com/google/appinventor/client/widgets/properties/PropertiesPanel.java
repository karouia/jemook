// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.client.widgets.properties;

import com.google.appinventor.client.Images;
import com.google.appinventor.client.Ode;
import com.google.appinventor.client.editor.FileEditor;
import com.google.appinventor.client.editor.simple.components.MockCheckBox;
import com.google.appinventor.client.editor.simple.components.MockComponent;
import com.google.appinventor.client.editor.simple.components.MockForm;
import com.google.appinventor.client.editor.simple.components.MockLabel;
import com.google.appinventor.client.editor.youngandroid.YaFormEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.*;

import static com.google.appinventor.client.Ode.INTERMEDIATE;
import static com.google.appinventor.client.Ode.MESSAGES;
import static com.google.appinventor.client.Ode.STANDARD;
import static com.google.gwt.user.client.ui.HasHorizontalAlignment.ALIGN_CENTER;

/**
 * Panel to display properties.
 *
 */
public class PropertiesPanel extends Composite {

  // UI elements
  private final VerticalPanel panel;
  private final Label componentName;
  private final VerticalPanel outerPanel;

  protected static final Images images = Ode.getImageBundle();

  // Un bouton pour afficher + ou - de propriétés
  private PushButton advancedProperties;
  private ImageResource morePropertiesImage;
  private ImageResource lessPropertiesImage ;

  /**
   * Creates a new properties panel.
   */
  public PropertiesPanel() {
    setAdvancedPropertiesButton();
    // Initialize UI
    outerPanel = new VerticalPanel();
    outerPanel.setWidth("100%");

    componentName = new Label("");
    componentName.setStyleName("ode-PropertiesComponentName");
    outerPanel.add(componentName);

    panel = new VerticalPanel();
    panel.setHorizontalAlignment(ALIGN_CENTER);
    panel.setWidth("100%");
    panel.setStylePrimaryName("ode-PropertiesPanel");
    outerPanel.setHorizontalAlignment(ALIGN_CENTER);
    outerPanel.add(panel);

    outerPanel.add(advancedProperties);

    initWidget(outerPanel);
  }

  /**
   * Adds a new property to be displayed in the UI.
   *
   * @param property  new property to be shown
   */
  void addProperty(EditableProperty property) {
    Label label = new Label(property.getCaption());
    label.setStyleName("ode-PropertyLabel");
    panel.add(label);
    PropertyEditor editor = property.getEditor();
    // Since UIObject#setStyleName(String) clears existing styles, only
    // style the editor if it hasn't already been styled during instantiation.
    if(!editor.getStyleName().contains("PropertyEditor")) {
      editor.setStyleName("ode-PropertyEditor");
    }
    panel.add(editor);
  }

  /**
   * Removes all properties from the properties panel.
   */
  public void clear() {
    panel.clear();
    componentName.setText("");
  }

  /**
   * Shows a set of properties in the panel. Any previous content will be
   * removed.
   *
   * @param properties  properties to be shown
   */
  public void setProperties(EditableProperties properties) {
    clear();
    properties.addToPropertiesPanel(this);
  }

  /**
   * Set the label at the top of the properties panel. Note that you have
   * to do this after calling setProperties because it clears the label!
   * @param name
   */
  public void setPropertiesCaption(String name) {
    componentName.setText(name);
  }

  private boolean isMaximized(MockComponent component){
    if (component instanceof MockForm){
      if (component.getProperties().getProperty("TriggerMode").getType()==EditableProperty.TYPE_INVISIBLE)
        return false;
    }
    else if (component instanceof MockLabel || component instanceof MockCheckBox){
      if (component.getProperties().getProperty("FontSize").getType()==EditableProperty.TYPE_INVISIBLE)
        return false;
    }
    return true;
  }

  public PushButton getAdvancedPropertiesButton(){
    return advancedProperties;
  }

  public void setAdvancedPropertiesButton(){
    // Donne les couleurs en fonction du mode
    if (Ode.getInstance().getCurrentMode()==STANDARD){
      morePropertiesImage = images.morePropertiesStandard();
      lessPropertiesImage = images.lessPropertiesStandard();
    }
    else if (Ode.getInstance().getCurrentMode()==INTERMEDIATE){
      morePropertiesImage = images.morePropertiesIntermediate();
      lessPropertiesImage = images.lessPropertiesIntermediate();
    }
    else {
      morePropertiesImage = images.morePropertiesExpert();
      lessPropertiesImage = images.lessPropertiesExpert();
    }
    // Initialize advancedProperties button
    advancedProperties = Ode.createPushButton(morePropertiesImage, MESSAGES.moreProperties(), new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        FileEditor currentFileEditor=Ode.getInstance().getCurrentFileEditor();
        if (currentFileEditor instanceof YaFormEditor) {
          YaFormEditor formEditor = (YaFormEditor)currentFileEditor;
          MockComponent selectedComponent = formEditor.getForm().getSelectedComponent();
          if (isMaximized(selectedComponent)){
            formEditor.toggleAdvancedJemProperties(selectedComponent,EditableProperty.TYPE_INVISIBLE);
            advancedProperties.getUpFace().setImage(new Image(morePropertiesImage));
            advancedProperties.setTitle(MESSAGES.moreProperties());
          }
          else{
            formEditor.toggleAdvancedJemProperties(selectedComponent,EditableProperty.TYPE_NORMAL);
            advancedProperties.getUpFace().setImage(new Image(lessPropertiesImage));
            advancedProperties.setTitle(MESSAGES.lessProperties());
          }
        }
      }
    });
    advancedProperties.setStyleName("Aous-NonActiveMode-button");
  }


  // Fonction à supprimer (inutile pour le moment: 05/01/2020)
  public void updateAvdancedPropertiesButton(){
    outerPanel.clear();
    setAdvancedPropertiesButton();
    outerPanel.add(panel);
    outerPanel.add(advancedProperties);
  }
}

// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.client.boxes;

import com.google.appinventor.client.*;
import com.google.appinventor.client.editor.FileEditor;
import com.google.appinventor.client.editor.youngandroid.YaBlocksEditor;
import com.google.appinventor.client.editor.youngandroid.YaFormEditor;
import com.google.appinventor.client.explorer.commands.AddFormCommand;
import com.google.appinventor.client.explorer.commands.ChainableCommand;
import com.google.appinventor.client.explorer.project.Project;
import com.google.appinventor.client.tracking.Tracking;
import com.google.appinventor.client.widgets.boxes.Box;
import com.google.appinventor.shared.rpc.project.ProjectRootNode;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.http.client.*;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.maps.client.LoadApi;
import com.google.gwt.maps.client.MapOptions;
import com.google.gwt.maps.client.MapTypeId;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.base.LatLngBounds;
import com.google.gwt.maps.client.controls.ControlPosition;
import com.google.gwt.maps.client.events.bounds.BoundsChangeMapEvent;
import com.google.gwt.maps.client.events.bounds.BoundsChangeMapHandler;
import com.google.gwt.maps.client.events.click.ClickMapEvent;
import com.google.gwt.maps.client.events.click.ClickMapHandler;
import com.google.gwt.maps.client.events.dragend.DragEndMapEvent;
import com.google.gwt.maps.client.events.dragend.DragEndMapHandler;
import com.google.gwt.maps.client.events.dragstart.DragStartMapEvent;
import com.google.gwt.maps.client.events.dragstart.DragStartMapHandler;
import com.google.gwt.maps.client.events.place.PlaceChangeMapEvent;
import com.google.gwt.maps.client.events.place.PlaceChangeMapHandler;
import com.google.gwt.maps.client.overlays.*;
import com.google.gwt.maps.client.placeslib.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import static com.google.appinventor.client.Ode.*;
import static com.google.gwt.user.client.Window.alert;



public class MapBox extends Box {
  protected static final Images images = Ode.getImageBundle();
  private static final MapBox INSTANCE = new MapBox();
  private MapWidget mapWidget;
  private final PushButton plus;
  private final PushButton reload;
  private TextBox placeSearchTextField;
  private Ode ode=Ode.getInstance();

  private List<Marker> markers;
  private Geolocation geolocation;
  private MarkerOptions options;
  private LatLng pos;
  private LatLng userPosition;
  private int zoom;
  private boolean geolocated;
  private FlowPanel plusWidget;
  private boolean plusWidgetBlink;

  //            RootPanel.get().add(mapWidget);

  /**
   * Return the singleton Map box.
   *
   * @return  Map box
   */
  public static MapBox getMapBox() {return INSTANCE;}

  /**
   * Creates new empty Map box.
   */
  private MapBox() {
    super(MESSAGES.map(),
            544,    // height
            true,  // minimizable
            false, // removable
            false, // start minimized
            550);// width

    zoom = 4;
    geolocated = false;

    plus = Ode.createPushButton(images.plus(), MESSAGES.addNewActivity(), new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        createForm(); // ajouter if (formCreated) create Marker; else ne rien faire;
      }
    });
    plus.setPixelSize(28, 31);
//    plus.setStyleName("ode-TopPanelButton");
    reload = Ode.createPushButton(images.reload(), MESSAGES.relaodMap(), new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        showMap();
      }
    });
//    reload.setStyleName("Aous-NonActiveMode-button");
    geolocation = Geolocation.getIfSupported();
  }

  public void initializeMap(){
    geolocation.getCurrentPosition(new Callback<Position, PositionError>() {
      @Override
      public void onFailure(PositionError error) { // always called
//            alert("Time-out geolocation callback"); // ... Time-out
        userPosition= LatLng.newInstance(48.858370, 2.294481);
        drawEmptyMap();
        if (mapWidget!=null){
          setContent(mapWidget);
          scheduleMapResize();
        }
      }
      @Override
      public void onSuccess(Position result) {
        geolocated=true;
        Position.Coordinates coords = result.getCoordinates();
        double lat = coords.getLatitude();
        double lng = coords.getLongitude();
        userPosition = LatLng.newInstance(lat, lng);
        zoom=2;
        drawEmptyMap();// sans lequel y a pas interaction
        if (mapWidget!=null){
          setContent(mapWidget);
          scheduleMapResize();
        }
      }
    });
  }

  public void showMap(){
    initializeMap();
    Timer timerThistimer = new Timer() {
      @Override
      public void run() {
        if (mapWidget!=null){
          loadMarkers(markers);
          setContent(mapWidget);
          scheduleMapResize();
        }
      }
    };
    timerThistimer.schedule(5000);
    // Il vaut mieux donner autant de temps nécessaire pour pouvoir charger tous les screens
    // afin que la boucle du loadMarker() fonctionne bien
    // ça dépend du nombre des screens/POI à charger
  }

  public void drawEmptyMap() {
    this.clear();
    drawMap();
    drawControls();
  }

  public void draw() {
    this.clear();
    placeSearchTextField = new TextBox();
    placeSearchTextField.setWidth("350px");
    drawMap();
    drawControls();
    drawAutoComplete();
    loadMarkers(markers);
//    if (markersLoaded) {
//      LatLngBounds latLngBounds = mapWidget.getBounds();  //plustard
//      latLngBounds.extend(firstScreenPosition);           //plustard
//      mapWidget.fitBounds(latLngBounds);                  //plustard
//    }
  }


  private void drawMap() {
    MapOptions opts = MapOptions.newInstance();
    opts.setMapTypeId(MapTypeId.ROADMAP);
    opts.setDraggable(true);
    opts.setCenter(userPosition);
    opts.setZoom(zoom);
    mapWidget = new MapWidget(opts);

    mapWidget.setSize("500px", "547.5px");
//    loadMarkers(markers);

    options = MarkerOptions.newInstance();

    MarkerImage logo=MarkerImage.newInstance("http://aous-karoui.com/JemInv/img/marker.png");
    options.setIcon(logo);
    options.setPosition(userPosition);
    options.setDraggable(true);
  }


  private void drawControls() {
    DesignToolbar.DesignProject currentProject = ode.getDesignToolbar().getCurrentProject();
    placeSearchTextField = new TextBox();
    placeSearchTextField.addKeyPressHandler( new KeyPressHandler() {
      @Override
      public void onKeyPress( final KeyPressEvent event ) {
        if ( event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER ){
          Requete();
        }
      }
    } );

    final Button validationTexte = new Button("Valider", new ClickHandler() {
      public void onClick(ClickEvent event) {
        Requete();


        //  alert(placeSearchTextField.getValue());
      }
    });
    plusWidget = new FlowPanel();

    HTML newActivity = new HTML("Nouvelle</br>&nbsp;Activité");
//    newActivity.setStyleName("ode-BlinkingText");

    plusWidget.add(newActivity);
    plusWidget.add(plus);

    if (currentProject.screens.size()<2)
      makePlusWidgetBlink();

    // TODO I'm not able to get the stylesheet to work, but this works below
    DOM.setStyleAttribute(plusWidget.getElement(), "background", "white");
    DOM.setStyleAttribute(plusWidget.getElement(), "padding", "5px");
    DOM.setStyleAttribute(plusWidget.getElement(), "margin", "3px");
    if (ode.getCurrentMode()==STANDARD)
      DOM.setStyleAttribute(plusWidget.getElement(), "border", "3px solid #5DBB46");
    else if (ode.getCurrentMode()==INTERMEDIATE)
      DOM.setStyleAttribute(plusWidget.getElement(), "border", "3px solid #22A0DA");
    else
      DOM.setStyleAttribute(plusWidget.getElement(), "border", "3px solid #F06626");

    //  mapWidget.setControls(ControlPosition.RIGHT_TOP, placeSearchTextField );
    Timer t = new Timer() {
      @Override
      public void run() {
        mapWidget.setControls(ControlPosition.RIGHT_TOP, plusWidget);
        mapWidget.setControls(ControlPosition.TOP_CENTER, reload);


        mapWidget.setControls(ControlPosition.LEFT_BOTTOM, placeSearchTextField );
        mapWidget.setControls(ControlPosition.BOTTOM_LEFT, validationTexte );
      }
    };
    t.schedule(2000);
    // Il vaut mieux retarder l'affichage du plusBouton pour pallier
    // au problème du chargement double du POI si on appuie très vite sur le plusBouton
  /*  Timer u  = new Timer() {
      @Override
      public void run() {

        alert(placeSearchTextField.getValue());
      }
    };
    u.schedule(20000);*/

  }


  public void drawMarkerWithBounceAnimation(String formName, Project project) {

    Marker marker = Marker.newInstance(options);
    pos= marker.getPosition();
    marker.setTitle(formName);
    marker.setPosition(userPosition);
    marker.setMap(mapWidget);

    if (markers==null) {
      markers=new ArrayList<Marker>();   // Pour les animations
    }
    markers.add(marker);

    drawInfoWindowOnMarker(marker, formName);
    editMarkerEvents(project.getProjectId(),formName,marker);

  }

  private void drawMarkerWithBounceAnimation(String formName, LatLng markerPosition, long projectId) {

    Marker marker = Marker.newInstance(options);
    marker.setTitle(formName);
    marker.setPosition(markerPosition);
    marker.setMap(mapWidget);
    pos= marker.getPosition();

    if (markers==null) {
      markers=new ArrayList<Marker>();   // Pour les animations
    }
    markers.add(marker);

//    if (formName.equals("Screen1"))
//      drawInfoWindowOnMarker(marker, "Activite_1");
//    else
    drawInfoWindowOnMarker(marker, formName);
    editMarkerEvents(projectId,formName,marker);

  }


  private void drawInfoWindowOnMarker(Marker marker, String formName){
    HTML html = new HTML(formName);
    VerticalPanel verticalPanel = new VerticalPanel();
    verticalPanel.add(html);
    InfoWindowOptions infoWindowOptions = InfoWindowOptions.newInstance();
    InfoWindow infoWindow = InfoWindow.newInstance(infoWindowOptions);
    infoWindow.setPosition(userPosition);
    infoWindow.setContent(verticalPanel);
    infoWindow.open(mapWidget,marker);
  }


  private void editMarkerEvents(final long projectId, final String formName, final Marker marker){
    marker.addClickHandler(new ClickMapHandler() {
      @Override
      public void onEvent(ClickMapEvent event) {
        ode.getDesignToolbar().switchToScreen(projectId,formName,DesignToolbar.View.FORM);
      }
    });

    marker.addDragStartHandler(new DragStartMapHandler() {
      @Override
      public void onEvent(DragStartMapEvent event) {
        ode.getDesignToolbar().switchToScreen(projectId,formName,DesignToolbar.View.FORM);
      }
    });

    marker.addDragEndHandler(new DragEndMapHandler() {
      @Override
      public void onEvent(DragEndMapEvent event) {
        LatLng pos = marker.getPosition();
        FileEditor editor = ode.getCurrentFileEditor();
        YaFormEditor casted = (YaFormEditor) editor;
        casted.getForm().changeProperty("APoiLong",""+pos.getLongitude());
        casted.getForm().changeProperty("APoiLat",""+pos.getLatitude());
//        casted.getForm().changeProperty("AboutScreen","Longitude: "+pos.getLongitude()+" Latitude: "+pos.getLatitude());
      }
    });
  }


  private void createForm(){
    if (ode.screensLocked()) {
      return;                 // Don't permit this if we are locked out (saving files)
    }
    ProjectRootNode projectRootNode = ode.getCurrentYoungAndroidProjectRootNode();
    if (projectRootNode != null) {
      ChainableCommand cm = new AddFormCommand();
      cm.startExecuteChain(Tracking.PROJECT_ACTION_ADDFORM_YA, projectRootNode);
    }
  }

  private void loadMarkers(List<Marker> markers){
    DesignToolbar.DesignProject currentProject = ode.getDesignToolbar().getCurrentProject();
    if (currentProject != null){
      // AOUS Inutile d'exécuter tout le reste si on est pas sur le DESIGNER (cas page début)

      // Autant qu'il y a d'activités autant qu'il y aura de temps de chargement
      // donc vaut mieux encore retarder quand screens.size>4 pour garantir l'affichage des POI
      for (final DesignToolbar.Screen screen : currentProject.screens.values()) {
        if (screen.formEditor instanceof YaFormEditor) {
          final YaFormEditor thisFormEditor= (YaFormEditor)screen.formEditor;
          final YaBlocksEditor thisBlocksEditor= (YaBlocksEditor)screen.blocksEditor;
          if (thisFormEditor.isLoadComplete()) {
            String longitude = thisFormEditor.getForm().getPropertyValue("APoiLong");
            String latitude = thisFormEditor.getForm().getPropertyValue("APoiLat");
            if (longitude.equals("Undefined")) {
              if (screen.screenName!="Screen1")
                drawMarkerWithBounceAnimation(screen.screenName, userPosition, thisFormEditor.getProjectId());
              else { // On est dans Screen1
                // désactiver boutons de navigation latéraux car on est dans Screen1
                thisFormEditor.getForm().getNextMockFormButton().setEnabled(false);
                if (!thisFormEditor.getComponentNames().contains("StartButton")) {
                  // Ici nous rechargeons les blocs avec la chaine XML du Screen1 m.à.j avec tellNextActivity
                  thisBlocksEditor.reloadBlocks(JemBlocksMethods.tellNextActivity("Screen1", JemBlocksStrings.SCREEN1));
                  new JemComponents().addWelcomeComponents(thisFormEditor);
                }
                else
                  new JemComponents(); // if JemComponent already exist, we instantiate the class to enable interactions (next, previous buttons, etc.)
              }
            }
            else {
              if (screen.screenName!="Screen1") {
                LatLng markerPosition = LatLng.newInstance(Double.parseDouble(latitude),Double.parseDouble(longitude));
                drawMarkerWithBounceAnimation(screen.screenName, markerPosition, thisFormEditor.getProjectId());
                mapWidget.panTo(userPosition);
              }
            }
          }
          else alert("Problème de chargement. Veuillez rafraichir la page. Si le problème persiste, vérifiez la connexion.");
        }
      }
    }
//    Le zoom arrive une fois tous les marqueurs chargés
    mapWidget.setZoom(16);
  }

  private void scheduleMapResize(){
    Timer timer = new Timer() {
      @Override
      public void run() {
        mapWidget.triggerResize();
      }
    };
    timer.schedule(9000);
  }

  public List<Marker> getMarkerList() { return markers;}


  public void setMarkerList(List<Marker> markers) { this.markers=markers;}


  public LatLng getMarkerPos() { return pos;}


//  public LatLng getFirstScreenPosition() { return firstScreenPosition;}


  public LatLng getUserPosition() { return userPosition;}


  public MapWidget getMapWidget() { return mapWidget;}

  private void loadMapApi() {

    boolean sensor = true;

    // load all the libs for use in the maps
    ArrayList<LoadApi.LoadLibrary> loadLibraries = new ArrayList<LoadApi.LoadLibrary>();
    loadLibraries.add(LoadApi.LoadLibrary.ADSENSE);
    loadLibraries.add(LoadApi.LoadLibrary.DRAWING);
    loadLibraries.add(LoadApi.LoadLibrary.GEOMETRY);
    loadLibraries.add(LoadApi.LoadLibrary.PANORAMIO);
    loadLibraries.add(LoadApi.LoadLibrary.PLACES);
    loadLibraries.add(LoadApi.LoadLibrary.WEATHER);
    loadLibraries.add(LoadApi.LoadLibrary.VISUALIZATION);

    Runnable onLoad = new Runnable() {
      @Override
      public void run() {
        draw();
      }
    };

    LoadApi.go(onLoad, loadLibraries, sensor);
  }


  private void drawAutoComplete() {
    Element element = placeSearchTextField.getElement();

    AutocompleteType[] types = new AutocompleteType[2];
    types[0] = AutocompleteType.ESTABLISHMENT;
    types[1] = AutocompleteType.GEOCODE;

    AutocompleteOptions options = AutocompleteOptions.newInstance();
    options.setTypes(types);
    options.setBounds(mapWidget.getBounds());

    final Autocomplete autoComplete= Autocomplete.newInstance(element,options);

    autoComplete.addPlaceChangeHandler(new PlaceChangeMapHandler() {
      public void onEvent(PlaceChangeMapEvent event) {

        PlaceResult result = autoComplete.getPlace();

        PlaceGeometry geomtry = result.getGeometry();
        LatLng center = geomtry.getLocation();

        mapWidget.panTo(center);
        mapWidget.setZoom(8);

        GWT.log("place changed center=" + center);
      }
    });

    mapWidget.addBoundsChangeHandler(new BoundsChangeMapHandler() {
      public void onEvent(BoundsChangeMapEvent event) {
        LatLngBounds bounds = mapWidget.getBounds();
        autoComplete.setBounds(bounds);
      }
    });
  }

  public void stopBlinkingPlusWidget(){
    plusWidget.setStyleName("");
    plusWidgetBlink=false;
  }

  public void makePlusWidgetBlink(){
    plusWidget.setStyleName("ode-BlinkingText");
    plusWidgetBlink=true;
  }

  public boolean plusWidgetBlinking(){
    return plusWidgetBlink;
  }

  public FlowPanel getPlusWidget(){
    return plusWidget;
  }

  // Adam begin
  public void Requete(){
    String url ="https://maps.googleapis.com/maps/api/geocode/json?address=" + placeSearchTextField.getValue() +",+CA&key=AIzaSyDqzYsKgkd2BkmxtPezUIzAGlk_SwVTi9Q";
    url= url.trim().replaceAll("( )+", "+");
    url= url.trim().replaceAll("é", "e");
    url= url.trim().replaceAll("è", "e");
    url= url.trim().replaceAll("ê", "e");
    url= url.trim().replaceAll("ë", "e");
    url= url.trim().replaceAll("â", "a");
    url= url.trim().replaceAll("à", "a");
    url= url.trim().replaceAll("ï", "i");
    url= url.trim().replaceAll("î", "i");
    url= url.trim().replaceAll("ô", "o");
    url= url.trim().replaceAll("ç", "c");
    url= url.trim().replaceAll("û", "u");
    url= url.trim().replaceAll("ù", "u");
    RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);

    try {
      Request response = builder.sendRequest(null, new RequestCallback() {
        public void onError(Request request, Throwable exception) {
          if (exception instanceof RequestTimeoutException) {
            alert("timeout");
          } else {
            // handle other request errors
          }
        }
        public void onResponseReceived(Request request, Response response) {
          if (response.getStatusCode()==200) {
            JSONValue jsonValue = JSONParser.parseStrict(response.getText());
            JSONObject customerObject = jsonValue.isObject();
            double latitude = Double.parseDouble(customerObject.get("results").isArray().get(0).isObject().get("geometry").isObject().get("location").isObject().get("lat").isNumber().toString());
            double longitude = Double.parseDouble(customerObject.get("results").isArray().get(0).isObject().get("geometry").isObject().get("location").isObject().get("lng").isNumber().toString());
            userPosition = LatLng.newInstance(latitude, longitude);
            mapWidget.panTo(userPosition);
          }
          else {alert("Requete invalide");}
        }
      });
    } catch (RequestException e) {
      alert(e.getMessage());
    }
  }
  // Adam end
}

// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.client.explorer.youngandroid;

import com.google.appinventor.client.GalleryClient;
import com.google.appinventor.client.Ode;
import com.google.appinventor.client.boxes.GalleryListBox;
import com.google.appinventor.client.widgets.DropDownButton;
import com.google.appinventor.client.widgets.DropDownButton.DropDownItem;
import com.google.common.collect.Lists;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

/**
 * The search / sort Toolbar
 * @author  AOUS
 *
 */
public class SearchSortToolbar extends Composite {
  private static final int SEARCHTAB = 4;
  public static List<SearchSortToolbar> allSearchToolbars = new ArrayList<SearchSortToolbar>();  //store the reference of all creating toolbar
  final TextBox searchText;
  final Button searchButton;

  public DropDownButton sortDropDown;


  /**
   * Initializes and assembles all commands into buttons in the toolbar.
   */
  public SearchSortToolbar() {
    allSearchToolbars.add(this);
    HorizontalPanel toolbar = new HorizontalPanel();
    toolbar.setWidth("100%");
    toolbar.setStylePrimaryName("ya-GalleryToolbar");

    FlowPanel searchPanel = new FlowPanel();
    searchText = new TextBox();
    searchText.addStyleName("gallery-search-textarea");
    searchButton = new Button("Search for apps");
    searchButton.addStyleName("search-compontent");
    searchPanel.add(searchText);
    searchPanel.add(searchButton);
    searchPanel.addStyleName("gallery");
    toolbar.add(searchPanel);
    searchButton.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(ClickEvent event) {
        GalleryClient.getInstance().FindApps(searchText.getText(), 0, GalleryList.NUMAPPSTOSHOW, 0, true);
        searchText.setFocus(true);
        Ode.getInstance().switchToGalleryView();
        GalleryListBox.getGalleryListBox().getGalleryList().setSelectTabIndex(SEARCHTAB);
        for(SearchSortToolbar toolbar : allSearchToolbars){
          toolbar.getSearchText().setText(searchText.getText());
        }
        //TODO in gallerylist.java --> findapps: create a way to grab keyword from this toolbar
        //this is just a temp solution.
        GalleryListBox.getGalleryListBox().getGalleryList().getSearchText().setText(searchText.getText());
      }
    });
    searchText.addKeyDownHandler(new KeyDownHandler() {
      @Override
      public void onKeyDown(KeyDownEvent e) {
        if(e.getNativeKeyCode() == KeyCodes.KEY_ENTER){
          GalleryClient.getInstance().FindApps(searchText.getText(), 0, GalleryList.NUMAPPSTOSHOW, 0, true);
          searchText.setFocus(true);
          Ode.getInstance().switchToGalleryView();
          GalleryListBox.getGalleryListBox().getGalleryList().setSelectTabIndex(SEARCHTAB);
          for(SearchSortToolbar toolbar : allSearchToolbars){
            toolbar.getSearchText().setText(searchText.getText());
          }
          //TODO in gallerylist.java --> findapps: create a way to grab keyword from this toolbar
          //this is just a temp solution.
          GalleryListBox.getGalleryListBox().getGalleryList().getSearchText().setText(searchText.getText());
        }
      }
    });

    // Sort Menu AOUS
    List<DropDownButton.DropDownItem> sortItems = Lists.newArrayList();
    FlowPanel sortPanel = new FlowPanel();
    sortItems.add(new DropDownItem("","Popularité", new SwitchToProjectAction()));
    sortItems.add(new DropDownItem("","Meilleurs notes", new SwitchToProjectAction()));
    sortItems.add(new DropDownItem("","Ordre amphabétique", new SwitchToProjectAction()));
    sortItems.add(new DropDownItem("","Date de création", new SwitchToProjectAction()));
    sortItems.add(new DropDownItem("","Nombre d'UJS", new SwitchToProjectAction()));
    sortDropDown = new DropDownButton("panelSort","Trier par",sortItems ,false);
    sortPanel.add(sortDropDown);
    //sortPanel.setHorizontalAlignment(Label.ALIGN_LEFT);
    toolbar.add(sortDropDown);

    initWidget(toolbar);
  }

  /**
   * get searchText label
   * @return Label searchText
   */
  public TextBox getSearchText(){
    return searchText;
  }

  /**
   * get getSearchButton button
   * @return Button searchButton
   */
  public Button getSearchButton(){
    return searchButton;
  }

  private static class SwitchToProjectAction implements Command {
    @Override
    public void execute() {
      Ode.getInstance().switchToProjectsView();
      Ode.getInstance().getTopToolbar().updateFileMenuButtons(0);
    }
  }
}



package com.google.appinventor.client;

/**
 * Created by akaroui on 20/05/2017.
 */
public class JemBlocksStrings {
  // Key Words
  public static final String CURRENT_FORM = "CURRENT_FORM";
  public static final String NEXT_FORM = "NEXT_FORM";
  public static final String SUCCESS_MESSAGE = "SUCCESS_MESSAGE";
  public static final String FAIL_MESSAGE = "FAIL_MESSAGE";


  /**
   * Chaines de départ et de fin pour les blocs
   */

  public static String WEB_VIEWER_FIRST_KEY_STRING ="</field>\n" +
      "\" +\n" +
      "      \"            <field name=\\\"PROP\\\">DataUri</field>\\n\" +\n" +
      "      \"            <value name=\\\"VALUE\\\">\\n\" +\n" +
      "      \"              <block type=\\\"text\\\" id=\\\"227\\\">\\n\" +\n" +
      "      \"                <field name=\"TEXT\">WEB_LINK";
  public static String WEB_VIEWER_SECOND_KEY_STRING ="</field>\n" +
      "\" +\n" +
      "      \"            <field name=\\\"PROP\\\">DataUri</field>\\n\" +\n" +
      "      \"            <value name=\\\"VALUE\\\">\\n\" +\n" +
      "      \"              <block type=\\\"text\\\" id=\\\"227\\\">\\n\" +\n" +
      "      \"                <field name=\"TEXT\">";
  public static String BLOCKS_STARTING ="<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n";
  public static String BLOCKS_ENDING =
      "<yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  /**
   * Chaine initialement vide du corps des blocs
   */
  public static String INITIAL_BLOCKS_CORE ="";

  public static String SCORE_START_VALUE =
      "<block type=\"global_declaration\" id=\"4\" inline=\"false\" x=\"-20\" y=\"-295\">\n" +
      "<field name=\"NAME\">score</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"controls_getStartValue\" id=\"5\"></block>\n" +
      "</value>\n" +
      "</block>";

  public static String SCORE_NULL ="<block type=\"global_declaration\" id=\"4\" inline=\"false\" x=\"-3\" y=\"-306\">\n" +
      "    <field name=\"NAME\">score</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"math_number\" id=\"115\">\n" +
      "        <field name=\"NUM\">0</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>";

  public static String CLUE_NEXT_MANUAL=
      "  <block type=\"component_event\" id=\"81\" x=\"-8\" y=\"-124\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"82\">\n" +
          "        <mutation name=\"displayLearningContent\"></mutation>\n" +
          "        <field name=\"PROCNAME\">displayLearningContent</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" ;

  /**
   * Chaine du Screen1 par défaut
   */
  public static String SCREEN1 = "<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
      "  <block type=\"component_event\" id=\"1\" x=\"-2\" y=\"-621\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"StartButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">StartButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"13\">\n" +
      "        <mutation name=\"FermeEcran\"></mutation>\n" +
      "        <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "        <next>\n" +
      "          <block type=\"controls_openAnotherScreen\" id=\"2\" inline=\"false\">\n" +
      "            <value name=\"SCREEN\">\n" +
      "              <block type=\"text\" id=\"3\">\n" +
      "                <field name=\"TEXT\">Activite_1</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"procedures_defnoreturn\" id=\"8\" x=\"539\" y=\"-618\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">FermeEcran</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_closeScreen\" id=\"9\"></block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"component_event\" id=\"4\" x=\"-2\" y=\"-502\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"QuitButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">QuitButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"controls_closeApplication\" id=\"5\"></block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  /**
   * Chaine initiale d'un écran
   * */
  public static String INITIAL_FORM =
      "<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"16\" x=\"-1157\" y=\"-607\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">displayClue</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_set_get\" id=\"17\" inline=\"false\">\n" +
          "        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
          "        <field name=\"PROP\">Visible</field>\n" +
          "        <value name=\"VALUE\">\n" +
          "          <block type=\"logic_boolean\" id=\"18\">\n" +
          "            <field name=\"BOOL\">TRUE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <next>\n" +
          "          <block type=\"component_set_get\" id=\"19\" inline=\"false\">\n" +
          "            <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
          "            <field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
          "            <field name=\"PROP\">Visible</field>\n" +
          "            <value name=\"VALUE\">\n" +
          "              <block type=\"logic_boolean\" id=\"20\">\n" +
          "                <field name=\"BOOL\">TRUE</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <next>\n" +
          "              <block type=\"component_set_get\" id=\"21\" inline=\"false\">\n" +
          "                <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
          "                <field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
          "                <field name=\"PROP\">Visible</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"logic_boolean\" id=\"22\">\n" +
          "                    <field name=\"BOOL\">TRUE</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"component_set_get\" id=\"23\" inline=\"false\">\n" +
          "                    <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
          "                    <field name=\"PROP\">Visible</field>\n" +
          "                    <value name=\"VALUE\">\n" +
          "                      <block type=\"logic_boolean\" id=\"24\">\n" +
          "                        <field name=\"BOOL\">FALSE</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <next>\n" +
          "                      <block type=\"component_set_get\" id=\"25\" inline=\"false\">\n" +
          "                        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
          "                        <field name=\"PROP\">Visible</field>\n" +
          "                        <value name=\"VALUE\">\n" +
          "                          <block type=\"logic_boolean\" id=\"26\">\n" +
          "                            <field name=\"BOOL\">FALSE</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <next>\n" +
          "                          <block type=\"component_set_get\" id=\"27\" inline=\"false\">\n" +
          "                            <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
          "                            <field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
          "                            <field name=\"PROP\">Visible</field>\n" +
          "                            <value name=\"VALUE\">\n" +
          "                              <block type=\"logic_boolean\" id=\"28\">\n" +
          "                                <field name=\"BOOL\">FALSE</field>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <next>\n" +
          "                              <block type=\"component_set_get\" id=\"29\" inline=\"false\">\n" +
          "                                <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
          "                                <field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
          "                                <field name=\"PROP\">Visible</field>\n" +
          "                                <value name=\"VALUE\">\n" +
          "                                  <block type=\"logic_boolean\" id=\"30\">\n" +
          "                                    <field name=\"BOOL\">FALSE</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                                <next>\n" +
          "                                  <block type=\"component_set_get\" id=\"31\" inline=\"false\">\n" +
          "                                    <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
          "                                    <field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
          "                                    <field name=\"PROP\">Visible</field>\n" +
          "                                    <value name=\"VALUE\">\n" +
          "                                      <block type=\"logic_boolean\" id=\"32\">\n" +
          "                                        <field name=\"BOOL\">FALSE</field>\n" +
          "                                      </block>\n" +
          "                                    </value>\n" +
          "                                    <next>\n" +
          "                                      <block type=\"component_set_get\" id=\"33\" inline=\"false\">\n" +
          "                                        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
          "                                        <field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
          "                                        <field name=\"PROP\">Visible</field>\n" +
          "                                        <value name=\"VALUE\">\n" +
          "                                          <block type=\"logic_boolean\" id=\"34\">\n" +
          "                                            <field name=\"BOOL\">FALSE</field>\n" +
          "                                          </block>\n" +
          "                                        </value>\n" +
          "                                      </block>\n" +
          "                                    </next>\n" +
          "                                  </block>\n" +
          "                                </next>\n" +
          "                              </block>\n" +
          "                            </next>\n" +
          "                          </block>\n" +
          "                        </next>\n" +
          "                      </block>\n" +
          "                    </next>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"50\" x=\"-574\" y=\"-610\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">displayLearningContent</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_set_get\" id=\"51\" inline=\"false\">\n" +
          "        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
          "        <field name=\"PROP\">Visible</field>\n" +
          "        <value name=\"VALUE\">\n" +
          "          <block type=\"logic_boolean\" id=\"52\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <next>\n" +
          "          <block type=\"component_set_get\" id=\"53\" inline=\"false\">\n" +
          "            <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
          "            <field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
          "            <field name=\"PROP\">Visible</field>\n" +
          "            <value name=\"VALUE\">\n" +
          "              <block type=\"logic_boolean\" id=\"54\">\n" +
          "                <field name=\"BOOL\">FALSE</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <next>\n" +
          "              <block type=\"component_set_get\" id=\"55\" inline=\"false\">\n" +
          "                <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
          "                <field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
          "                <field name=\"PROP\">Visible</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"logic_boolean\" id=\"56\">\n" +
          "                    <field name=\"BOOL\">FALSE</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"component_set_get\" id=\"57\" inline=\"false\">\n" +
          "                    <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
          "                    <field name=\"PROP\">Visible</field>\n" +
          "                    <value name=\"VALUE\">\n" +
          "                      <block type=\"logic_boolean\" id=\"58\">\n" +
          "                        <field name=\"BOOL\">TRUE</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <next>\n" +
          "                      <block type=\"component_set_get\" id=\"59\" inline=\"false\">\n" +
          "                        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
          "                        <field name=\"PROP\">Visible</field>\n" +
          "                        <value name=\"VALUE\">\n" +
          "                          <block type=\"logic_boolean\" id=\"60\">\n" +
          "                            <field name=\"BOOL\">TRUE</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <next>\n" +
          "                          <block type=\"component_set_get\" id=\"61\" inline=\"false\">\n" +
          "                            <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
          "                            <field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
          "                            <field name=\"PROP\">Visible</field>\n" +
          "                            <value name=\"VALUE\">\n" +
          "                              <block type=\"logic_boolean\" id=\"62\">\n" +
          "                                <field name=\"BOOL\">TRUE</field>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <next>\n" +
          "                              <block type=\"component_set_get\" id=\"63\" inline=\"false\">\n" +
          "                                <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
          "                                <field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
          "                                <field name=\"PROP\">Visible</field>\n" +
          "                                <value name=\"VALUE\">\n" +
          "                                  <block type=\"logic_boolean\" id=\"64\">\n" +
          "                                    <field name=\"BOOL\">FALSE</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                                <next>\n" +
          "                                  <block type=\"component_set_get\" id=\"65\" inline=\"false\">\n" +
          "                                    <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
          "                                    <field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
          "                                    <field name=\"PROP\">Visible</field>\n" +
          "                                    <value name=\"VALUE\">\n" +
          "                                      <block type=\"logic_boolean\" id=\"66\">\n" +
          "                                        <field name=\"BOOL\">FALSE</field>\n" +
          "                                      </block>\n" +
          "                                    </value>\n" +
          "                                    <next>\n" +
          "                                      <block type=\"component_set_get\" id=\"67\" inline=\"false\">\n" +
          "                                        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
          "                                        <field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
          "                                        <field name=\"PROP\">Visible</field>\n" +
          "                                        <value name=\"VALUE\">\n" +
          "                                          <block type=\"logic_boolean\" id=\"68\">\n" +
          "                                            <field name=\"BOOL\">FALSE</field>\n" +
          "                                          </block>\n" +
          "                                        </value>\n" +
          "                                      </block>\n" +
          "                                    </next>\n" +
          "                                  </block>\n" +
          "                                </next>\n" +
          "                              </block>\n" +
          "                            </next>\n" +
          "                          </block>\n" +
          "                        </next>\n" +
          "                      </block>\n" +
          "                    </next>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"91\" x=\"-5\" y=\"-621\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">displayTask</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_set_get\" id=\"92\" inline=\"false\">\n" +
          "        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
          "        <field name=\"PROP\">Visible</field>\n" +
          "        <value name=\"VALUE\">\n" +
          "          <block type=\"logic_boolean\" id=\"93\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <next>\n" +
          "          <block type=\"component_set_get\" id=\"94\" inline=\"false\">\n" +
          "            <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
          "            <field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
          "            <field name=\"PROP\">Visible</field>\n" +
          "            <value name=\"VALUE\">\n" +
          "              <block type=\"logic_boolean\" id=\"95\">\n" +
          "                <field name=\"BOOL\">FALSE</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <next>\n" +
          "              <block type=\"component_set_get\" id=\"96\" inline=\"false\">\n" +
          "                <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
          "                <field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
          "                <field name=\"PROP\">Visible</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"logic_boolean\" id=\"97\">\n" +
          "                    <field name=\"BOOL\">FALSE</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"component_set_get\" id=\"98\" inline=\"false\">\n" +
          "                    <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
          "                    <field name=\"PROP\">Visible</field>\n" +
          "                    <value name=\"VALUE\">\n" +
          "                      <block type=\"logic_boolean\" id=\"99\">\n" +
          "                        <field name=\"BOOL\">FALSE</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <next>\n" +
          "                      <block type=\"component_set_get\" id=\"100\" inline=\"false\">\n" +
          "                        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
          "                        <field name=\"PROP\">Visible</field>\n" +
          "                        <value name=\"VALUE\">\n" +
          "                          <block type=\"logic_boolean\" id=\"101\">\n" +
          "                            <field name=\"BOOL\">FALSE</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <next>\n" +
          "                          <block type=\"component_set_get\" id=\"102\" inline=\"false\">\n" +
          "                            <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
          "                            <field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
          "                            <field name=\"PROP\">Visible</field>\n" +
          "                            <value name=\"VALUE\">\n" +
          "                              <block type=\"logic_boolean\" id=\"103\">\n" +
          "                                <field name=\"BOOL\">FALSE</field>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <next>\n" +
          "                              <block type=\"component_set_get\" id=\"104\" inline=\"false\">\n" +
          "                                <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
          "                                <field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
          "                                <field name=\"PROP\">Visible</field>\n" +
          "                                <value name=\"VALUE\">\n" +
          "                                  <block type=\"logic_boolean\" id=\"105\">\n" +
          "                                    <field name=\"BOOL\">TRUE</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                                <next>\n" +
          "                                  <block type=\"component_set_get\" id=\"106\" inline=\"false\">\n" +
          "                                    <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
          "                                    <field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
          "                                    <field name=\"PROP\">Visible</field>\n" +
          "                                    <value name=\"VALUE\">\n" +
          "                                      <block type=\"logic_boolean\" id=\"107\">\n" +
          "                                        <field name=\"BOOL\">TRUE</field>\n" +
          "                                      </block>\n" +
          "                                    </value>\n" +
          "                                    <next>\n" +
          "                                      <block type=\"component_set_get\" id=\"108\" inline=\"false\">\n" +
          "                                        <mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
          "                                        <field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
          "                                        <field name=\"PROP\">Visible</field>\n" +
          "                                        <value name=\"VALUE\">\n" +
          "                                          <block type=\"logic_boolean\" id=\"109\">\n" +
          "                                            <field name=\"BOOL\">TRUE</field>\n" +
          "                                          </block>\n" +
          "                                        </value>\n" +
          "                                      </block>\n" +
          "                                    </next>\n" +
          "                                  </block>\n" +
          "                                </next>\n" +
          "                              </block>\n" +
          "                            </next>\n" +
          "                          </block>\n" +
          "                        </next>\n" +
          "                      </block>\n" +
          "                    </next>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"1\" x=\"-1154\" y=\"-306\">\n" +
          "    <mutation component_type=\"Form\" instance_name=\""+CURRENT_FORM+"\" event_name=\"Initialize\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">"+CURRENT_FORM+"</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"component_set_get\" id=\"2\" inline=\"false\">\n" +
          "        <mutation component_type=\"Label\" set_or_get=\"set\" property_name=\"Text\" is_generic=\"false\" instance_name=\"PointsLabel\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">PointsLabel</field>\n" +
          "        <field name=\"PROP\">Text</field>\n" +
          "        <value name=\"VALUE\">\n" +
          "          <block type=\"controls_getStartValue\" id=\"3\"></block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"4\" inline=\"false\" x=\"-590\" y=\"-283\">\n" +
          "    <field name=\"NAME\">score</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"controls_getStartValue\" id=\"5\"></block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"110\" x=\"-11\" y=\"-293\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">FermeEcran</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"controls_closeScreen\" id=\"111\"></block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"6\" x=\"-1162\" y=\"-216\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"ClueMenuButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">ClueMenuButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"7\">\n" +
          "        <mutation name=\"FermeEcran\"></mutation>\n" +
          "        <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "        <next>\n" +
          "          <block type=\"controls_openAnotherScreen\" id=\"8\" inline=\"false\">\n" +
          "            <value name=\"SCREEN\">\n" +
          "              <block type=\"text\" id=\"9\">\n" +
          "                <field name=\"TEXT\">Screen1</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"10\" x=\"-729\" y=\"-210\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"LearningContentMenuButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">LearningContentMenuButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"11\">\n" +
          "        <mutation name=\"FermeEcran\"></mutation>\n" +
          "        <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "        <next>\n" +
          "          <block type=\"controls_openAnotherScreen\" id=\"12\" inline=\"false\">\n" +
          "            <value name=\"SCREEN\">\n" +
          "              <block type=\"text\" id=\"13\">\n" +
          "                <field name=\"TEXT\">Screen1</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"35\" x=\"-303\" y=\"-205\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskMenuButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskMenuButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"36\">\n" +
          "        <mutation name=\"FermeEcran\"></mutation>\n" +
          "        <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "        <next>\n" +
          "          <block type=\"controls_openAnotherScreen\" id=\"37\" inline=\"false\">\n" +
          "            <value name=\"SCREEN\">\n" +
          "              <block type=\"text\" id=\"38\">\n" +
          "                <field name=\"TEXT\">Screen1</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"14\" x=\"-1159\" y=\"-88\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"15\">\n" +
          "        <mutation name=\"displayLearningContent\"></mutation>\n" +
          "        <field name=\"PROCNAME\">displayLearningContent</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"39\" x=\"-829\" y=\"-91\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"LearningContentPreviousButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">LearningContentPreviousButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"40\">\n" +
          "        <mutation name=\"displayClue\"></mutation>\n" +
          "        <field name=\"PROCNAME\">displayClue</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"46\" x=\"-469\" y=\"-89\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"LearningContentNextButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">LearningContentNextButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"47\">\n" +
          "        <mutation name=\"displayTask\"></mutation>\n" +
          "        <field name=\"PROCNAME\">displayTask</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"48\" x=\"-136\" y=\"-90\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskPreviousButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskPreviousButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"49\">\n" +
          "        <mutation name=\"displayLearningContent\"></mutation>\n" +
          "        <field name=\"PROCNAME\">displayLearningContent</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
      "  <yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  public static String SOUND_COMPONENT = "  <block type=\"component_event\" id=\"60\" x=\"-10\" y=\"90\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"SoundButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">SoundButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"component_method\" id=\"61\">\n" +
      "        <mutation component_type=\"Sound\" method_name=\"Play\" is_generic=\"false\" instance_name=\"Son1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">HPA_98</field>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n";

  public static String TAKE_PICTURE_COMPONENT =
      "<block type=\"component_event\" id=\"69\" x=\"-1163\" y=\"37\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"70\">\n" +
          "        <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"78\" x=\"-846\" y=\"36\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"CameraButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">CameraButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"component_method\" id=\"79\">\n" +
          "        <mutation component_type=\"Camera\" method_name=\"TakePicture\" is_generic=\"false\" instance_name=\"Prise_d_image1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"80\" x=\"-441\" y=\"28\">\n" +
          "    <mutation component_type=\"Camera\" instance_name=\"Prise_d_image1\" event_name=\"AfterPicture\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"81\">\n" +
          "        <mutation name=\"bonneReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"41\" x=\"-1157\" y=\"143\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"42\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"43\">\n" +
          "            <field name=\"TEXT\">Veuillez prendre la photo d'abord !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"44\">\n" +
          "            <field name=\"TEXT\">Erreur</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"45\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"71\" x=\"-433\" y=\"130\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"72\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"73\">\n" +
          "            <field name=\"TEXT\">Vous allez passer à l'activité suivante</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"74\">\n" +
          "            <field name=\"TEXT\">Bravo !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"75\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"76\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"77\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"82\" x=\"-1150\" y=\"321\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"83\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_compare\" id=\"84\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"85\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"86\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"87\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"88\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"89\">\n" +
          "                    <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"90\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" ;

  public static String QCM_COMPONENT =
      "<block type=\"component_event\" id=\"362\" x=\"-1174\" y=\"28\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"lexical_variable_set\" id=\"539\" inline=\"false\">\n" +
          "        <field name=\"VAR\">global QCM</field>\n" +
          "        <value name=\"VALUE\">\n" +
          "          <block type=\"lists_create_with\" id=\"561\" inline=\"false\">\n" +
          "            <mutation items=\"4\"></mutation>\n" +
          "            <value name=\"ADD0\">\n" +
          "              <block type=\"component_component_block\" id=\"586\">\n" +
          "                <mutation component_type=\"CheckBox\" instance_name=\"Case_à_cocher1\"></mutation>\n" +
          "                <field name=\"COMPONENT_SELECTOR\">Case_à_cocher1</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"ADD1\">\n" +
          "              <block type=\"component_component_block\" id=\"593\">\n" +
          "                <mutation component_type=\"CheckBox\" instance_name=\"Case_à_cocher2\"></mutation>\n" +
          "                <field name=\"COMPONENT_SELECTOR\">Case_à_cocher2</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"ADD2\">\n" +
          "              <block type=\"component_component_block\" id=\"594\">\n" +
          "                <mutation component_type=\"CheckBox\" instance_name=\"Case_à_cocher3\"></mutation>\n" +
          "                <field name=\"COMPONENT_SELECTOR\">Case_à_cocher3</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"ADD3\">\n" +
          "              <block type=\"component_component_block\" id=\"595\">\n" +
          "                <mutation component_type=\"CheckBox\" instance_name=\"Case_à_cocher4\"></mutation>\n" +
          "                <field name=\"COMPONENT_SELECTOR\">Case_à_cocher4</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <next>\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"363\">\n" +
          "            <mutation name=\"vérifier_coché\"></mutation>\n" +
          "            <field name=\"PROCNAME\">vérifier_coché</field>\n" +
          "            <next>\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"364\">\n" +
          "                <mutation name=\"calculer_Score\"></mutation>\n" +
          "                <field name=\"PROCNAME\">calculer_Score</field>\n" +
          "                <next>\n" +
          "                  <block type=\"lexical_variable_set\" id=\"365\" inline=\"false\">\n" +
          "                    <field name=\"VAR\">global coché</field>\n" +
          "                    <value name=\"VALUE\">\n" +
          "                      <block type=\"logic_boolean\" id=\"366\">\n" +
          "                        <field name=\"BOOL\">FALSE</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <next>\n" +
          "                      <block type=\"lexical_variable_set\" id=\"367\" inline=\"false\">\n" +
          "                        <field name=\"VAR\">global reponses</field>\n" +
          "                        <value name=\"VALUE\">\n" +
          "                          <block type=\"math_add\" id=\"368\" inline=\"true\">\n" +
          "                            <mutation items=\"2\"></mutation>\n" +
          "                            <value name=\"NUM0\">\n" +
          "                              <block type=\"lexical_variable_get\" id=\"369\">\n" +
          "                                <field name=\"VAR\">global reponses</field>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <value name=\"NUM1\">\n" +
          "                              <block type=\"math_number\" id=\"370\">\n" +
          "                                <field name=\"NUM\">1</field>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                      </block>\n" +
          "                    </next>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"371\" inline=\"false\" x=\"-335\" y=\"113\">\n" +
          "    <field name=\"NAME\">QCM</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"lists_create_with\" id=\"372\">\n" +
          "        <mutation items=\"0\"></mutation>\n" +
          "      </block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"373\" inline=\"false\" x=\"-337\" y=\"140\">\n" +
          "    <field name=\"NAME\">coché</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"logic_false\" id=\"374\">\n" +
          "        <field name=\"BOOL\">FALSE</field>\n" +
          "      </block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"375\" inline=\"false\" x=\"-335\" y=\"167\">\n" +
          "    <field name=\"NAME\">score</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"math_number\" id=\"376\">\n" +
          "        <field name=\"NUM\">0</field>\n" +
          "      </block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"463\" inline=\"false\" x=\"-336\" y=\"193\">\n" +
          "    <field name=\"NAME\">reponses</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"math_number\" id=\"464\">\n" +
          "        <field name=\"NUM\">0</field>\n" +
          "      </block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"465\" x=\"-1272\" y=\"296\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"466\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"467\">\n" +
          "            <field name=\"TEXT\">Essaies encore de trouver la réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"468\">\n" +
          "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"469\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"470\" x=\"-471\" y=\"293\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"471\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"472\">\n" +
          "            <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"473\">\n" +
          "            <field name=\"TEXT\">Bonne réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"474\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"475\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"476\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"377\" x=\"-1278\" y=\"464\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">vérifier_coché</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"controls_forEach\" id=\"378\" inline=\"false\">\n" +
          "        <field name=\"VAR\">élément</field>\n" +
          "        <value name=\"LIST\">\n" +
          "          <block type=\"lexical_variable_get\" id=\"379\">\n" +
          "            <field name=\"VAR\">global QCM</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO\">\n" +
          "          <block type=\"controls_if\" id=\"380\" inline=\"false\">\n" +
          "            <value name=\"IF0\">\n" +
          "              <block type=\"component_set_get\" id=\"381\" inline=\"false\">\n" +
          "                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
          "                <field name=\"PROP\">Checked</field>\n" +
          "                <value name=\"COMPONENT\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"382\">\n" +
          "                    <field name=\"VAR\">élément</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO0\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"383\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global coché</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"logic_boolean\" id=\"384\">\n" +
          "                    <field name=\"BOOL\">TRUE</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"525\" x=\"-473\" y=\"510\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"526\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_compare\" id=\"527\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"528\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"529\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"530\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"531\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"532\">\n" +
          "                    <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"533\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"385\" x=\"-1281\" y=\"642\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">calculer_Score</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"controls_if\" id=\"386\" inline=\"false\">\n" +
          "        <mutation else=\"1\"></mutation>\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"lexical_variable_get\" id=\"387\">\n" +
          "            <field name=\"VAR\">global coché</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"controls_if\" id=\"388\" inline=\"false\">\n" +
          "            <mutation elseif=\"3\"></mutation>\n" +
          "            <value name=\"IF0\">\n" +
          "              <block type=\"math_compare\" id=\"389\" inline=\"true\">\n" +
          "                <field name=\"OP\">EQ</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"390\">\n" +
          "                    <field name=\"VAR\">global reponses</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"math_number\" id=\"391\">\n" +
          "                    <field name=\"NUM\">0</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO0\">\n" +
          "              <block type=\"controls_forEach\" id=\"392\" inline=\"false\">\n" +
          "                <field name=\"VAR\">élément</field>\n" +
          "                <value name=\"LIST\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"393\">\n" +
          "                    <field name=\"VAR\">global QCM</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <statement name=\"DO\">\n" +
          "                  <block type=\"controls_if\" id=\"394\" inline=\"false\">\n" +
          "                    <value name=\"IF0\">\n" +
          "                      <block type=\"component_set_get\" id=\"395\" inline=\"false\">\n" +
          "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
          "                        <field name=\"PROP\">Checked</field>\n" +
          "                        <value name=\"COMPONENT\">\n" +
          "                          <block type=\"lexical_variable_get\" id=\"396\">\n" +
          "                            <field name=\"VAR\">élément</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <statement name=\"DO0\">\n" +
          "                      <block type=\"controls_if\" id=\"397\" inline=\"false\">\n" +
          "                        <mutation else=\"1\"></mutation>\n" +
          "                        <value name=\"IF0\">\n" +
          "                          <block type=\"logic_compare\" id=\"398\" inline=\"true\">\n" +
          "                            <field name=\"OP\">EQ</field>\n" +
          "                            <value name=\"A\">\n" +
          "                              <block type=\"component_set_get\" id=\"399\" inline=\"false\">\n" +
          "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
          "                                <field name=\"PROP\">Text</field>\n" +
          "                                <value name=\"COMPONENT\">\n" +
          "                                  <block type=\"lexical_variable_get\" id=\"400\">\n" +
          "                                    <field name=\"VAR\">élément</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <value name=\"B\">\n" +
          "                              <block type=\"component_set_get\" id=\"401\">\n" +
          "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                                <field name=\"PROP\">Text</field>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <statement name=\"DO0\">\n" +
          "                          <block type=\"lexical_variable_set\" id=\"402\" inline=\"false\">\n" +
          "                            <field name=\"VAR\">global score</field>\n" +
          "                            <value name=\"VALUE\">\n" +
          "                              <block type=\"math_add\" id=\"403\" inline=\"true\">\n" +
          "                                <mutation items=\"2\"></mutation>\n" +
          "                                <value name=\"NUM0\">\n" +
          "                                  <block type=\"lexical_variable_get\" id=\"404\">\n" +
          "                                    <field name=\"VAR\">global score</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                                <value name=\"NUM1\">\n" +
          "                                  <block type=\"math_number\" id=\"405\">\n" +
          "                                    <field name=\"NUM\">50</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <next>\n" +
          "                              <block type=\"procedures_callnoreturn\" id=\"406\">\n" +
          "                                <mutation name=\"bonneReponse\"></mutation>\n" +
          "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                              </block>\n" +
          "                            </next>\n" +
          "                          </block>\n" +
          "                        </statement>\n" +
          "                        <statement name=\"ELSE\">\n" +
          "                          <block type=\"procedures_callnoreturn\" id=\"407\">\n" +
          "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "                          </block>\n" +
          "                        </statement>\n" +
          "                      </block>\n" +
          "                    </statement>\n" +
          "                  </block>\n" +
          "                </statement>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <value name=\"IF1\">\n" +
          "              <block type=\"math_compare\" id=\"408\" inline=\"true\">\n" +
          "                <field name=\"OP\">EQ</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"409\">\n" +
          "                    <field name=\"VAR\">global reponses</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"math_number\" id=\"410\">\n" +
          "                    <field name=\"NUM\">1</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO1\">\n" +
          "              <block type=\"controls_forEach\" id=\"411\" inline=\"false\">\n" +
          "                <field name=\"VAR\">élément</field>\n" +
          "                <value name=\"LIST\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"412\">\n" +
          "                    <field name=\"VAR\">global QCM</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <statement name=\"DO\">\n" +
          "                  <block type=\"controls_if\" id=\"413\" inline=\"false\">\n" +
          "                    <value name=\"IF0\">\n" +
          "                      <block type=\"component_set_get\" id=\"414\" inline=\"false\">\n" +
          "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
          "                        <field name=\"PROP\">Checked</field>\n" +
          "                        <value name=\"COMPONENT\">\n" +
          "                          <block type=\"lexical_variable_get\" id=\"415\">\n" +
          "                            <field name=\"VAR\">élément</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <statement name=\"DO0\">\n" +
          "                      <block type=\"controls_if\" id=\"416\" inline=\"false\">\n" +
          "                        <mutation else=\"1\"></mutation>\n" +
          "                        <value name=\"IF0\">\n" +
          "                          <block type=\"logic_compare\" id=\"417\" inline=\"true\">\n" +
          "                            <field name=\"OP\">EQ</field>\n" +
          "                            <value name=\"A\">\n" +
          "                              <block type=\"component_set_get\" id=\"418\" inline=\"false\">\n" +
          "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
          "                                <field name=\"PROP\">Text</field>\n" +
          "                                <value name=\"COMPONENT\">\n" +
          "                                  <block type=\"lexical_variable_get\" id=\"419\">\n" +
          "                                    <field name=\"VAR\">élément</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <value name=\"B\">\n" +
          "                              <block type=\"component_set_get\" id=\"420\">\n" +
          "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                                <field name=\"PROP\">Text</field>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <statement name=\"DO0\">\n" +
          "                          <block type=\"lexical_variable_set\" id=\"421\" inline=\"false\">\n" +
          "                            <field name=\"VAR\">global score</field>\n" +
          "                            <value name=\"VALUE\">\n" +
          "                              <block type=\"math_add\" id=\"422\" inline=\"true\">\n" +
          "                                <mutation items=\"2\"></mutation>\n" +
          "                                <value name=\"NUM0\">\n" +
          "                                  <block type=\"lexical_variable_get\" id=\"423\">\n" +
          "                                    <field name=\"VAR\">global score</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                                <value name=\"NUM1\">\n" +
          "                                  <block type=\"math_number\" id=\"424\">\n" +
          "                                    <field name=\"NUM\">30</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <next>\n" +
          "                              <block type=\"procedures_callnoreturn\" id=\"425\">\n" +
          "                                <mutation name=\"bonneReponse\"></mutation>\n" +
          "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                              </block>\n" +
          "                            </next>\n" +
          "                          </block>\n" +
          "                        </statement>\n" +
          "                        <statement name=\"ELSE\">\n" +
          "                          <block type=\"procedures_callnoreturn\" id=\"426\">\n" +
          "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "                          </block>\n" +
          "                        </statement>\n" +
          "                      </block>\n" +
          "                    </statement>\n" +
          "                  </block>\n" +
          "                </statement>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <value name=\"IF2\">\n" +
          "              <block type=\"math_compare\" id=\"427\" inline=\"true\">\n" +
          "                <field name=\"OP\">EQ</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"428\">\n" +
          "                    <field name=\"VAR\">global reponses</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"math_number\" id=\"429\">\n" +
          "                    <field name=\"NUM\">2</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO2\">\n" +
          "              <block type=\"controls_forEach\" id=\"430\" inline=\"false\">\n" +
          "                <field name=\"VAR\">élément</field>\n" +
          "                <value name=\"LIST\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"431\">\n" +
          "                    <field name=\"VAR\">global QCM</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <statement name=\"DO\">\n" +
          "                  <block type=\"controls_if\" id=\"432\" inline=\"false\">\n" +
          "                    <value name=\"IF0\">\n" +
          "                      <block type=\"component_set_get\" id=\"433\" inline=\"false\">\n" +
          "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
          "                        <field name=\"PROP\">Checked</field>\n" +
          "                        <value name=\"COMPONENT\">\n" +
          "                          <block type=\"lexical_variable_get\" id=\"434\">\n" +
          "                            <field name=\"VAR\">élément</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <statement name=\"DO0\">\n" +
          "                      <block type=\"controls_if\" id=\"435\" inline=\"false\">\n" +
          "                        <mutation else=\"1\"></mutation>\n" +
          "                        <value name=\"IF0\">\n" +
          "                          <block type=\"logic_compare\" id=\"436\" inline=\"true\">\n" +
          "                            <field name=\"OP\">EQ</field>\n" +
          "                            <value name=\"A\">\n" +
          "                              <block type=\"component_set_get\" id=\"437\" inline=\"false\">\n" +
          "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
          "                                <field name=\"PROP\">Text</field>\n" +
          "                                <value name=\"COMPONENT\">\n" +
          "                                  <block type=\"lexical_variable_get\" id=\"438\">\n" +
          "                                    <field name=\"VAR\">élément</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <value name=\"B\">\n" +
          "                              <block type=\"component_set_get\" id=\"439\">\n" +
          "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                                <field name=\"PROP\">Text</field>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <statement name=\"DO0\">\n" +
          "                          <block type=\"lexical_variable_set\" id=\"440\" inline=\"false\">\n" +
          "                            <field name=\"VAR\">global score</field>\n" +
          "                            <value name=\"VALUE\">\n" +
          "                              <block type=\"math_add\" id=\"441\" inline=\"true\">\n" +
          "                                <mutation items=\"2\"></mutation>\n" +
          "                                <value name=\"NUM0\">\n" +
          "                                  <block type=\"lexical_variable_get\" id=\"442\">\n" +
          "                                    <field name=\"VAR\">global score</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                                <value name=\"NUM1\">\n" +
          "                                  <block type=\"math_number\" id=\"443\">\n" +
          "                                    <field name=\"NUM\">10</field>\n" +
          "                                  </block>\n" +
          "                                </value>\n" +
          "                              </block>\n" +
          "                            </value>\n" +
          "                            <next>\n" +
          "                              <block type=\"procedures_callnoreturn\" id=\"444\">\n" +
          "                                <mutation name=\"bonneReponse\"></mutation>\n" +
          "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                              </block>\n" +
          "                            </next>\n" +
          "                          </block>\n" +
          "                        </statement>\n" +
          "                        <statement name=\"ELSE\">\n" +
          "                          <block type=\"procedures_callnoreturn\" id=\"445\">\n" +
          "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "                          </block>\n" +
          "                        </statement>\n" +
          "                      </block>\n" +
          "                    </statement>\n" +
          "                  </block>\n" +
          "                </statement>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <value name=\"IF3\">\n" +
          "              <block type=\"math_compare\" id=\"446\" inline=\"true\">\n" +
          "                <field name=\"OP\">EQ</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"447\">\n" +
          "                    <field name=\"VAR\">global reponses</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"math_number\" id=\"448\">\n" +
          "                    <field name=\"NUM\">3</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO3\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"449\">\n" +
          "                <mutation name=\"FermeEcran\"></mutation>\n" +
          "                <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "                <next>\n" +
          "                  <block type=\"controls_openAnotherScreenWithStartValue\" id=\"450\" inline=\"false\">\n" +
          "                    <value name=\"SCREENNAME\">\n" +
          "                      <block type=\"text\" id=\"451\">\n" +
          "                        <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"STARTVALUE\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"452\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <statement name=\"ELSE\">\n" +
          "          <block type=\"component_method\" id=\"453\" inline=\"false\">\n" +
          "            <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "            <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "            <value name=\"ARG0\">\n" +
          "              <block type=\"text\" id=\"454\">\n" +
          "                <field name=\"TEXT\">Il faut choisir une réponse pour passer à l'activité suivante !</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"ARG1\">\n" +
          "              <block type=\"text\" id=\"455\">\n" +
          "                <field name=\"TEXT\">Information</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"ARG2\">\n" +
          "              <block type=\"text\" id=\"456\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n";

  public static String FAIL_MSG =
      "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">"+FAIL_MESSAGE+"</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" ;

  public static String SUCCESS_MSG =
      "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">"+SUCCESS_MESSAGE+"</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Bonne réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n";

  public static String OPEN_QUESTION_COMPONENT =
      "<block type=\"component_event\" id=\"85\" x=\"-1161\" y=\"81\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"86\">\n" +
          "        <mutation name=\"calculer_Score\"></mutation>\n" +
          "        <field name=\"PROCNAME\">calculer_Score</field>\n" +
          "        <next>\n" +
          "          <block type=\"lexical_variable_set\" id=\"87\" inline=\"false\">\n" +
          "            <field name=\"VAR\">global reponses</field>\n" +
          "            <value name=\"VALUE\">\n" +
          "              <block type=\"math_add\" id=\"88\" inline=\"true\">\n" +
          "                <mutation items=\"2\"></mutation>\n" +
          "                <value name=\"NUM0\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"89\">\n" +
          "                    <field name=\"VAR\">global reponses</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"NUM1\">\n" +
          "                  <block type=\"math_number\" id=\"90\">\n" +
          "                    <field name=\"NUM\">1</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"91\" inline=\"false\" x=\"-555\" y=\"93\">\n" +
          "    <field name=\"NAME\">reponses</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"math_number\" id=\"92\">\n" +
          "        <field name=\"NUM\">0</field>\n" +
          "      </block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"93\" x=\"-1161\" y=\"191\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"94\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"95\">\n" +
          "            <field name=\"TEXT\">Essaies encore de trouver la réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"96\">\n" +
          "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"97\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"98\" x=\"-294\" y=\"191\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"172\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"100\">\n" +
          "            <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"101\">\n" +
          "            <field name=\"TEXT\">Bonne réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"102\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"174\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"173\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"107\" x=\"-1160\" y=\"341\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">calculer_Score</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"controls_if\" id=\"108\" inline=\"false\">\n" +
          "        <mutation elseif=\"3\"></mutation>\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"math_compare\" id=\"109\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"110\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"111\">\n" +
          "                <field name=\"NUM\">0</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"controls_if\" id=\"112\" inline=\"false\">\n" +
          "            <mutation else=\"1\"></mutation>\n" +
          "            <value name=\"IF0\">\n" +
          "              <block type=\"text_contains\" id=\"113\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"114\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"115\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO0\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"116\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"117\" inline=\"true\">\n" +
          "                    <mutation items=\"2\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"118\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"119\">\n" +
          "                        <field name=\"NUM\">50</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"120\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"121\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF1\">\n" +
          "          <block type=\"math_compare\" id=\"122\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"123\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"124\">\n" +
          "                <field name=\"NUM\">1</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO1\">\n" +
          "          <block type=\"controls_if\" id=\"125\" inline=\"false\">\n" +
          "            <mutation else=\"1\"></mutation>\n" +
          "            <value name=\"IF0\">\n" +
          "              <block type=\"text_contains\" id=\"126\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"127\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"128\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO0\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"129\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"130\" inline=\"true\">\n" +
          "                    <mutation items=\"2\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"131\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"132\">\n" +
          "                        <field name=\"NUM\">30</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"133\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"134\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF2\">\n" +
          "          <block type=\"math_compare\" id=\"135\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"136\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"137\">\n" +
          "                <field name=\"NUM\">2</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO2\">\n" +
          "          <block type=\"controls_if\" id=\"138\" inline=\"false\">\n" +
          "            <mutation else=\"1\"></mutation>\n" +
          "            <value name=\"IF0\">\n" +
          "              <block type=\"text_contains\" id=\"139\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"140\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"141\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO0\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"142\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"143\" inline=\"true\">\n" +
          "                    <mutation items=\"2\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"144\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"145\">\n" +
          "                        <field name=\"NUM\">10</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"146\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"147\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF3\">\n" +
          "          <block type=\"math_compare\" id=\"148\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"149\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"150\">\n" +
          "                <field name=\"NUM\">3</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO3\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"151\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"152\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"153\">\n" +
          "                    <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"154\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"192\" x=\"-295\" y=\"393\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"211\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_compare\" id=\"218\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"221\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"222\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"103\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"104\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"105\">\n" +
          "                    <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"106\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n";

  public static String DOUBLE_OPEN_QUESTION_COMPONENT =
      "<block type=\"component_event\" id=\"169\" x=\"-1161\" y=\"81\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"170\">\n" +
          "        <mutation name=\"calculer_Score\"></mutation>\n" +
          "        <field name=\"PROCNAME\">calculer_Score</field>\n" +
          "        <next>\n" +
          "          <block type=\"lexical_variable_set\" id=\"171\" inline=\"false\">\n" +
          "            <field name=\"VAR\">global reponses</field>\n" +
          "            <value name=\"VALUE\">\n" +
          "              <block type=\"math_add\" id=\"172\" inline=\"true\">\n" +
          "                <mutation items=\"2\"></mutation>\n" +
          "                <value name=\"NUM0\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"173\">\n" +
          "                    <field name=\"VAR\">global reponses</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"NUM1\">\n" +
          "                  <block type=\"math_number\" id=\"174\">\n" +
          "                    <field name=\"NUM\">1</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"175\" inline=\"false\" x=\"-555\" y=\"93\">\n" +
          "    <field name=\"NAME\">reponses</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"math_number\" id=\"176\">\n" +
          "        <field name=\"NUM\">0</field>\n" +
          "      </block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"177\" x=\"-1161\" y=\"191\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"178\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"179\">\n" +
          "            <field name=\"TEXT\">Essaies encore de trouver la réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"180\">\n" +
          "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"181\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"182\" x=\"-294\" y=\"191\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"183\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"184\">\n" +
          "            <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"185\">\n" +
          "            <field name=\"TEXT\">Bonne réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"186\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"187\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"188\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"189\" x=\"-1160\" y=\"341\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">calculer_Score</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"controls_if\" id=\"190\" inline=\"false\">\n" +
          "        <mutation elseif=\"3\"></mutation>\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"math_compare\" id=\"191\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"192\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"193\">\n" +
          "                <field name=\"NUM\">0</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"controls_if\" id=\"194\" inline=\"false\">\n" +
          "            <mutation else=\"1\"></mutation>\n" +
          "            <value name=\"IF0\">\n" +
          "              <block type=\"logic_operation\" id=\"252\" inline=\"true\">\n" +
          "                <field name=\"OP\">AND</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"text_contains\" id=\"195\" inline=\"false\">\n" +
          "                    <value name=\"TEXT\">\n" +
          "                      <block type=\"component_set_get\" id=\"196\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"PIECE\">\n" +
          "                      <block type=\"component_set_get\" id=\"197\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"text_contains\" id=\"253\" inline=\"false\">\n" +
          "                    <value name=\"TEXT\">\n" +
          "                      <block type=\"component_set_get\" id=\"254\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte3\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte3</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"PIECE\">\n" +
          "                      <block type=\"component_set_get\" id=\"255\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte4\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte4</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO0\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"198\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"199\" inline=\"true\">\n" +
          "                    <mutation items=\"2\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"200\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"201\">\n" +
          "                        <field name=\"NUM\">50</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"202\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"203\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF1\">\n" +
          "          <block type=\"math_compare\" id=\"204\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"205\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"206\">\n" +
          "                <field name=\"NUM\">1</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO1\">\n" +
          "          <block type=\"controls_if\" id=\"207\" inline=\"false\">\n" +
          "            <mutation else=\"1\"></mutation>\n" +
          "            <value name=\"IF0\">\n" +
          "              <block type=\"logic_operation\" id=\"256\" inline=\"true\">\n" +
          "                <field name=\"OP\">AND</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"text_contains\" id=\"257\" inline=\"false\">\n" +
          "                    <value name=\"TEXT\">\n" +
          "                      <block type=\"component_set_get\" id=\"258\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"PIECE\">\n" +
          "                      <block type=\"component_set_get\" id=\"259\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"text_contains\" id=\"260\" inline=\"false\">\n" +
          "                    <value name=\"TEXT\">\n" +
          "                      <block type=\"component_set_get\" id=\"261\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte3\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte3</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"PIECE\">\n" +
          "                      <block type=\"component_set_get\" id=\"262\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte4\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte4</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO0\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"211\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"212\" inline=\"true\">\n" +
          "                    <mutation items=\"2\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"213\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"214\">\n" +
          "                        <field name=\"NUM\">30</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"215\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"216\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF2\">\n" +
          "          <block type=\"math_compare\" id=\"217\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"218\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"219\">\n" +
          "                <field name=\"NUM\">2</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO2\">\n" +
          "          <block type=\"controls_if\" id=\"220\" inline=\"false\">\n" +
          "            <mutation else=\"1\"></mutation>\n" +
          "            <value name=\"IF0\">\n" +
          "              <block type=\"logic_operation\" id=\"263\" inline=\"true\">\n" +
          "                <field name=\"OP\">AND</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"text_contains\" id=\"264\" inline=\"false\">\n" +
          "                    <value name=\"TEXT\">\n" +
          "                      <block type=\"component_set_get\" id=\"265\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"PIECE\">\n" +
          "                      <block type=\"component_set_get\" id=\"266\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"text_contains\" id=\"267\" inline=\"false\">\n" +
          "                    <value name=\"TEXT\">\n" +
          "                      <block type=\"component_set_get\" id=\"268\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte3\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte3</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"PIECE\">\n" +
          "                      <block type=\"component_set_get\" id=\"269\">\n" +
          "                        <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte4\"></mutation>\n" +
          "                        <field name=\"COMPONENT_SELECTOR\">Zone_de_texte4</field>\n" +
          "                        <field name=\"PROP\">Text</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO0\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"224\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"225\" inline=\"true\">\n" +
          "                    <mutation items=\"2\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"226\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"227\">\n" +
          "                        <field name=\"NUM\">10</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"228\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"229\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF3\">\n" +
          "          <block type=\"math_compare\" id=\"230\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"231\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"232\">\n" +
          "                <field name=\"NUM\">3</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO3\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"233\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"234\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"235\">\n" +
          "                    <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"236\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"237\" x=\"-296\" y=\"477\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"238\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_compare\" id=\"239\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"240\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"241\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"242\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"243\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"244\">\n" +
          "                    <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"245\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>";

  public static String WEB_VIEWER_COMPONENT= "<block type=\"component_event\" id=\"120\" x=\"-705\" y=\"88\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"BUTTON_NAME\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">BUTTON_NAME</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"component_set_get\" id=\"148\" inline=\"false\">\n" +
      "        <mutation component_type=\"ActivityStarter\" set_or_get=\"set\" property_name=\"Action\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "        <field name=\"PROP\">Action</field>\n" +
      "        <value name=\"VALUE\">\n" +
      "          <block type=\"text\" id=\"226\">\n" +
      "            <field name=\"TEXT\">android.intent.action.VIEW</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <next>\n" +
      "          <block type=\"component_set_get\" id=\"176\" inline=\"false\">\n" +
      "            <mutation component_type=\"ActivityStarter\" set_or_get=\"set\" property_name=\"DataUri\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "            <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "            <field name=\"PROP\">DataUri</field>\n" +
      "            <value name=\"VALUE\">\n" +
      "              <block type=\"text\" id=\"227\">\n" +
      "                <field name=\"TEXT\">WEB_LINK</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <next>\n" +
      "              <block type=\"component_method\" id=\"211\">\n" +
      "                <mutation component_type=\"ActivityStarter\" method_name=\"StartActivity\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "                <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>";

  public static String GEOLOCATION_BRUT =
      "<block type=\"component_event\" id=\"1\" x=\"-878\" y=\"-542\">\n" +
          "    <mutation component_type=\"LocationSensor\" instance_name=\"Capteur_position1\" event_name=\"LocationChanged\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Capteur_position1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"2\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_operation\" id=\"3\" inline=\"true\">\n" +
          "            <field name=\"OP\">AND</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"math_compare\" id=\"4\" inline=\"true\">\n" +
          "                <field name=\"OP\">LT</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"math_abs\" id=\"5\" inline=\"false\">\n" +
          "                    <field name=\"OP\">ABS</field>\n" +
          "                    <value name=\"NUM\">\n" +
          "                      <block type=\"math_subtract\" id=\"6\" inline=\"true\">\n" +
          "                        <value name=\"A\">\n" +
          "                          <block type=\"lexical_variable_get\" id=\"7\">\n" +
          "                            <mutation>\n" +
          "                              <eventparam name=\"latitude\"></eventparam>\n" +
          "                            </mutation>\n" +
          "                            <field name=\"VAR\">Latitude</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <value name=\"B\">\n" +
          "                          <block type=\"math_number\" id=\"8\">\n" +
          "                            <field name=\"NUM\">9944405</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"math_number\" id=\"9\">\n" +
          "                    <field name=\"NUM\">0.0001</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_compare\" id=\"10\" inline=\"true\">\n" +
          "                <field name=\"OP\">LT</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"math_abs\" id=\"11\" inline=\"false\">\n" +
          "                    <field name=\"OP\">ABS</field>\n" +
          "                    <value name=\"NUM\">\n" +
          "                      <block type=\"math_subtract\" id=\"12\" inline=\"true\">\n" +
          "                        <value name=\"A\">\n" +
          "                          <block type=\"lexical_variable_get\" id=\"13\">\n" +
          "                            <mutation>\n" +
          "                              <eventparam name=\"longitude\"></eventparam>\n" +
          "                            </mutation>\n" +
          "                            <field name=\"VAR\">Longitude</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <value name=\"B\">\n" +
          "                          <block type=\"math_number\" id=\"14\">\n" +
          "                            <field name=\"NUM\">9944405</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"math_number\" id=\"15\">\n" +
          "                    <field name=\"NUM\">0.0001</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"16\">\n" +
          "            <mutation name=\"displayLearningContent\"></mutation>\n" +
          "            <field name=\"PROCNAME\">displayLearningContent</field>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>" +
          "<block type=\"component_event\" id=\"241\" x=\"-966\" y=\"106\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"component_method\" id=\"242\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"243\">\n" +
          "            <field name=\"TEXT\">Il faut trouver le point indiqué pour afficher l'activité !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"244\">\n" +
          "            <field name=\"TEXT\">Information</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"245\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>";

  public static String OPEN_QUESTION_COMP =
      "<block type=\"global_declaration\" id=\"164\" inline=\"false\" x=\"-24\" y=\"-250\">\n" +
      "    <field name=\"NAME\">reponses</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"math_number\" id=\"165\">\n" +
      "        <field name=\"NUM\">0</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "  <block type=\"procedures_defnoreturn\" id=\"477\" x=\"-1907\" y=\"-299\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">bonneReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"478\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"479\">\n" +
      "            <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"480\">\n" +
      "            <field name=\"TEXT\">Bonne réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"481\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"482\">\n" +
      "            <mutation name=\"FermeEcran\"></mutation>\n" +
      "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "            <next>\n" +
      "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"483\" inline=\"false\">\n" +
      "                <value name=\"SCREENNAME\">\n" +
      "                  <block type=\"text\" id=\"484\">\n" +
      "                    <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"STARTVALUE\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"485\">\n" +
      "                    <field name=\"VAR\">global score</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"561\" x=\"-1932\" y=\"-66\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"562\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"563\">\n" +
      "            <field name=\"TEXT\">Tu es sur ? Essaies encore de trouver la réponse.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"564\">\n" +
      "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"565\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"321\" x=\"-1052\" y=\"-62\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"322\">\n" +
      "        <mutation name=\"calculer_Score\"></mutation>\n" +
      "        <field name=\"PROCNAME\">calculer_Score</field>\n" +
      "        <next>\n" +
      "          <block type=\"lexical_variable_set\" id=\"323\" inline=\"false\">\n" +
      "            <field name=\"VAR\">global reponses</field>\n" +
      "            <value name=\"VALUE\">\n" +
      "              <block type=\"math_add\" id=\"324\" inline=\"true\">\n" +
      "                <mutation items=\"2\"></mutation>\n" +
      "                <value name=\"NUM0\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"325\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"NUM1\">\n" +
      "                  <block type=\"math_number\" id=\"326\">\n" +
      "                    <field name=\"NUM\">1</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"641\" x=\"-1222\" y=\"316\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">calculer_Score</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_if\" id=\"642\" inline=\"false\">\n" +
      "        <mutation elseif=\"3\"></mutation>\n" +
      "        <value name=\"IF0\">\n" +
      "          <block type=\"math_compare\" id=\"643\" inline=\"true\">\n" +
      "            <field name=\"OP\">EQ</field>\n" +
      "            <value name=\"A\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"644\">\n" +
      "                <field name=\"VAR\">global reponses</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"B\">\n" +
      "              <block type=\"math_number\" id=\"645\">\n" +
      "                <field name=\"NUM\">0</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO0\">\n" +
      "          <block type=\"controls_if\" id=\"646\" inline=\"false\">\n" +
      "            <mutation else=\"1\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"text_contains\" id=\"647\" inline=\"false\">\n" +
      "                <value name=\"TEXT\">\n" +
      "                  <block type=\"component_set_get\" id=\"648\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"PIECE\">\n" +
      "                  <block type=\"component_set_get\" id=\"649\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"650\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global score</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"math_add\" id=\"651\" inline=\"true\">\n" +
      "                    <mutation items=\"2\"></mutation>\n" +
      "                    <value name=\"NUM0\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"652\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"NUM1\">\n" +
      "                      <block type=\"math_number\" id=\"653\">\n" +
      "                        <field name=\"NUM\">50</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"procedures_callnoreturn\" id=\"654\">\n" +
      "                    <mutation name=\"bonneReponse\"></mutation>\n" +
      "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <statement name=\"ELSE\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"655\">\n" +
      "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <value name=\"IF1\">\n" +
      "          <block type=\"math_compare\" id=\"656\" inline=\"true\">\n" +
      "            <field name=\"OP\">EQ</field>\n" +
      "            <value name=\"A\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"657\">\n" +
      "                <field name=\"VAR\">global reponses</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"B\">\n" +
      "              <block type=\"math_number\" id=\"658\">\n" +
      "                <field name=\"NUM\">1</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO1\">\n" +
      "          <block type=\"controls_if\" id=\"659\" inline=\"false\">\n" +
      "            <mutation else=\"1\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"text_contains\" id=\"660\" inline=\"false\">\n" +
      "                <value name=\"TEXT\">\n" +
      "                  <block type=\"component_set_get\" id=\"661\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"PIECE\">\n" +
      "                  <block type=\"component_set_get\" id=\"662\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"663\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global score</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"math_add\" id=\"664\" inline=\"true\">\n" +
      "                    <mutation items=\"2\"></mutation>\n" +
      "                    <value name=\"NUM0\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"665\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"NUM1\">\n" +
      "                      <block type=\"math_number\" id=\"666\">\n" +
      "                        <field name=\"NUM\">30</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"procedures_callnoreturn\" id=\"667\">\n" +
      "                    <mutation name=\"bonneReponse\"></mutation>\n" +
      "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <statement name=\"ELSE\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"668\">\n" +
      "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <value name=\"IF2\">\n" +
      "          <block type=\"math_compare\" id=\"669\" inline=\"true\">\n" +
      "            <field name=\"OP\">EQ</field>\n" +
      "            <value name=\"A\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"670\">\n" +
      "                <field name=\"VAR\">global reponses</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"B\">\n" +
      "              <block type=\"math_number\" id=\"671\">\n" +
      "                <field name=\"NUM\">2</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO2\">\n" +
      "          <block type=\"controls_if\" id=\"672\" inline=\"false\">\n" +
      "            <mutation else=\"1\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"text_contains\" id=\"673\" inline=\"false\">\n" +
      "                <value name=\"TEXT\">\n" +
      "                  <block type=\"component_set_get\" id=\"674\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"PIECE\">\n" +
      "                  <block type=\"component_set_get\" id=\"675\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"676\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global score</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"math_add\" id=\"677\" inline=\"true\">\n" +
      "                    <mutation items=\"2\"></mutation>\n" +
      "                    <value name=\"NUM0\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"678\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"NUM1\">\n" +
      "                      <block type=\"math_number\" id=\"679\">\n" +
      "                        <field name=\"NUM\">10</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"procedures_callnoreturn\" id=\"680\">\n" +
      "                    <mutation name=\"bonneReponse\"></mutation>\n" +
      "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <statement name=\"ELSE\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"681\">\n" +
      "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <value name=\"IF3\">\n" +
      "          <block type=\"math_compare\" id=\"682\" inline=\"true\">\n" +
      "            <field name=\"OP\">EQ</field>\n" +
      "            <value name=\"A\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"683\">\n" +
      "                <field name=\"VAR\">global reponses</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"B\">\n" +
      "              <block type=\"math_number\" id=\"684\">\n" +
      "                <field name=\"NUM\">3</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO3\">\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"685\">\n" +
      "            <mutation name=\"FermeEcran\"></mutation>\n" +
      "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "            <next>\n" +
      "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"686\" inline=\"false\">\n" +
      "                <value name=\"SCREENNAME\">\n" +
      "                  <block type=\"text\" id=\"687\">\n" +
      "                    <field name=\"TEXT\">"+NEXT_FORM+"</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"STARTVALUE\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"688\">\n" +
      "                    <field name=\"VAR\">global score</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>";

  public static String QCM_COMP = "<block type=\"global_declaration\" id=\"35\" inline=\"false\" x=\"-20\" y=\"-259\">\n" +
      "    <field name=\"NAME\">coché</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"logic_false\" id=\"36\">\n" +
      "        <field name=\"BOOL\">FALSE</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"17\" x=\"-1620\" y=\"-350\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">bonneReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"18\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"19\">\n" +
      "            <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"20\">\n" +
      "            <field name=\"TEXT\">Bonne réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"21\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"22\">\n" +
      "            <mutation name=\"FermeEcran\"></mutation>\n" +
      "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "            <next>\n" +
      "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"23\" inline=\"false\">\n" +
      "                <value name=\"SCREENNAME\">\n" +
      "                  <block type=\"text\" id=\"24\">\n" +
      "                    <field name=\"TEXT\">Activite_4</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"STARTVALUE\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"25\">\n" +
      "                    <field name=\"VAR\">global score</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"global_declaration\" id=\"37\" inline=\"false\" x=\"-23\" y=\"-221\">\n" +
      "    <field name=\"NAME\">QCM</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"lists_create_with\" id=\"38\">\n" +
      "        <mutation items=\"0\"></mutation>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"global_declaration\" id=\"39\" inline=\"false\" x=\"-30\" y=\"-186\">\n" +
      "    <field name=\"NAME\">reponses</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"math_number\" id=\"40\">\n" +
      "        <field name=\"NUM\">0</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"45\" x=\"-1599\" y=\"-65\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"46\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"47\">\n" +
      "            <field name=\"TEXT\">Tu es sur ? Essaies encore de trouver la réponse.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"48\">\n" +
      "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"49\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"50\" x=\"-9\" y=\"-118\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"51\">\n" +
      "        <mutation name=\"displayLearningContent\"></mutation>\n" +
      "        <field name=\"PROCNAME\">displayLearningContent</field>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"71\" x=\"-797\" y=\"-33\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"72\">\n" +
      "        <mutation name=\"vérifier_coché\"></mutation>\n" +
      "        <field name=\"PROCNAME\">vérifier_coché</field>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"73\">\n" +
      "            <mutation name=\"calculer_Score\"></mutation>\n" +
      "            <field name=\"PROCNAME\">calculer_Score</field>\n" +
      "            <next>\n" +
      "              <block type=\"lexical_variable_set\" id=\"74\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global coché</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"logic_boolean\" id=\"75\">\n" +
      "                    <field name=\"BOOL\">FALSE</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"lexical_variable_set\" id=\"76\" inline=\"false\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                    <value name=\"VALUE\">\n" +
      "                      <block type=\"math_add\" id=\"77\" inline=\"true\">\n" +
      "                        <mutation items=\"2\"></mutation>\n" +
      "                        <value name=\"NUM0\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"78\">\n" +
      "                            <field name=\"VAR\">global reponses</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <value name=\"NUM1\">\n" +
      "                          <block type=\"math_number\" id=\"79\">\n" +
      "                            <field name=\"NUM\">1</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"88\" x=\"-1036\" y=\"162\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">vérifier_coché</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_forEach\" id=\"89\" inline=\"false\">\n" +
      "        <field name=\"VAR\">élément</field>\n" +
      "        <value name=\"LIST\">\n" +
      "          <block type=\"lexical_variable_get\" id=\"90\">\n" +
      "            <field name=\"VAR\">global QCM</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO\">\n" +
      "          <block type=\"controls_if\" id=\"91\" inline=\"false\">\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"component_set_get\" id=\"92\" inline=\"false\">\n" +
      "                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                <field name=\"PROP\">Checked</field>\n" +
      "                <value name=\"COMPONENT\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"93\">\n" +
      "                    <field name=\"VAR\">élément</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"94\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global coché</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"logic_boolean\" id=\"95\">\n" +
      "                    <field name=\"BOOL\">TRUE</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"122\" x=\"-1304\" y=\"416\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">calculer_Score</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_if\" id=\"123\" inline=\"false\">\n" +
      "        <mutation else=\"1\"></mutation>\n" +
      "        <value name=\"IF0\">\n" +
      "          <block type=\"lexical_variable_get\" id=\"124\">\n" +
      "            <field name=\"VAR\">global coché</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO0\">\n" +
      "          <block type=\"controls_if\" id=\"125\" inline=\"false\">\n" +
      "            <mutation elseif=\"3\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"math_compare\" id=\"126\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"127\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"128\">\n" +
      "                    <field name=\"NUM\">0</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"controls_forEach\" id=\"129\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"130\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"131\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"132\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"133\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"134\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"135\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"136\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"137\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"138\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"139\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"140\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"141\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"142\">\n" +
      "                                    <field name=\"NUM\">50</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"143\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"144\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF1\">\n" +
      "              <block type=\"math_compare\" id=\"145\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"146\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"147\">\n" +
      "                    <field name=\"NUM\">1</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO1\">\n" +
      "              <block type=\"controls_forEach\" id=\"148\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"149\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"150\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"151\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"152\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"153\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"154\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"155\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"156\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"157\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"158\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"159\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"160\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"161\">\n" +
      "                                    <field name=\"NUM\">30</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"162\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"163\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF2\">\n" +
      "              <block type=\"math_compare\" id=\"164\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"165\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"166\">\n" +
      "                    <field name=\"NUM\">2</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO2\">\n" +
      "              <block type=\"controls_forEach\" id=\"167\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"168\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"169\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"170\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"171\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"172\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"173\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"174\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"175\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"176\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"177\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"178\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"179\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"180\">\n" +
      "                                    <field name=\"NUM\">10</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"181\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"182\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF3\">\n" +
      "              <block type=\"math_compare\" id=\"183\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"184\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"185\">\n" +
      "                    <field name=\"NUM\">3</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO3\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"186\">\n" +
      "                <mutation name=\"FermeEcran\"></mutation>\n" +
      "                <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "                <next>\n" +
      "                  <block type=\"controls_openAnotherScreenWithStartValue\" id=\"187\" inline=\"false\">\n" +
      "                    <value name=\"SCREENNAME\">\n" +
      "                      <block type=\"text\" id=\"188\">\n" +
      "                        <field name=\"TEXT\">Activite_4</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"STARTVALUE\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"189\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <statement name=\"ELSE\">\n" +
      "          <block type=\"component_method\" id=\"190\" inline=\"false\">\n" +
      "            <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "            <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "            <value name=\"ARG0\">\n" +
      "              <block type=\"text\" id=\"191\">\n" +
      "                <field name=\"TEXT\">Il faut choisir une réponse pour passer à l'activité suivante !</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"ARG1\">\n" +
      "              <block type=\"text\" id=\"192\">\n" +
      "                <field name=\"TEXT\">Information</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"ARG2\">\n" +
      "              <block type=\"text\" id=\"193\">\n" +
      "                <field name=\"TEXT\">Ok</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>";

  public static String FORMULA = "<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"16\" x=\"-1157\" y=\"-607\">\n" +
          "     <field name=\"TEXT\">Vous n'avez pas la formule requise pour accéder aux modes d'édition avancés.</field>\n" +
          "     <field name=\"TEXT\">Choisissez une formule parmis celles proposées pour pouvoir changer de mode.</field>\n" +
          "  </block>\n" +
          "  <yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
          "</xml>";
}
package com.google.appinventor.client;

import com.google.appinventor.client.editor.FileEditor;
import com.google.appinventor.client.editor.simple.components.*;
import com.google.appinventor.client.editor.youngandroid.YaFormEditor;
import com.google.appinventor.components.common.ComponentConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.appinventor.client.Ode.*;
import static com.google.appinventor.client.editor.simple.components.MockForm.PROPERTY_NAME_HORIZONTAL_ALIGNMENT;
import static com.google.appinventor.client.editor.simple.components.MockForm.PROPERTY_NAME_VERTICAL_ALIGNMENT;
import static com.google.appinventor.client.editor.simple.components.MockVisibleComponent.LENGTH_FILL_PARENT;

/**
 * Created by AOUS on 11/01/2017.
 */
public class JemComponents {
  protected static final Images images = Ode.getImageBundle();
  private MockHorizontalArrangement scoreContainer;
  private MockVerticalArrangement topClueContainer;
  private MockVerticalArrangement middleClueContainer;
  private MockHorizontalArrangement bottomClueContainer;
  private MockVerticalArrangement topLearningContentContainer;
  private MockVerticalArrangement middleLearningContentContainer;
  private MockHorizontalArrangement bottomLearningContentContainer;
  private MockVerticalArrangement topTaskContainer;
  private MockVerticalArrangement middleTaskContainer;
  private MockHorizontalArrangement bottomTaskContainer;
  private MockVerticalArrangement topTaskValidationContainer;
  private MockVerticalArrangement middleTaskValidationContainer;
  private MockVerticalArrangement bottomTaskValidationContainer;
  private MockTextBox textBox;
  private static JemComponents instance=null;
  public static final String CLUE_CONTAINER = "ClueContainer";
  public static final String LEARNING_CONTENT_CONTAINER = "LearningContentContainer";
  public static final String TASK_CONTAINER = "TaskContainer";
  public static final String CLUE = "MiddleClueContainer";
  public static final String LEARNING_CONTENT = "MiddleLearningContentContainer";
  public static final String TASK = "MiddleTaskContainer";
  public static final String NEW_ADDED_SCREEN = "NewMiddleContainer";
  public static final String FIRST_ADDED_SCREEN = "NewMiddleContainer1";
  public static final String SECOND_ADDED_SCREEN = "NewMiddleContainer2";
  public static final String THIRD_ADDED_SCREEN = "NewMiddleContainer3";
  private static String currentJemComponent;
  private Ode ode = Ode.getInstance();
  public static List<String> futureProperties = Arrays.asList("PoiRadius","PoiTime","XnextActivity");
  /*public static List<String> hiddenStandardProperties = Arrays.asList("AlignHorizontal","AlignVertical","AppName","CloseScreenAnimation","Icon","OpenScreenAnimation","ScreenOrientation","ShowStatusBar","Sizing","VersionCode","VersionName","TitleVisible","Visible","Scrollable","BackgroundImage","BackgroundColor","HasMargins","Height","Width","TextAlignment","RotationAngle");

   */
  public static List<String> hiddenStandardProperties = Arrays.asList("AlignHorizontal","AlignVertical","AppName","CloseScreenAnimation","Icon","OpenScreenAnimation","ScreenOrientation","ShowStatusBar","Sizing","VersionCode","VersionName","TitleVisible","Visible","Scrollable","BackgroundImage","BackgroundColor","HasMargins","TextAlignment","RotationAngle");
  public static List<String> hiddenIntermediateProperties = Arrays.asList("AppName","VersionCode","VersionName","TitleVisible","Sizing","ShowStatusBar","ScreenOrientation","CloseScreenAnimation","OpenScreenAnimation","Visible","RotationAngle");
  public static List<String> standardProperties = Arrays.asList("APoiLat","APoiLong","BackgroundColor","FontBold","FontItalic","FontSize","FontTypeface","Shape","Text","TextColor","Title");
  public static List<String> intermediateProperties = Arrays.asList("APoiLat","APoiLong","BackgroundColor","FontBold","FontItalic","FontSize","FontTypeface","Shape","Text","TextColor","Title","AlignHorizontal","BackgroundImage","Height","Image","TextAlignment","Width");
  // liste spéciale pour les propriétés des activités où certaines propriétés apparaissent son nom (ex: Height, Image, Width ... à corriger)
  public static List<String> formIntermediateProperties = Arrays.asList("AlignHorizontal","BackgroundImage");
  public static List<String> expertProperties = Arrays.asList("Visible","Scrollable");
  // des propriétés toujours invisibles (vaut mieux)
  public static List<String> invisibleProperties = Arrays.asList("HasMargins","FollowLinks","Hint","Enabled");
  // Les proporiétés à la fin sont des propriétés de composants spécifiques (ex: ListPicker), voir MockComponent.java pour plus de détails
  public static List<String> allProperties = Arrays.asList("Uuid, AlignHorizontal","Answer","AnswerSuccesMsg","AppName","APoiLat","APoiLong","BackgroundColor","BackgroundImage","CloseScreenAnimation","Column", "Checked","Enabled","FailMsg","FontBold","FontItalic","FontTypeface","FontSize","FollowLinks","HasMargins","Height","Hint","NumbersOnly","MultiLine","Name","Icon","Image","IgnoreSslErrors","OpenScreenAnimation","Row","Picture","PoiRadius","PoiTime","PromptforPermission","RotationAngle","ResultForGoodAnswer","ResultToBadAnswer","ScalePictureToFit","ScreenOrientation","Scrollable","ShowStatusBar","Sizing","TextAlignment","Text","TextColor","TitleVisible","TriggerMode","UsesLocation","VersionCode","VersionName","Visible","Width","XnextActivity","ItemBackgroundColor","ItemTextColor","Selection","ShowFilterBar","ShowFeedback","Shape");

  public JemComponents(){
    currentJemComponent=CLUE;
    instance=this;
  }

  public JemComponents(YaFormEditor formEditor){
    addGlobalScore(formEditor);
    addClueComponents(formEditor);
    addLearningContentComponents(formEditor);
    addTaskComponents(formEditor);
    hideLearningContentComponents(formEditor);
    hideTaskComponents(formEditor);
    hideTaskValidationComponents(formEditor);
    formEditor.getForm().getNextMockFormButton().setEnabled(true);
    currentJemComponent=CLUE;
    instance=this;
  }

  public void addWelcomeComponents(YaFormEditor formEditor){
    if (ode.getCurrentMode()!=EXPERT){
      String projectName = ode.getCurrentYoungAndroidProjectRootNode().getName();

      MockForm mockForm = formEditor.getForm();
      mockForm.changeProperty("Title","Accueil");
      mockForm.changeProperty("Scrollable","False");
      mockForm.changeProperty(PROPERTY_NAME_HORIZONTAL_ALIGNMENT,""+ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
      mockForm.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);

      MockLabel jemName = new MockLabel(formEditor);
      jemName.changeProperty("Text",projectName);
      jemName.changeProperty("Name","jemNameLabel");
      jemName.changeProperty("FontBold","True");
      if (projectName.length()<15)
        jemName.changeProperty("FontSize","45.0");
      else if (projectName.length()>=15 && projectName.length()<22)
        jemName.changeProperty("FontSize","30.0");
      else
        jemName.changeProperty("FontSize","22.0");

      MockLabel spaceLabel = new MockLabel(formEditor);
      spaceLabel.changeProperty("Text","SPACETEXT");
      spaceLabel.changeProperty("Name","spaceText");
      spaceLabel.changeProperty("FontSize","45.0");
      spaceLabel.changeProperty("TextColor","&H00FFFFFF");

      MockButton startButton = new MockButton(formEditor);
      startButton.changeProperty("Name","StartButton");
      startButton.changeProperty("Width","200");
      startButton.changeProperty("Text","Démarrer");

      MockButton optionsButton = new MockButton(formEditor);
      optionsButton.changeProperty("Name","OptionsButton");
      optionsButton.changeProperty("Width","200");
      optionsButton.changeProperty("Text","Options");

      MockButton quitButton = new MockButton(formEditor);
      quitButton.changeProperty("Name","QuitButton");
      quitButton.changeProperty("Width","200");
      quitButton.changeProperty("Text","Quitter");

      mockForm.addComponent(jemName);
      mockForm.addComponent(spaceLabel);
      mockForm.addComponent(startButton);
      mockForm.addComponent(optionsButton);
      mockForm.addComponent(quitButton);
    }
  }

  private void addGlobalScore(YaFormEditor formEditor){
    if (ode.getCurrentMode()==STANDARD)
    {
      MockForm mockForm = formEditor.getForm();
//      mockForm.changeProperty("Title","Activite1");

      scoreContainer = new MockHorizontalArrangement(formEditor);

      MockLabel scoreLabel = new MockLabel(formEditor);
      MockLabel scorePoints = new MockLabel(formEditor);
      MockLabel scoreLabel2 = new MockLabel(formEditor);

      editScoreContainer(formEditor,mockForm);

      scoreLabel.changeProperty("Text","Score: ");
      scoreLabel.changeProperty("Name","ScoreLabel");
      scoreLabel.changeProperty("FontBold","True");
      scoreContainer.addComponent(scoreLabel);

      scorePoints.changeProperty("Text","--");
      scorePoints.changeProperty("Name","PointsLabel");
      scoreContainer.addComponent(scorePoints);

      scoreLabel2.changeProperty("Text"," points.");
      scoreLabel2.changeProperty("Name","LabelPoints");
      scoreContainer.addComponent(scoreLabel2);

    }
  }

  private void addClueComponents(YaFormEditor formEditor){
    if (ode.getCurrentMode()!=EXPERT){
      MockForm mockForm = formEditor.getForm();
//      mockForm.changeProperty("Title","Activite1");

      topClueContainer = new MockVerticalArrangement(formEditor);
      middleClueContainer = new MockVerticalArrangement(formEditor);
      bottomClueContainer = new MockHorizontalArrangement(formEditor);

      editTopClueContainer(formEditor,mockForm);
      editMiddleClueContainer(formEditor,mockForm);
      editBottomClueContainer(formEditor,mockForm);
    }
  }

  private void addLearningContentComponents(YaFormEditor formEditor){
    if (ode.getCurrentMode()==STANDARD)
    {
      MockForm mockForm = formEditor.getForm();

      topLearningContentContainer = new MockVerticalArrangement(formEditor);
      middleLearningContentContainer = new MockVerticalArrangement(formEditor);
      bottomLearningContentContainer = new MockHorizontalArrangement(formEditor);

      editTopLearningContentContainer(formEditor,mockForm);
      editMiddleLearningContentContainer(formEditor,mockForm);
      editBottomLearningContentContainer(formEditor,mockForm);

    }
  }

  private void addTaskComponents(YaFormEditor formEditor){
    if (ode.getCurrentMode()==STANDARD) {
      MockForm mockForm = formEditor.getForm();
      topTaskContainer = new MockVerticalArrangement(formEditor);
      middleTaskContainer = new MockVerticalArrangement(formEditor);
      bottomTaskContainer = new MockHorizontalArrangement(formEditor);
      editTopTaskContainer(formEditor,mockForm);
      editMiddleTaskContainer(formEditor,mockForm);
      editBottomTaskContainer(formEditor,mockForm);
    }
  }

  private void editScoreContainer(YaFormEditor formEditor, MockForm mockForm){
    scoreContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
//    clueContainer.changeProperty("Height",""+(-(50+LENGTH_PERCENT_TAG)));
    scoreContainer.changeProperty("Height",""+LENGTH_FILL_PARENT);
    scoreContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    scoreContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);
    scoreContainer.changeProperty("Name", "ScoreContainer");
    scoreContainer.changeProperty("BackgroundColor","&HFFCCCCCC");
    mockForm.addComponent(scoreContainer);
  }

private void editTopClueContainer(YaFormEditor formEditor, MockForm mockForm){
    topClueContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
//    clueContainer.changeProperty("Height",""+(-(50+LENGTH_PERCENT_TAG)));
    topClueContainer.changeProperty("Height",""+LENGTH_FILL_PARENT);
    topClueContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    topClueContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);
    topClueContainer.changeProperty("Name", "TopClueContainer");
    topClueContainer.changeProperty("BackgroundColor","&HFFCCCCCC");
    mockForm.addComponent(topClueContainer);
    addClueInitialLabels(formEditor);
  }

  private void editMiddleClueContainer(YaFormEditor formEditor, MockForm mockForm){
    middleClueContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    middleClueContainer.changeProperty("Height","370");
    middleClueContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    middleClueContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);
    middleClueContainer.changeProperty("Name", "MiddleClueContainer");
//    middleClueContainer.changeProperty("BackgroundColor","&H00FFFFFF");
//    middleClueContainer.setIconImage(new Image(images.poiClue()));
    mockForm.addComponent(middleClueContainer);
  }

  private void editBottomClueContainer(YaFormEditor formEditor, MockForm mockForm){
    bottomClueContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    bottomClueContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    bottomClueContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_BOTTOM);
    bottomClueContainer.changeProperty("Name", "BottomClueContainer");
    mockForm.addComponent(bottomClueContainer);
    addClueButtons(formEditor);
  }

  private void editTopLearningContentContainer(YaFormEditor formEditor, MockForm mockForm){
    topLearningContentContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    topLearningContentContainer.changeProperty("Height",""+LENGTH_FILL_PARENT);
    topLearningContentContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    topLearningContentContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);
    topLearningContentContainer.changeProperty("Name", "TopLearningContentContainer");
    topLearningContentContainer.changeProperty("BackgroundColor","&HFFCCCCCC");
    mockForm.addComponent(topLearningContentContainer);
    addLearningContentInitialLabels(formEditor);
  }

  private void editMiddleLearningContentContainer(YaFormEditor formEditor, MockForm mockForm){
    middleLearningContentContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    middleLearningContentContainer.changeProperty("Height","370");
    middleLearningContentContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    middleLearningContentContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);
//    middleLearningContentContainer.changeProperty("BackgroundColor","&H00FFFFFF");
    middleLearningContentContainer.changeProperty("Name", "MiddleLearningContentContainer");
//    middleLearningContentContainer.setIconImage(new Image(images.poiLearningContent()));
    mockForm.addComponent(middleLearningContentContainer);
  }

  private void editBottomLearningContentContainer(YaFormEditor formEditor, MockForm mockForm){
    bottomLearningContentContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    bottomLearningContentContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    bottomLearningContentContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_BOTTOM);
    bottomLearningContentContainer.changeProperty("Name", "BottomLearningContentContainer");
    mockForm.addComponent(bottomLearningContentContainer);
    addLearningContentButton(formEditor);
  }

  private void editTopTaskContainer(YaFormEditor formEditor, MockForm mockForm){
    topTaskContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    topTaskContainer.changeProperty("Height",""+LENGTH_FILL_PARENT);
    topTaskContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    topTaskContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);
    topTaskContainer.changeProperty("Name", "TopTaskContainer");
    topTaskContainer.changeProperty("BackgroundColor","&HFFCCCCCC");
    mockForm.addComponent(topTaskContainer);
    addTaskInitialLabels(formEditor);
  }

  private void editTopTaskValidationContainer(YaFormEditor formEditor, MockForm mockForm){
    topTaskValidationContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    topTaskValidationContainer.changeProperty("Height",""+LENGTH_FILL_PARENT);
    topTaskValidationContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    topTaskValidationContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);
    topTaskValidationContainer.changeProperty("Name", "TopTaskValidationContainer");
    mockForm.addComponent(topTaskValidationContainer);
    addTaskValidationInitialLabels(formEditor);
  }

  private void editMiddleTaskContainer(YaFormEditor formEditor, MockForm mockForm){
    middleTaskContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    middleTaskContainer.changeProperty("Height","320");
    middleTaskContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    middleTaskContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);
//    middleTaskContainer.changeProperty("BackgroundColor","&H00FFFFFF");
    middleTaskContainer.changeProperty("Name", "MiddleTaskContainer");
//    middleTaskContainer.setIconImage(new Image(images.poiLearningContent()));
    mockForm.addComponent(middleTaskContainer);
  }

  private void editBottomTaskContainer(YaFormEditor formEditor, MockForm mockForm){
    bottomTaskContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    bottomTaskContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    bottomTaskContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_BOTTOM);
    bottomTaskContainer.changeProperty("Name", "BottomTaskContainer");
    mockForm.addComponent(bottomTaskContainer);
    addTaskButton(formEditor);
  }

  private void editMiddleTaskValidationContainer(YaFormEditor formEditor, MockForm mockForm){
    middleTaskValidationContainer.changeProperty("Width",""+LENGTH_FILL_PARENT);
    middleTaskValidationContainer.changeProperty("Height","320");
    middleTaskValidationContainer.changeProperty("AlignHorizontal",""+ ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//    middleTaskValidationContainer.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_CENTER_VERTICAL);
//    middleTaskValidationContainer.changeProperty("BackgroundColor","&H00FFFFFF");
    middleTaskValidationContainer.changeProperty("Name", "MiddleTaskValidationContainer");
//    middleTaskValidationContainer.setIconImage(new Image(images.poiLearningContent()));
    mockForm.addComponent(middleTaskValidationContainer);
    middleTaskValidationContainer.addComponent(textBox);
  }

  private void addClueButtons(YaFormEditor formEditor){
    MockButton menuButton = new MockButton (formEditor);
    MockButton nextButton = new MockButton (formEditor);
    menuButton.changeProperty("Text","Menu principal");
    menuButton.changeProperty("Name","ClueMenuButton");
    nextButton.changeProperty("Text","Suivant");
    nextButton.changeProperty("Name","ClueNextButton");
    bottomClueContainer.addComponent(menuButton);
    bottomClueContainer.addComponent(nextButton);
  }

  private void addLearningContentButton(YaFormEditor formEditor){
    MockButton menuButton = new MockButton (formEditor);
    MockButton previousButton = new MockButton (formEditor);
    MockButton nextButton = new MockButton (formEditor);
    menuButton.changeProperty("Text","Menu Principal");
    menuButton.changeProperty("Name","LearningContentMenuButton");
    previousButton.changeProperty("Text","Retour");
    previousButton.changeProperty("Name","LearningContentPreviousButton");
    nextButton.changeProperty("Text","Suivant");
    nextButton.changeProperty("Name","LearningContentNextButton");
    bottomLearningContentContainer.addComponent(previousButton);
    bottomLearningContentContainer.addComponent(menuButton);
    bottomLearningContentContainer.addComponent(nextButton);
  }

  private void addTaskButton(YaFormEditor formEditor){
    MockButton menuButton = new MockButton (formEditor);
    MockButton previousButton = new MockButton (formEditor);
    MockButton nextButton = new MockButton (formEditor);
    menuButton.changeProperty("Text","Menu Principal");
    menuButton.changeProperty("Name","TaskMenuButton");
    previousButton.changeProperty("Text","Retour");
    previousButton.changeProperty("Name","TaskPreviousButton");
    nextButton.changeProperty("Text","Valider");
    nextButton.changeProperty("Name","TaskValidationButton");
    bottomTaskContainer.addComponent(previousButton);
    bottomTaskContainer.addComponent(menuButton);
    bottomTaskContainer.addComponent(nextButton);
  }

  private void addClueInitialLabels(YaFormEditor formEditor){
    MockLabel mockClueLabel = new MockLabel(formEditor);
    mockClueLabel.changeProperty("Text","Indice");
    mockClueLabel.changeProperty("Name","clueLabel");
    mockClueLabel.changeProperty("FontItalic","True");
    mockClueLabel.changeProperty("FontSize","18.0");
    mockClueLabel.changeProperty("FontBold","True");
    topClueContainer.addComponent(mockClueLabel);
  }

  private void addLearningContentInitialLabels(YaFormEditor formEditor){
    MockLabel mockLearningContentLabel = new MockLabel(formEditor);
    mockLearningContentLabel.changeProperty("Text", MESSAGES.resources());
    mockLearningContentLabel.changeProperty("FontItalic","True");
    mockLearningContentLabel.changeProperty("FontSize","18.0");
    mockLearningContentLabel.changeProperty("FontBold","True");
    topLearningContentContainer.addComponent(mockLearningContentLabel);
  }

  private void addTaskInitialLabels(YaFormEditor formEditor){
    MockLabel mockLearningContentLabel = new MockLabel(formEditor);
    mockLearningContentLabel.changeProperty("Text", MESSAGES.quiz());
    mockLearningContentLabel.changeProperty("FontItalic","True");
    mockLearningContentLabel.changeProperty("FontSize","18.0");
    mockLearningContentLabel.changeProperty("FontBold","True");
    topTaskContainer.addComponent(mockLearningContentLabel);
  }

  private void addTaskValidationInitialLabels(YaFormEditor formEditor){
    MockLabel mockLearningContentLabel = new MockLabel(formEditor);
    mockLearningContentLabel.changeProperty("Text","Validation de l'activité");
    mockLearningContentLabel.changeProperty("FontItalic","True");
    mockLearningContentLabel.changeProperty("FontSize","18.0");
    mockLearningContentLabel.changeProperty("FontBold","True");
    topTaskValidationContainer.addComponent(mockLearningContentLabel);
  }

  public void showClueComponents(YaFormEditor formEditor){
    for (final MockComponent mockComponent: formEditor.getComponents().values()) {
      if (mockComponent.getName().equals("TopClueContainer"))
        mockComponent.changeProperty("Visible","True");
      else if (mockComponent.getName().equals("MiddleClueContainer"))
        mockComponent.changeProperty("Visible","True");
      else if (mockComponent.getName().equals("BottomClueContainer"))
        mockComponent.changeProperty("Visible","True");
    }
//    topClueContainer.changeProperty("Visible","True");
//    middleClueContainer.changeProperty("Visible","True");
//    bottomClueContainer.changeProperty("Visible","True");
  }

  public void hideClueComponents(YaFormEditor formEditor){
      for (final MockComponent mockComponent: formEditor.getComponents().values()) {
          if (mockComponent.getName().equals("TopClueContainer"))
            mockComponent.changeProperty("Visible","False");
          else if (mockComponent.getName().equals("MiddleClueContainer"))
            mockComponent.changeProperty("Visible","False");
          else if (mockComponent.getName().equals("BottomClueContainer"))
            mockComponent.changeProperty("Visible","False");
      }
//    topClueContainer.changeProperty("Visible","False");
//    middleClueContainer.changeProperty("Visible","False");
//    bottomClueContainer.changeProperty("Visible","False");
  }

  public void showLearningContentComponents(YaFormEditor formEditor){
      for (final MockComponent mockComponent: formEditor.getComponents().values()) {
        if (mockComponent.getName().equals("TopLearningContentContainer"))
          mockComponent.changeProperty("Visible","True");
        else if (mockComponent.getName().equals("MiddleLearningContentContainer"))
          mockComponent.changeProperty("Visible","True");
        else if (mockComponent.getName().equals("BottomLearningContentContainer"))
          mockComponent.changeProperty("Visible","True");
      }
//    topLearningContentContainer.changeProperty("Visible","True");
//    middleLearningContentContainer.changeProperty("Visible","True");
//    bottomLearningContentContainer.changeProperty("Visible","True");
  }

  public void hideLearningContentComponents(YaFormEditor formEditor){
    for (final MockComponent mockComponent: formEditor.getComponents().values()) {
      if (mockComponent.getName().equals("TopLearningContentContainer"))
        mockComponent.changeProperty("Visible","False");
      else if (mockComponent.getName().equals("MiddleLearningContentContainer"))
        mockComponent.changeProperty("Visible","False");
      else if (mockComponent.getName().equals("BottomLearningContentContainer"))
        mockComponent.changeProperty("Visible","False");
    }
//    topLearningContentContainer.changeProperty("Visible","False");
//    middleLearningContentContainer.changeProperty("Visible","False");
//    bottomLearningContentContainer.changeProperty("Visible","False");
  }

  public void showTaskComponents(YaFormEditor formEditor){
    for (final MockComponent mockComponent: formEditor.getComponents().values()) {
      if (mockComponent.getName().equals("TopTaskContainer"))
        mockComponent.changeProperty("Visible","True");
      else if (mockComponent.getName().equals("MiddleTaskContainer"))
        mockComponent.changeProperty("Visible","True");
      else if (mockComponent.getName().equals("BottomTaskContainer"))
        mockComponent.changeProperty("Visible","True");
    }
  }

  public void hideTaskComponents(YaFormEditor formEditor){
    for (final MockComponent mockComponent: formEditor.getComponents().values()) {
      if (mockComponent.getName().equals("TopTaskContainer"))
        mockComponent.changeProperty("Visible","False");
      else if (mockComponent.getName().equals("MiddleTaskContainer"))
        mockComponent.changeProperty("Visible","False");
      else if (mockComponent.getName().equals("BottomTaskContainer"))
        mockComponent.changeProperty("Visible","False");
    }
  }

  public void showTaskValidationComponents(YaFormEditor formEditor){
    for (final MockComponent mockComponent: formEditor.getComponents().values()) {
      if (mockComponent.getName().equals("TopTaskValidationContainer"))
        mockComponent.changeProperty("Visible","True");
      else if (mockComponent.getName().equals("MiddleTaskValidationContainer"))
        mockComponent.changeProperty("Visible","True");
//      else if (mockComponent.getName().equals("BottomTaskContainer"))
//        mockComponent.changeProperty("Visible","True");
    }
  }

  public void hideTaskValidationComponents(YaFormEditor formEditor){
    for (final MockComponent mockComponent: formEditor.getComponents().values()) {
      if (mockComponent.getName().equals("TopTaskValidationContainer"))
        mockComponent.changeProperty("Visible","False");
      else if (mockComponent.getName().equals("MiddleTaskValidationContainer"))
        mockComponent.changeProperty("Visible","False");
//      else if (mockComponent.getName().equals("BottomTaskValidaitonContainer"))
//        mockComponent.changeProperty("Visible","False");
    }
  }

  public void onlyShowThisContainer(String visibleContainer, YaFormEditor formEditor){
    for (final MockComponent mockContainer: formEditor.getContainers().values()) {
      if (!mockContainer.getName().contains(visibleContainer) && !mockContainer.getName().contains("Score"))
        mockContainer.changeProperty("Visible","False");
      else
        mockContainer.changeProperty("Visible","True");
    }
  }

  public void returnToClue(FileEditor fileEditor){
    if (fileEditor instanceof YaFormEditor) {
      YaFormEditor formEditor= (YaFormEditor) fileEditor;
      hideTaskComponents(formEditor);
      hideLearningContentComponents(formEditor);
//      onlyShowThisContainer("MiddleClueContainer",formEditor);
      showClueComponents(formEditor);
      setCurrentJemComponent(CLUE);
      formEditor.getForm().getPreviousMockFormButton().setEnabled(false);
      formEditor.getForm().setPointEnabled(CLUE);
    }
  }

  public String getCurrentJemComponent() {
    return currentJemComponent;
  }

  // Pour le moment on pourra juste rajouter trois écrans
  public String getNextJemComponent() {
    if (currentJemComponent.equals(CLUE))
      return LEARNING_CONTENT;
    else if (currentJemComponent.equals(LEARNING_CONTENT))
      return TASK;
    else if (currentJemComponent.equals(TASK))
      return FIRST_ADDED_SCREEN;
    else if (currentJemComponent.equals(FIRST_ADDED_SCREEN))
      return SECOND_ADDED_SCREEN;
    else if (currentJemComponent.equals(SECOND_ADDED_SCREEN))
      return THIRD_ADDED_SCREEN;
    else
      return null;
    }

    // Pour le moment on pourra juste rajouter trois écrans
  public String getNextJemComponent(String currentJemComponent) {
    if (currentJemComponent.equals(CLUE))
      return LEARNING_CONTENT;
    else if (currentJemComponent.equals(LEARNING_CONTENT))
      return TASK;
    else if (currentJemComponent.equals(TASK))
      return FIRST_ADDED_SCREEN;
    else if (currentJemComponent.equals(FIRST_ADDED_SCREEN))
      return SECOND_ADDED_SCREEN;
    else if (currentJemComponent.equals(SECOND_ADDED_SCREEN))
      return THIRD_ADDED_SCREEN;
    else
      return null;
    }

  // Pour le moment on pourra juste rajouter trois écrans
  public String getPreviousJemComponent() {
    if (currentJemComponent.equals(CLUE))
      return null;
    else if (currentJemComponent.equals(LEARNING_CONTENT))
      return CLUE;
    else if (currentJemComponent.equals(TASK))
      return LEARNING_CONTENT;
    else if (currentJemComponent.equals(FIRST_ADDED_SCREEN))
      return TASK;
    else if (currentJemComponent.equals(SECOND_ADDED_SCREEN))
      return FIRST_ADDED_SCREEN;
    else if (currentJemComponent.equals(THIRD_ADDED_SCREEN))
      return SECOND_ADDED_SCREEN;
    else return null;
  }

  public String getPreviousJemComponent(String currentJemComponent) {
    if (currentJemComponent.equals(CLUE))
      return null;
    else if (currentJemComponent.equals(LEARNING_CONTENT))
      return CLUE;
    else if (currentJemComponent.equals(TASK))
      return LEARNING_CONTENT;
    else if (currentJemComponent.equals(FIRST_ADDED_SCREEN))
      return TASK;
    else if (currentJemComponent.equals(SECOND_ADDED_SCREEN))
      return FIRST_ADDED_SCREEN;
    else if (currentJemComponent.equals(THIRD_ADDED_SCREEN))
      return SECOND_ADDED_SCREEN;
    else return null;
  }

  public void setCurrentJemComponent(String currentJemComponent) {
    this.currentJemComponent=currentJemComponent;
  }

  public static JemComponents getInstance() {
    return instance;
  }

  public MockVerticalArrangement getMiddleClueContainer() {
    return middleClueContainer;
  }

}


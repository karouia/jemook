// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.client.editor.simple.components;

import com.google.appinventor.client.editor.simple.SimpleEditor;
import com.google.appinventor.components.common.ComponentConstants;
import com.google.gwt.user.client.ui.InlineLabel;

/**
 * Mock VerticalArrangement component.
 *
 * @author sharon@google.com (Sharon Perl)
 */
public final class MockPoiClue extends MockHVArrangement {

  /**
   * Component type name.
   */
  public static final String TYPE = "PoiClue";

//  private MockLabel labelBlanc;
  private MockImage clueImage ;
  private MockLabel clueLabel ;
  private MockButton clueButton ;

  // GWT label widget used to mock a Simple Label

  /**
   * Creates a new MockPoiClue component.
   *
   * @param editor  editor of source file the component belongs to
   */

  public MockPoiClue(SimpleEditor editor) {
    super(editor, TYPE, images.poiLearningContent(),
        ComponentConstants.LAYOUT_ORIENTATION_VERTICAL,
        ComponentConstants.NONSCROLLABLE_ARRANGEMENT);

    //    container.addVisibleComponent(hpa,-1);

/*    labelWidget = new InlineLabel();
    labelWidget.setStylePrimaryName("ode-SimpleMockComponent");
    initComponent(labelWidget);
*/
    //labelWidget = new InlineLabel();
//    layoutWidget.setLayoutData("hello");
  }


  /*
   * Sets the label's Text property to a new value.
   */
//  private void setTextProperty(String text) {
//    labelWidget.setText(text);
//  }

}

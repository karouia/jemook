// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0
package com.google.appinventor.client.editor.youngandroid.properties;

import com.google.appinventor.client.widgets.properties.ChoicePropertyEditor;

import static com.google.appinventor.client.Ode.MESSAGES;

/**
 * Property editor for trigger mode.
 * 
 * @author feeney.kate@gmail.com (Kate Feeney)
 */
public class YoungAndroidTriggerModeChoicePropertyEditor extends ChoicePropertyEditor {

  // Button shape choices
  private static final Choice[] shapes = new Choice[] {
    new Choice(MESSAGES.manualMode(), "0"),
    new Choice(MESSAGES.gpsMode(), "1"),
    new Choice(MESSAGES.qrCodeMode(), "2"),
  };

  public YoungAndroidTriggerModeChoicePropertyEditor() {
    super(shapes);
  }
}

// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.client.editor.simple.components;

import com.google.appinventor.client.editor.simple.SimpleEditor;
import com.google.appinventor.client.editor.simple.palette.SimpleComponentDescriptor;
import com.google.appinventor.components.common.ComponentConstants;

import static com.google.gwt.user.client.Window.alert;

/**
 * Mock VerticalArrangement component.
 *
 * @author sharon@google.com (Sharon Perl)
 */
public final class MockPoiLearningContent extends MockHVArrangement {

  /**
   * Component type name.
   */
  public static final String TYPE = "PoiLearningContent";

  /**
   * Creates a new MockVerticalArrangement component.
   *
   * @param editor  editor of source file the component belongs to
   */
  public MockPoiLearningContent(SimpleEditor editor) {
    super(editor, TYPE, images.poiLearningContent(),
        ComponentConstants.LAYOUT_ORIENTATION_VERTICAL,
        ComponentConstants.NONSCROLLABLE_ARRANGEMENT);

//  this.changeProperty(PROPERTY_NAME_WIDTH,""+LENGTH_FILL_PARENT);
//  this.changeProperty(PROPERTY_NAME_HEIGHT,""+(-100+LENGTH_PERCENT_TAG));
//  this.changeProperty(MockHVArrangement.PROPERTY_NAME_HORIZONTAL_ALIGNMENT,""+ComponentConstants.GRAVITY_CENTER_HORIZONTAL);
//  this.changeProperty(PROPERTY_NAME_VERTICAL_ALIGNMENT,""+ComponentConstants.GRAVITY_TOP);

//    MockLabel clueLabel = new MockLabel(editor);
//
//    clueLabel.setContainer(this);
//    rootPanel.add(clueLabel);
//    refreshForm();
//    addMockComponent(clueLabel,-1);
//    alert(""+clueLabel);
  }


}

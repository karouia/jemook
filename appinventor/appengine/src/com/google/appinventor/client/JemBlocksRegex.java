package com.google.appinventor.client;

/**
 * Created by akaroui on 20/05/2017.
 */
public class JemBlocksRegex {
  // Key Words
  public static final String CURRENT_FORM = "CURRENT_FORM";


  /**
   * Chaines de départ et de fin pour les blocs
   */

  public static String WEB_VIEWER_FIRST_KEY_STRING ="</field>\n" +
      "\" +\n" +
      "      \"            <field name=\\\"PROP\\\">DataUri</field>\\n\" +\n" +
      "      \"            <value name=\\\"VALUE\\\">\\n\" +\n" +
      "      \"              <block type=\\\"text\\\" id=\\\"227\\\">\\n\" +\n" +
      "      \"                <field name=\"TEXT\">WEB_LINK";
  public static String WEB_VIEWER_SECOND_KEY_STRING ="</field>\n" +
      "\" +\n" +
      "      \"            <field name=\\\"PROP\\\">DataUri</field>\\n\" +\n" +
      "      \"            <value name=\\\"VALUE\\\">\\n\" +\n" +
      "      \"              <block type=\\\"text\\\" id=\\\"227\\\">\\n\" +\n" +
      "      \"                <field name=\"TEXT\">";
  public static String BLOCKS_STARTING ="<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n";
  public static String BLOCKS_ENDING =
      "<yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  /**
   * Chaine initialement vide du corps des blocs
   */
  public static String INITIAL_BLOCKS_CORE ="";

  public static String SCORE_START_VALUE =
      "  <block type=\"global_declaration\" id=\"[0-9]+\" inline=\"false\" x=\"-[0-9]+\" y=\"-[0-9]+\">\n" +
          "    <field name=\"NAME\">score</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"controls_getStartValue\" id=\"[0-9]+\"></block>\n" +
          "    </value>\n" +
          "  </block>";

  public static String SCORE_NULL ="<block type=\"global_declaration\" id=\"4\" inline=\"false\" x=\"-3\" y=\"-306\">\n" +
      "    <field name=\"NAME\">score</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"math_number\" id=\"115\">\n" +
      "        <field name=\"NUM\">0</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>";

  public static String CLUE_NEXT_MANUAL =
      "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"-[0-9]+\">\n"+
          "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"displayLearningContent\"></mutation>\n" +
          "        <field name=\"PROCNAME\">displayLearningContent</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" ;

  /**
   * Chaine du Screen1 par défaut
   */
  public static String SCREEN1 = "<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
      "  <block type=\"component_event\" id=\"1\" x=\"-2\" y=\"-621\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"StartButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">StartButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"13\">\n" +
      "        <mutation name=\"FermeEcran\"></mutation>\n" +
      "        <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "        <next>\n" +
      "          <block type=\"controls_openAnotherScreen\" id=\"2\" inline=\"false\">\n" +
      "            <value name=\"SCREEN\">\n" +
      "              <block type=\"text\" id=\"3\">\n" +
      "                <field name=\"TEXT\">Activite_1</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"procedures_defnoreturn\" id=\"8\" x=\"539\" y=\"-618\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">FermeEcran</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_closeScreen\" id=\"9\"></block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"component_event\" id=\"4\" x=\"-2\" y=\"-502\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"QuitButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">QuitButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"controls_closeApplication\" id=\"5\"></block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  /**
   * Chaine initiale d'un écran
   * */

  public static String INITIAL_FORM =
      "<xml xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
      "<block type=\"component_event\" id=\"1\" x=\"-714\" y=\"-325\">\n" +
      "<mutation component_type=\"Form\" instance_name=\"CURRENT_FORM\" event_name=\"Initialize\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">CURRENT_FORM</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"component_set_get\" id=\"2\" inline=\"false\">\n" +
      "<mutation component_type=\"Label\" set_or_get=\"set\" property_name=\"Text\" is_generic=\"false\" instance_name=\"PointsLabel\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">PointsLabel</field>\n" +
      "<field name=\"PROP\">Text</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"controls_getStartValue\" id=\"3\"></block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"global_declaration\" id=\"4\" inline=\"false\" x=\"-20\" y=\"-295\">\n" +
      "<field name=\"NAME\">score</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"controls_getStartValue\" id=\"5\"></block>\n" +
      "</value>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"6\" x=\"-449\" y=\"-241\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"ClueMenuButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">ClueMenuButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"7\">\n" +
      "<mutation name=\"FermeEcran\"></mutation>\n" +
      "<field name=\"PROCNAME\">FermeEcran</field>\n" +
      "<next>\n" +
      "<block type=\"controls_openAnotherScreen\" id=\"8\" inline=\"false\">\n" +
      "<value name=\"SCREEN\">\n" +
      "<block type=\"text\" id=\"9\">\n" +
      "<field name=\"TEXT\">Screen1</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"10\" x=\"-451\" y=\"-146\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"LearningContentMenuButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">LearningContentMenuButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"11\">\n" +
      "<mutation name=\"FermeEcran\"></mutation>\n" +
      "<field name=\"PROCNAME\">FermeEcran</field>\n" +
      "<next>\n" +
      "<block type=\"controls_openAnotherScreen\" id=\"12\" inline=\"false\">\n" +
      "<value name=\"SCREEN\">\n" +
      "<block type=\"text\" id=\"13\">\n" +
      "<field name=\"TEXT\">Screen1</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"procedures_defnoreturn\" id=\"14\" x=\"335\" y=\"-116\">\n" +
      "<mutation></mutation>\n" +
      "<field name=\"NAME\">displayClue</field>\n" +
      "<statement name=\"STACK\">\n" +
      "<block type=\"component_set_get\" id=\"15\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"16\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"17\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"18\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"19\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"20\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"21\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"22\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"23\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"24\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"25\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"26\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"27\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"28\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"29\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"30\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"31\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"32\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"33\" x=\"-449\" y=\"-49\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"TaskMenuButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TaskMenuButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"34\">\n" +
      "<mutation name=\"FermeEcran\"></mutation>\n" +
      "<field name=\"PROCNAME\">FermeEcran</field>\n" +
      "<next>\n" +
      "<block type=\"controls_openAnotherScreen\" id=\"35\" inline=\"false\">\n" +
      "<value name=\"SCREEN\">\n" +
      "<block type=\"text\" id=\"36\">\n" +
      "<field name=\"TEXT\">Screen1</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"37\" x=\"-9\" y=\"-52\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"LearningContentPreviousButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">LearningContentPreviousButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"38\">\n" +
      "<mutation name=\"displayClue\"></mutation>\n" +
      "<field name=\"PROCNAME\">displayClue</field>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"83\" x=\"-9\" y=\"17\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"LearningContentNextButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">LearningContentNextButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"84\">\n" +
      "<mutation name=\"displayTask\"></mutation>\n" +
      "<field name=\"PROCNAME\">displayTask</field>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"component_event\" id=\"39\" x=\"-7\" y=\"89\">\n" +
      "<mutation component_type=\"Button\" instance_name=\"TaskPreviousButton\" event_name=\"Click\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TaskPreviousButton</field>\n" +
      "<statement name=\"DO\">\n" +
      "<block type=\"procedures_callnoreturn\" id=\"40\">\n" +
      "<mutation name=\"displayLearningContent\"></mutation>\n" +
      "<field name=\"PROCNAME\">displayLearningContent</field>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"procedures_defnoreturn\" id=\"41\" x=\"334\" y=\"165\">\n" +
      "<mutation></mutation>\n" +
      "<field name=\"NAME\">displayLearningContent</field>\n" +
      "<statement name=\"STACK\">\n" +
      "<block type=\"component_set_get\" id=\"42\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"43\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"44\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"45\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"46\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"47\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"48\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"49\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"50\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"51\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"52\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"53\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"54\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"55\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"56\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"57\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"58\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"59\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"procedures_defnoreturn\" id=\"60\" x=\"336\" y=\"444\">\n" +
      "<mutation></mutation>\n" +
      "<field name=\"NAME\">displayTask</field>\n" +
      "<statement name=\"STACK\">\n" +
      "<block type=\"component_set_get\" id=\"61\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"62\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"63\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"64\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"65\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomClueContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomClueContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"66\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"67\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"68\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"69\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"70\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"71\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomLearningContentContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomLearningContentContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"72\">\n" +
      "<field name=\"BOOL\">FALSE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"73\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"TopTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">TopTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"74\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"75\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"MiddleTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">MiddleTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"76\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "<next>\n" +
      "<block type=\"component_set_get\" id=\"77\" inline=\"false\">\n" +
      "<mutation component_type=\"VerticalArrangement\" set_or_get=\"set\" property_name=\"Visible\" is_generic=\"false\" instance_name=\"BottomTaskContainer\"></mutation>\n" +
      "<field name=\"COMPONENT_SELECTOR\">BottomTaskContainer</field>\n" +
      "<field name=\"PROP\">Visible</field>\n" +
      "<value name=\"VALUE\">\n" +
      "<block type=\"logic_boolean\" id=\"78\">\n" +
      "<field name=\"BOOL\">TRUE</field>\n" +
      "</block>\n" +
      "</value>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</next>\n" +
      "</block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<block type=\"procedures_defnoreturn\" id=\"79\" x=\"333\" y=\"741\">\n" +
      "<mutation></mutation>\n" +
      "<field name=\"NAME\">FermeEcran</field>\n" +
      "<statement name=\"STACK\">\n" +
      "<block type=\"controls_closeScreen\" id=\"80\"></block>\n" +
      "</statement>\n" +
      "</block>\n" +
      "<yacodeblocks ya-version=\"151\" language-version=\"20\"></yacodeblocks>\n" +
      "</xml>";

  public static String SOUND_COMPONENT = "  <block type=\"component_event\" id=\"60\" x=\"-10\" y=\"90\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"SoundButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">SoundButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"component_method\" id=\"61\">\n" +
      "        <mutation component_type=\"Sound\" method_name=\"Play\" is_generic=\"false\" instance_name=\"Son1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">HPA_98</field>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n";

  public static String QCM_COMPONENT = "    <block type=\"global_declaration\" id=\"250\" inline=\"false\" x=\"17\" y=\"-210\">\n" +
      "    <field name=\"NAME\">QCM</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"lists_create_with\" id=\"251\">\n" +
      "        <mutation items=\"0\"></mutation>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>\n" +
      "  <block type=\"global_declaration\" id=\"64\" inline=\"false\" x=\"17\" y=\"-251\">\n" +
      "    <field name=\"NAME\">coché</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"logic_false\" id=\"65\">\n" +
      "        <field name=\"BOOL\">FALSE</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>\n" +
      "  <block type=\"component_event\" id=\"252\" x=\"-587\" y=\"64\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"LearningContentNextButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">LearningContentNextButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"253\">\n" +
      "        <mutation name=\"displayTask\"></mutation>\n" +
      "        <field name=\"PROCNAME\">displayTask</field>\n" +
      "        <next>\n" +
      "          <block type=\"lexical_variable_set\" id=\"254\" inline=\"false\">\n" +
      "            <field name=\"VAR\">global QCM</field>\n" +
      "            <value name=\"VALUE\">\n" +
      "              <block type=\"lists_create_with\" id=\"255\" inline=\"false\">\n" +
      "                <mutation items=\"3\"></mutation>\n" +
      "                <value name=\"ADD0\">\n" +
      "                  <block type=\"component_component_block\" id=\"256\">\n" +
      "                    <mutation component_type=\"CheckBox\" instance_name=\"Case_à_cocher1\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Case_à_cocher1</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"ADD1\">\n" +
      "                  <block type=\"component_component_block\" id=\"257\">\n" +
      "                    <mutation component_type=\"CheckBox\" instance_name=\"Case_à_cocher2\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Case_à_cocher2</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"ADD2\">\n" +
      "                  <block type=\"component_component_block\" id=\"258\">\n" +
      "                    <mutation component_type=\"CheckBox\" instance_name=\"Case_à_cocher3\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Case_à_cocher3</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"component_event\" id=\"259\" x=\"-8\" y=\"179\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"260\">\n" +
      "        <mutation name=\"vérifier_coché\"></mutation>\n" +
      "        <field name=\"PROCNAME\">vérifier_coché</field>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"261\">\n" +
      "            <mutation name=\"calculer_Score\"></mutation>\n" +
      "            <field name=\"PROCNAME\">calculer_Score</field>\n" +
      "            <next>\n" +
      "              <block type=\"lexical_variable_set\" id=\"262\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global coché</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"logic_boolean\" id=\"263\">\n" +
      "                    <field name=\"BOOL\">FALSE</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"procedures_defnoreturn\" id=\"264\" x=\"-753\" y=\"392\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">vérifier_coché</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_forEach\" id=\"265\" inline=\"false\">\n" +
      "        <field name=\"VAR\">élément</field>\n" +
      "        <value name=\"LIST\">\n" +
      "          <block type=\"lexical_variable_get\" id=\"266\">\n" +
      "            <field name=\"VAR\">global QCM</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO\">\n" +
      "          <block type=\"controls_if\" id=\"267\" inline=\"false\">\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"component_set_get\" id=\"268\" inline=\"false\">\n" +
      "                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                <field name=\"PROP\">Checked</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"270\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global coché</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"logic_boolean\" id=\"271\">\n" +
      "                    <field name=\"BOOL\">TRUE</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n" +
      "  <block type=\"procedures_defnoreturn\" id=\"272\" x=\"-750\" y=\"578\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">calculer_Score</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_if\" id=\"273\" inline=\"false\">\n" +
      "        <mutation else=\"1\"></mutation>\n" +
      "        <value name=\"IF0\">\n" +
      "          <block type=\"lexical_variable_get\" id=\"274\">\n" +
      "            <field name=\"VAR\">global coché</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO0\">\n" +
      "          <block type=\"controls_forEach\" id=\"275\" inline=\"false\">\n" +
      "            <field name=\"VAR\">élément</field>\n" +
      "            <value name=\"LIST\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"276\">\n" +
      "                <field name=\"VAR\">global QCM</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO\">\n" +
      "              <block type=\"controls_if\" id=\"277\" inline=\"false\">\n" +
      "                <value name=\"IF0\">\n" +
      "                  <block type=\"component_set_get\" id=\"278\" inline=\"false\">\n" +
      "                    <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                    <field name=\"PROP\">Checked</field>\n" +
      "                    <value name=\"COMPONENT\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"279\">\n" +
      "                        <field name=\"VAR\">élément</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO0\">\n" +
      "                  <block type=\"controls_if\" id=\"280\" inline=\"false\">\n" +
      "                    <mutation else=\"1\"></mutation>\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"logic_compare\" id=\"281\" inline=\"true\">\n" +
      "                        <field name=\"OP\">EQ</field>\n" +
      "                        <value name=\"A\">\n" +
      "                          <block type=\"component_set_get\" id=\"282\" inline=\"false\">\n" +
      "                            <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                            <field name=\"PROP\">Text</field>\n" +
      "                            <value name=\"COMPONENT\">\n" +
      "                              <block type=\"lexical_variable_get\" id=\"283\">\n" +
      "                                <field name=\"VAR\">élément</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <value name=\"B\">\n" +
      "                          <block type=\"component_set_get\" id=\"284\">\n" +
      "                            <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                            <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                            <field name=\"PROP\">Text</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"lexical_variable_set\" id=\"285\" inline=\"false\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                        <value name=\"VALUE\">\n" +
      "                          <block type=\"math_add\" id=\"286\" inline=\"true\">\n" +
      "                            <mutation items=\"2\"></mutation>\n" +
      "                            <value name=\"NUM0\">\n" +
      "                              <block type=\"lexical_variable_get\" id=\"287\">\n" +
      "                                <field name=\"VAR\">global score</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"NUM1\">\n" +
      "                              <block type=\"math_number\" id=\"288\">\n" +
      "                                <field name=\"NUM\">50</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <next>\n" +
      "                          <block type=\"component_method\" id=\"289\" inline=\"false\">\n" +
      "                            <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "                            <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "                            <value name=\"ARG0\">\n" +
      "                              <block type=\"text\" id=\"290\">\n" +
      "                                <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"ARG1\">\n" +
      "                              <block type=\"text\" id=\"291\">\n" +
      "                                <field name=\"TEXT\">Bonne réponse</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"ARG2\">\n" +
      "                              <block type=\"text\" id=\"292\">\n" +
      "                                <field name=\"TEXT\">Ok</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"293\" inline=\"false\">\n" +
      "                                <value name=\"SCREENNAME\">\n" +
      "                                  <block type=\"text\" id=\"294\">\n" +
      "                                    <field name=\"TEXT\">NEXT_FORM</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"STARTVALUE\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"295\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </next>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                    <statement name=\"ELSE\">\n" +
      "                      <block type=\"component_method\" id=\"296\" inline=\"false\">\n" +
      "                        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "                        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "                        <value name=\"ARG0\">\n" +
      "                          <block type=\"text\" id=\"297\">\n" +
      "                            <field name=\"TEXT\">Dommage ! Tu peux ressayer pour trouver la bonne réponse.</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <value name=\"ARG1\">\n" +
      "                          <block type=\"text\" id=\"298\">\n" +
      "                            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <value name=\"ARG2\">\n" +
      "                          <block type=\"text\" id=\"299\">\n" +
      "                            <field name=\"TEXT\">Ok</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <statement name=\"ELSE\">\n" +
      "          <block type=\"component_method\" id=\"300\" inline=\"false\">\n" +
      "            <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "            <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "            <value name=\"ARG0\">\n" +
      "              <block type=\"text\" id=\"301\">\n" +
      "                <field name=\"TEXT\">Il faut choisir une réponse pour passer à l'activité suivante !</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"ARG1\">\n" +
      "              <block type=\"text\" id=\"302\">\n" +
      "                <field name=\"TEXT\">Information</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"ARG2\">\n" +
      "              <block type=\"text\" id=\"303\">\n" +
      "                <field name=\"TEXT\">Ok</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n";

  public static String FAIL_MSG =
      "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">.*</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" ;

  public static String SUCCESS_MSG =
      "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"[0-9]+\">\n" +
      "            <field name=\"TEXT\">.*</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"[0-9]+\">\n" +
      "            <field name=\"TEXT\">Bonne réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"[0-9]+\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG3\">\n" +
      "          <block type=\"text\" id=\"[0-9]+\">\n" +
      "            <field name=\"TEXT\">Retour</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG4\">\n" +
      "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
      "            <field name=\"BOOL\">FALSE</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>\n";

  public static String OPEN_QUESTION_COMPONENT =
      "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"calculer_Score\"></mutation>\n" +
          "        <field name=\"PROCNAME\">calculer_Score</field>\n" +
          "        <next>\n" +
          "          <block type=\"lexical_variable_set\" id=\"[0-9]+\" inline=\"false\">\n" +
          "            <field name=\"VAR\">global reponses</field>\n" +
          "            <value name=\"VALUE\">\n" +
          "              <block type=\"math_add\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                <mutation items=\"[0-9]+\"></mutation>\n" +
          "                <value name=\"NUM0\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                    <field name=\"VAR\">global reponses</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"NUM1\">\n" +
          "                  <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                    <field name=\"NUM\">[0-9]+</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </next>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"global_declaration\" id=\"[0-9]+\" inline=\"false\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <field name=\"NAME\">reponses</field>\n" +
          "    <value name=\"VALUE\">\n" +
          "      <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "        <field name=\"NUM\">[0-9]+</field>\n" +
          "      </block>\n" +
          "    </value>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Essaies encore de trouver la réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">.*</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Bonne réponse</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">calculer_Score</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation elseif=\"[0-9]+\"></mutation>\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                <field name=\"NUM\">[0-9]+</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "            <mutation else=\"[0-9]+\"></mutation>\n" +
          "            <value name=\"IF[0-9]+\">\n" +
          "              <block type=\"text_contains\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO[0-9]+\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                    <mutation items=\"[0-9]+\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                        <field name=\"NUM\">[0-9]+</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                <field name=\"NUM\">[0-9]+</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "            <mutation else=\"[0-9]+\"></mutation>\n" +
          "            <value name=\"IF[0-9]+\">\n" +
          "              <block type=\"text_contains\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO[0-9]+\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                    <mutation items=\"2\"></mutation>\n" +
          "                    <value name=\"NUM0\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM1\">\n" +
          "                      <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                        <field name=\"NUM\">[0-9]+</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                <field name=\"NUM\">[0-9]+</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "            <mutation else=\"[0-9]+\"></mutation>\n" +
          "            <value name=\"IF[0-9]+\">\n" +
          "              <block type=\"text_contains\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"TEXT\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"PIECE\">\n" +
          "                  <block type=\"component_set_get\" id=\"[0-9]+\">\n" +
          "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
          "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
          "                    <field name=\"PROP\">Text</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <statement name=\"DO[0-9]+\">\n" +
          "              <block type=\"lexical_variable_set\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <field name=\"VAR\">global score</field>\n" +
          "                <value name=\"VALUE\">\n" +
          "                  <block type=\"math_add\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                    <mutation items=\"[0-9]+\"></mutation>\n" +
          "                    <value name=\"NUM[0-9]+\">\n" +
          "                      <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                        <field name=\"VAR\">global score</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                    <value name=\"NUM[0-9]+\">\n" +
          "                      <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                        <field name=\"NUM\">[0-9]+</field>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <next>\n" +
          "                  <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                    <mutation name=\"bonneReponse\"></mutation>\n" +
          "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "                  </block>\n" +
          "                </next>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "            <statement name=\"ELSE\">\n" +
          "              <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "              </block>\n" +
          "            </statement>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <field name=\"VAR\">global reponses</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                <field name=\"NUM\">[0-9]+</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"[0-9]+\">\n" +
          "                    <field name=\"TEXT\">Activite_[0-9]+</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <value name=\"IF[0-9]+\">\n" +
          "          <block type=\"logic_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"[0-9]+\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"[0-9]+\">\n" +
          "                    <field name=\"TEXT\">Activite_[0-9]+</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n"
 ;

  public static String WEB_VIEWER_COMPONENT= "<block type=\"component_event\" id=\"120\" x=\"-705\" y=\"88\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"BUTTON_NAME\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">BUTTON_NAME</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"component_set_get\" id=\"148\" inline=\"false\">\n" +
      "        <mutation component_type=\"ActivityStarter\" set_or_get=\"set\" property_name=\"Action\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "        <field name=\"PROP\">Action</field>\n" +
      "        <value name=\"VALUE\">\n" +
      "          <block type=\"text\" id=\"226\">\n" +
      "            <field name=\"TEXT\">android.intent.action.VIEW</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <next>\n" +
      "          <block type=\"component_set_get\" id=\"176\" inline=\"false\">\n" +
      "            <mutation component_type=\"ActivityStarter\" set_or_get=\"set\" property_name=\"DataUri\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "            <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "            <field name=\"PROP\">DataUri</field>\n" +
      "            <value name=\"VALUE\">\n" +
      "              <block type=\"text\" id=\"227\">\n" +
      "                <field name=\"TEXT\">WEB_LINK</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <next>\n" +
      "              <block type=\"component_method\" id=\"211\">\n" +
      "                <mutation component_type=\"ActivityStarter\" method_name=\"StartActivity\" is_generic=\"false\" instance_name=\"ACTIVITY_STARTER_NAME\"></mutation>\n" +
      "                <field name=\"COMPONENT_SELECTOR\">ACTIVITY_STARTER_NAME</field>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>";

  public static String GEOLOCATION =
      "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"-[0-9]+\">\n" +
          "    <mutation component_type=\"LocationSensor\" instance_name=\"Capteur_position1\" event_name=\"LocationChanged\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Capteur_position1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_operation\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">AND</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                <field name=\"OP\">LT</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"math_abs\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                    <field name=\"OP\">ABS</field>\n" +
          "                    <value name=\"NUM\">\n" +
          "                      <block type=\"math_subtract\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                        <value name=\"A\">\n" +
          "                          <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                            <mutation>\n" +
          "                              <eventparam name=\"latitude\"></eventparam>\n" +
          "                            </mutation>\n" +
          "                            <field name=\"VAR\">Latitude</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <value name=\"B\">\n" +
          "                          <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                            <field name=\"NUM\">9944405</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                    <field name=\"NUM\">0.0001</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"math_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                <field name=\"OP\">LT</field>\n" +
          "                <value name=\"A\">\n" +
          "                  <block type=\"math_abs\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                    <field name=\"OP\">ABS</field>\n" +
          "                    <value name=\"NUM\">\n" +
          "                      <block type=\"math_subtract\" id=\"[0-9]+\" inline=\"true\">\n" +
          "                        <value name=\"A\">\n" +
          "                          <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                            <mutation>\n" +
          "                              <eventparam name=\"longitude\"></eventparam>\n" +
          "                            </mutation>\n" +
          "                            <field name=\"VAR\">Longitude</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                        <value name=\"B\">\n" +
          "                          <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                            <field name=\"NUM\">9944405</field>\n" +
          "                          </block>\n" +
          "                        </value>\n" +
          "                      </block>\n" +
          "                    </value>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"B\">\n" +
          "                  <block type=\"math_number\" id=\"[0-9]+\">\n" +
          "                    <field name=\"NUM\">0.0001</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO0\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"displayLearningContent\"></mutation>\n" +
          "            <field name=\"PROCNAME\">displayLearningContent</field>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>" +
          "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Il faut trouver le point indiqué pour afficher l'activité !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Information</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>";

  public static String TEST =
      "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"CameraButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">CameraButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\">\n" +
          "        <mutation component_type=\"Camera\" method_name=\"TakePicture\" is_generic=\"false\" instance_name=\"Prise_d_image1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Camera\" instance_name=\"Prise_d_image1\" event_name=\"AfterPicture\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"bonneReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Veuillez prendre la photo d'abord !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Erreur</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Vous allez passer à l'activité suivante</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Bravo !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"[0-9]+\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"[0-9]+\">\n" +
          "                    <field name=\"TEXT\">Activite_6</field>\n" +
          "                  </block>\n" +
          "                </value>\n"
      ;
  public static String TAKE_PICTURE_COMPONENT =
      "<block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"mauvaiseReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Button\" instance_name=\"CameraButton\" event_name=\"Click\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">CameraButton</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\">\n" +
          "        <mutation component_type=\"Camera\" method_name=\"TakePicture\" is_generic=\"false\" instance_name=\"Prise_d_image1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Camera\" instance_name=\"Prise_d_image1\" event_name=\"AfterPicture\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Prise_d_image1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "        <mutation name=\"bonneReponse\"></mutation>\n" +
          "        <field name=\"PROCNAME\">bonneReponse</field>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Veuillez prendre la photo d'abord !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Erreur</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"procedures_defnoreturn\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation></mutation>\n" +
          "    <field name=\"NAME\">bonneReponse</field>\n" +
          "    <statement name=\"STACK\">\n" +
          "      <block type=\"component_method\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <mutation component_type=\"Notifier\" method_name=\"ShowChooseDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
          "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "        <value name=\"ARG0\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Vous allez passer à l'activité suivante</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG1\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Bravo !</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG2\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Ok</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG3\">\n" +
          "          <block type=\"text\" id=\"[0-9]+\">\n" +
          "            <field name=\"TEXT\">Retour</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <value name=\"ARG4\">\n" +
          "          <block type=\"logic_boolean\" id=\"[0-9]+\">\n" +
          "            <field name=\"BOOL\">FALSE</field>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" +
          "  <block type=\"component_event\" id=\"[0-9]+\" x=\"-[0-9]+\" y=\"[0-9]+\">\n" +
          "    <mutation component_type=\"Notifier\" instance_name=\"Notificateur1\" event_name=\"AfterChoosing\"></mutation>\n" +
          "    <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
          "    <statement name=\"DO\">\n" +
          "      <block type=\"controls_if\" id=\"[0-9]+\" inline=\"false\">\n" +
          "        <value name=\"IF0\">\n" +
          "          <block type=\"logic_compare\" id=\"[0-9]+\" inline=\"true\">\n" +
          "            <field name=\"OP\">EQ</field>\n" +
          "            <value name=\"A\">\n" +
          "              <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                <mutation>\n" +
          "                  <eventparam name=\"choice\"></eventparam>\n" +
          "                </mutation>\n" +
          "                <field name=\"VAR\">Choix</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "            <value name=\"B\">\n" +
          "              <block type=\"text\" id=\"[0-9]+\">\n" +
          "                <field name=\"TEXT\">Ok</field>\n" +
          "              </block>\n" +
          "            </value>\n" +
          "          </block>\n" +
          "        </value>\n" +
          "        <statement name=\"DO[0-9]+\">\n" +
          "          <block type=\"procedures_callnoreturn\" id=\"[0-9]+\">\n" +
          "            <mutation name=\"FermeEcran\"></mutation>\n" +
          "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
          "            <next>\n" +
          "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"[0-9]+\" inline=\"false\">\n" +
          "                <value name=\"SCREENNAME\">\n" +
          "                  <block type=\"text\" id=\"[0-9]+\">\n" +
          "                    <field name=\"TEXT\">Activite_[0-9]+</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "                <value name=\"STARTVALUE\">\n" +
          "                  <block type=\"lexical_variable_get\" id=\"[0-9]+\">\n" +
          "                    <field name=\"VAR\">global score</field>\n" +
          "                  </block>\n" +
          "                </value>\n" +
          "              </block>\n" +
          "            </next>\n" +
          "          </block>\n" +
          "        </statement>\n" +
          "      </block>\n" +
          "    </statement>\n" +
          "  </block>\n" ;

  public static String OPEN_QUESTION_COMP = "<block type=\"global_declaration\" id=\"164\" inline=\"false\" x=\"-24\" y=\"-250\">\n" +
      "    <field name=\"NAME\">reponses</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"math_number\" id=\"165\">\n" +
      "        <field name=\"NUM\">0</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "  <block type=\"procedures_defnoreturn\" id=\"477\" x=\"-1907\" y=\"-299\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">bonneReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"478\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"479\">\n" +
      "            <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"480\">\n" +
      "            <field name=\"TEXT\">Bonne réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"481\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"482\">\n" +
      "            <mutation name=\"FermeEcran\"></mutation>\n" +
      "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "            <next>\n" +
      "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"483\" inline=\"false\">\n" +
      "                <value name=\"SCREENNAME\">\n" +
      "                  <block type=\"text\" id=\"484\">\n" +
      "                    <field name=\"TEXT\">Activite_2</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"STARTVALUE\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"485\">\n" +
      "                    <field name=\"VAR\">global score</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"561\" x=\"-1932\" y=\"-66\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"562\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"563\">\n" +
      "            <field name=\"TEXT\">Tu es sur ? Essaies encore de trouver la réponse.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"564\">\n" +
      "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"565\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"321\" x=\"-1052\" y=\"-62\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"322\">\n" +
      "        <mutation name=\"calculer_Score\"></mutation>\n" +
      "        <field name=\"PROCNAME\">calculer_Score</field>\n" +
      "        <next>\n" +
      "          <block type=\"lexical_variable_set\" id=\"323\" inline=\"false\">\n" +
      "            <field name=\"VAR\">global reponses</field>\n" +
      "            <value name=\"VALUE\">\n" +
      "              <block type=\"math_add\" id=\"324\" inline=\"true\">\n" +
      "                <mutation items=\"2\"></mutation>\n" +
      "                <value name=\"NUM0\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"325\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"NUM1\">\n" +
      "                  <block type=\"math_number\" id=\"326\">\n" +
      "                    <field name=\"NUM\">1</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"241\" x=\"-966\" y=\"106\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"component_method\" id=\"242\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"243\">\n" +
      "            <field name=\"TEXT\">Il faut trouver le point indiqué pour afficher l'activité !</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"244\">\n" +
      "            <field name=\"TEXT\">Information</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"245\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"641\" x=\"-1222\" y=\"316\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">calculer_Score</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_if\" id=\"642\" inline=\"false\">\n" +
      "        <mutation elseif=\"3\"></mutation>\n" +
      "        <value name=\"IF0\">\n" +
      "          <block type=\"math_compare\" id=\"643\" inline=\"true\">\n" +
      "            <field name=\"OP\">EQ</field>\n" +
      "            <value name=\"A\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"644\">\n" +
      "                <field name=\"VAR\">global reponses</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"B\">\n" +
      "              <block type=\"math_number\" id=\"645\">\n" +
      "                <field name=\"NUM\">0</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO0\">\n" +
      "          <block type=\"controls_if\" id=\"646\" inline=\"false\">\n" +
      "            <mutation else=\"1\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"text_contains\" id=\"647\" inline=\"false\">\n" +
      "                <value name=\"TEXT\">\n" +
      "                  <block type=\"component_set_get\" id=\"648\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"PIECE\">\n" +
      "                  <block type=\"component_set_get\" id=\"649\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"650\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global score</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"math_add\" id=\"651\" inline=\"true\">\n" +
      "                    <mutation items=\"2\"></mutation>\n" +
      "                    <value name=\"NUM0\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"652\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"NUM1\">\n" +
      "                      <block type=\"math_number\" id=\"653\">\n" +
      "                        <field name=\"NUM\">50</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"procedures_callnoreturn\" id=\"654\">\n" +
      "                    <mutation name=\"bonneReponse\"></mutation>\n" +
      "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <statement name=\"ELSE\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"655\">\n" +
      "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <value name=\"IF1\">\n" +
      "          <block type=\"math_compare\" id=\"656\" inline=\"true\">\n" +
      "            <field name=\"OP\">EQ</field>\n" +
      "            <value name=\"A\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"657\">\n" +
      "                <field name=\"VAR\">global reponses</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"B\">\n" +
      "              <block type=\"math_number\" id=\"658\">\n" +
      "                <field name=\"NUM\">1</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO1\">\n" +
      "          <block type=\"controls_if\" id=\"659\" inline=\"false\">\n" +
      "            <mutation else=\"1\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"text_contains\" id=\"660\" inline=\"false\">\n" +
      "                <value name=\"TEXT\">\n" +
      "                  <block type=\"component_set_get\" id=\"661\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"PIECE\">\n" +
      "                  <block type=\"component_set_get\" id=\"662\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"663\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global score</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"math_add\" id=\"664\" inline=\"true\">\n" +
      "                    <mutation items=\"2\"></mutation>\n" +
      "                    <value name=\"NUM0\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"665\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"NUM1\">\n" +
      "                      <block type=\"math_number\" id=\"666\">\n" +
      "                        <field name=\"NUM\">30</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"procedures_callnoreturn\" id=\"667\">\n" +
      "                    <mutation name=\"bonneReponse\"></mutation>\n" +
      "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <statement name=\"ELSE\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"668\">\n" +
      "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <value name=\"IF2\">\n" +
      "          <block type=\"math_compare\" id=\"669\" inline=\"true\">\n" +
      "            <field name=\"OP\">EQ</field>\n" +
      "            <value name=\"A\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"670\">\n" +
      "                <field name=\"VAR\">global reponses</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"B\">\n" +
      "              <block type=\"math_number\" id=\"671\">\n" +
      "                <field name=\"NUM\">2</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO2\">\n" +
      "          <block type=\"controls_if\" id=\"672\" inline=\"false\">\n" +
      "            <mutation else=\"1\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"text_contains\" id=\"673\" inline=\"false\">\n" +
      "                <value name=\"TEXT\">\n" +
      "                  <block type=\"component_set_get\" id=\"674\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte2\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte2</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"PIECE\">\n" +
      "                  <block type=\"component_set_get\" id=\"675\">\n" +
      "                    <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                    <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                    <field name=\"PROP\">Text</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"676\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global score</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"math_add\" id=\"677\" inline=\"true\">\n" +
      "                    <mutation items=\"2\"></mutation>\n" +
      "                    <value name=\"NUM0\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"678\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"NUM1\">\n" +
      "                      <block type=\"math_number\" id=\"679\">\n" +
      "                        <field name=\"NUM\">10</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"procedures_callnoreturn\" id=\"680\">\n" +
      "                    <mutation name=\"bonneReponse\"></mutation>\n" +
      "                    <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <statement name=\"ELSE\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"681\">\n" +
      "                <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <value name=\"IF3\">\n" +
      "          <block type=\"math_compare\" id=\"682\" inline=\"true\">\n" +
      "            <field name=\"OP\">EQ</field>\n" +
      "            <value name=\"A\">\n" +
      "              <block type=\"lexical_variable_get\" id=\"683\">\n" +
      "                <field name=\"VAR\">global reponses</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"B\">\n" +
      "              <block type=\"math_number\" id=\"684\">\n" +
      "                <field name=\"NUM\">3</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO3\">\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"685\">\n" +
      "            <mutation name=\"FermeEcran\"></mutation>\n" +
      "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "            <next>\n" +
      "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"686\" inline=\"false\">\n" +
      "                <value name=\"SCREENNAME\">\n" +
      "                  <block type=\"text\" id=\"687\">\n" +
      "                    <field name=\"TEXT\">Activite_2</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"STARTVALUE\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"688\">\n" +
      "                    <field name=\"VAR\">global score</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>";

  public static String QCM_COMP = "<block type=\"global_declaration\" id=\"35\" inline=\"false\" x=\"-20\" y=\"-259\">\n" +
      "    <field name=\"NAME\">coché</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"logic_false\" id=\"36\">\n" +
      "        <field name=\"BOOL\">FALSE</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"17\" x=\"-1620\" y=\"-350\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">bonneReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"18\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"19\">\n" +
      "            <field name=\"TEXT\">Bravo ! Tu peux passer à l'activité suivante.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"20\">\n" +
      "            <field name=\"TEXT\">Bonne réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"21\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"22\">\n" +
      "            <mutation name=\"FermeEcran\"></mutation>\n" +
      "            <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "            <next>\n" +
      "              <block type=\"controls_openAnotherScreenWithStartValue\" id=\"23\" inline=\"false\">\n" +
      "                <value name=\"SCREENNAME\">\n" +
      "                  <block type=\"text\" id=\"24\">\n" +
      "                    <field name=\"TEXT\">Activite_4</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"STARTVALUE\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"25\">\n" +
      "                    <field name=\"VAR\">global score</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"global_declaration\" id=\"37\" inline=\"false\" x=\"-23\" y=\"-221\">\n" +
      "    <field name=\"NAME\">QCM</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"lists_create_with\" id=\"38\">\n" +
      "        <mutation items=\"0\"></mutation>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"global_declaration\" id=\"39\" inline=\"false\" x=\"-30\" y=\"-186\">\n" +
      "    <field name=\"NAME\">reponses</field>\n" +
      "    <value name=\"VALUE\">\n" +
      "      <block type=\"math_number\" id=\"40\">\n" +
      "        <field name=\"NUM\">0</field>\n" +
      "      </block>\n" +
      "    </value>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"45\" x=\"-1599\" y=\"-65\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">mauvaiseReponse</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"component_method\" id=\"46\" inline=\"false\">\n" +
      "        <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "        <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "        <value name=\"ARG0\">\n" +
      "          <block type=\"text\" id=\"47\">\n" +
      "            <field name=\"TEXT\">Tu es sur ? Essaies encore de trouver la réponse.</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG1\">\n" +
      "          <block type=\"text\" id=\"48\">\n" +
      "            <field name=\"TEXT\">Mauvaise réponse</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <value name=\"ARG2\">\n" +
      "          <block type=\"text\" id=\"49\">\n" +
      "            <field name=\"TEXT\">Ok</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"50\" x=\"-9\" y=\"-118\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"ClueNextButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">ClueNextButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"51\">\n" +
      "        <mutation name=\"displayLearningContent\"></mutation>\n" +
      "        <field name=\"PROCNAME\">displayLearningContent</field>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"component_event\" id=\"71\" x=\"-797\" y=\"-33\">\n" +
      "    <mutation component_type=\"Button\" instance_name=\"TaskValidationButton\" event_name=\"Click\"></mutation>\n" +
      "    <field name=\"COMPONENT_SELECTOR\">TaskValidationButton</field>\n" +
      "    <statement name=\"DO\">\n" +
      "      <block type=\"procedures_callnoreturn\" id=\"72\">\n" +
      "        <mutation name=\"vérifier_coché\"></mutation>\n" +
      "        <field name=\"PROCNAME\">vérifier_coché</field>\n" +
      "        <next>\n" +
      "          <block type=\"procedures_callnoreturn\" id=\"73\">\n" +
      "            <mutation name=\"calculer_Score\"></mutation>\n" +
      "            <field name=\"PROCNAME\">calculer_Score</field>\n" +
      "            <next>\n" +
      "              <block type=\"lexical_variable_set\" id=\"74\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global coché</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"logic_boolean\" id=\"75\">\n" +
      "                    <field name=\"BOOL\">FALSE</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <next>\n" +
      "                  <block type=\"lexical_variable_set\" id=\"76\" inline=\"false\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                    <value name=\"VALUE\">\n" +
      "                      <block type=\"math_add\" id=\"77\" inline=\"true\">\n" +
      "                        <mutation items=\"2\"></mutation>\n" +
      "                        <value name=\"NUM0\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"78\">\n" +
      "                            <field name=\"VAR\">global reponses</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <value name=\"NUM1\">\n" +
      "                          <block type=\"math_number\" id=\"79\">\n" +
      "                            <field name=\"NUM\">1</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </next>\n" +
      "          </block>\n" +
      "        </next>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"88\" x=\"-1036\" y=\"162\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">vérifier_coché</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_forEach\" id=\"89\" inline=\"false\">\n" +
      "        <field name=\"VAR\">élément</field>\n" +
      "        <value name=\"LIST\">\n" +
      "          <block type=\"lexical_variable_get\" id=\"90\">\n" +
      "            <field name=\"VAR\">global QCM</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO\">\n" +
      "          <block type=\"controls_if\" id=\"91\" inline=\"false\">\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"component_set_get\" id=\"92\" inline=\"false\">\n" +
      "                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                <field name=\"PROP\">Checked</field>\n" +
      "                <value name=\"COMPONENT\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"93\">\n" +
      "                    <field name=\"VAR\">élément</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"lexical_variable_set\" id=\"94\" inline=\"false\">\n" +
      "                <field name=\"VAR\">global coché</field>\n" +
      "                <value name=\"VALUE\">\n" +
      "                  <block type=\"logic_boolean\" id=\"95\">\n" +
      "                    <field name=\"BOOL\">TRUE</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>" +
      "<block type=\"procedures_defnoreturn\" id=\"122\" x=\"-1304\" y=\"416\">\n" +
      "    <mutation></mutation>\n" +
      "    <field name=\"NAME\">calculer_Score</field>\n" +
      "    <statement name=\"STACK\">\n" +
      "      <block type=\"controls_if\" id=\"123\" inline=\"false\">\n" +
      "        <mutation else=\"1\"></mutation>\n" +
      "        <value name=\"IF0\">\n" +
      "          <block type=\"lexical_variable_get\" id=\"124\">\n" +
      "            <field name=\"VAR\">global coché</field>\n" +
      "          </block>\n" +
      "        </value>\n" +
      "        <statement name=\"DO0\">\n" +
      "          <block type=\"controls_if\" id=\"125\" inline=\"false\">\n" +
      "            <mutation elseif=\"3\"></mutation>\n" +
      "            <value name=\"IF0\">\n" +
      "              <block type=\"math_compare\" id=\"126\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"127\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"128\">\n" +
      "                    <field name=\"NUM\">0</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO0\">\n" +
      "              <block type=\"controls_forEach\" id=\"129\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"130\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"131\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"132\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"133\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"134\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"135\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"136\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"137\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"138\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"139\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"140\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"141\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"142\">\n" +
      "                                    <field name=\"NUM\">50</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"143\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"144\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF1\">\n" +
      "              <block type=\"math_compare\" id=\"145\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"146\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"147\">\n" +
      "                    <field name=\"NUM\">1</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO1\">\n" +
      "              <block type=\"controls_forEach\" id=\"148\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"149\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"150\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"151\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"152\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"153\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"154\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"155\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"156\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"157\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"158\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"159\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"160\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"161\">\n" +
      "                                    <field name=\"NUM\">30</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"162\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"163\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF2\">\n" +
      "              <block type=\"math_compare\" id=\"164\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"165\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"166\">\n" +
      "                    <field name=\"NUM\">2</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO2\">\n" +
      "              <block type=\"controls_forEach\" id=\"167\" inline=\"false\">\n" +
      "                <field name=\"VAR\">élément</field>\n" +
      "                <value name=\"LIST\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"168\">\n" +
      "                    <field name=\"VAR\">global QCM</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <statement name=\"DO\">\n" +
      "                  <block type=\"controls_if\" id=\"169\" inline=\"false\">\n" +
      "                    <value name=\"IF0\">\n" +
      "                      <block type=\"component_set_get\" id=\"170\" inline=\"false\">\n" +
      "                        <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Checked\" is_generic=\"true\"></mutation>\n" +
      "                        <field name=\"PROP\">Checked</field>\n" +
      "                        <value name=\"COMPONENT\">\n" +
      "                          <block type=\"lexical_variable_get\" id=\"171\">\n" +
      "                            <field name=\"VAR\">élément</field>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <statement name=\"DO0\">\n" +
      "                      <block type=\"controls_if\" id=\"172\" inline=\"false\">\n" +
      "                        <mutation else=\"1\"></mutation>\n" +
      "                        <value name=\"IF0\">\n" +
      "                          <block type=\"logic_compare\" id=\"173\" inline=\"true\">\n" +
      "                            <field name=\"OP\">EQ</field>\n" +
      "                            <value name=\"A\">\n" +
      "                              <block type=\"component_set_get\" id=\"174\" inline=\"false\">\n" +
      "                                <mutation component_type=\"CheckBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"true\"></mutation>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                                <value name=\"COMPONENT\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"175\">\n" +
      "                                    <field name=\"VAR\">élément</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <value name=\"B\">\n" +
      "                              <block type=\"component_set_get\" id=\"176\">\n" +
      "                                <mutation component_type=\"TextBox\" set_or_get=\"get\" property_name=\"Text\" is_generic=\"false\" instance_name=\"Zone_de_texte1\"></mutation>\n" +
      "                                <field name=\"COMPONENT_SELECTOR\">Zone_de_texte1</field>\n" +
      "                                <field name=\"PROP\">Text</field>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                          </block>\n" +
      "                        </value>\n" +
      "                        <statement name=\"DO0\">\n" +
      "                          <block type=\"lexical_variable_set\" id=\"177\" inline=\"false\">\n" +
      "                            <field name=\"VAR\">global score</field>\n" +
      "                            <value name=\"VALUE\">\n" +
      "                              <block type=\"math_add\" id=\"178\" inline=\"true\">\n" +
      "                                <mutation items=\"2\"></mutation>\n" +
      "                                <value name=\"NUM0\">\n" +
      "                                  <block type=\"lexical_variable_get\" id=\"179\">\n" +
      "                                    <field name=\"VAR\">global score</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                                <value name=\"NUM1\">\n" +
      "                                  <block type=\"math_number\" id=\"180\">\n" +
      "                                    <field name=\"NUM\">10</field>\n" +
      "                                  </block>\n" +
      "                                </value>\n" +
      "                              </block>\n" +
      "                            </value>\n" +
      "                            <next>\n" +
      "                              <block type=\"procedures_callnoreturn\" id=\"181\">\n" +
      "                                <mutation name=\"bonneReponse\"></mutation>\n" +
      "                                <field name=\"PROCNAME\">bonneReponse</field>\n" +
      "                              </block>\n" +
      "                            </next>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                        <statement name=\"ELSE\">\n" +
      "                          <block type=\"procedures_callnoreturn\" id=\"182\">\n" +
      "                            <mutation name=\"mauvaiseReponse\"></mutation>\n" +
      "                            <field name=\"PROCNAME\">mauvaiseReponse</field>\n" +
      "                          </block>\n" +
      "                        </statement>\n" +
      "                      </block>\n" +
      "                    </statement>\n" +
      "                  </block>\n" +
      "                </statement>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "            <value name=\"IF3\">\n" +
      "              <block type=\"math_compare\" id=\"183\" inline=\"true\">\n" +
      "                <field name=\"OP\">EQ</field>\n" +
      "                <value name=\"A\">\n" +
      "                  <block type=\"lexical_variable_get\" id=\"184\">\n" +
      "                    <field name=\"VAR\">global reponses</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "                <value name=\"B\">\n" +
      "                  <block type=\"math_number\" id=\"185\">\n" +
      "                    <field name=\"NUM\">3</field>\n" +
      "                  </block>\n" +
      "                </value>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <statement name=\"DO3\">\n" +
      "              <block type=\"procedures_callnoreturn\" id=\"186\">\n" +
      "                <mutation name=\"FermeEcran\"></mutation>\n" +
      "                <field name=\"PROCNAME\">FermeEcran</field>\n" +
      "                <next>\n" +
      "                  <block type=\"controls_openAnotherScreenWithStartValue\" id=\"187\" inline=\"false\">\n" +
      "                    <value name=\"SCREENNAME\">\n" +
      "                      <block type=\"text\" id=\"188\">\n" +
      "                        <field name=\"TEXT\">Activite_4</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                    <value name=\"STARTVALUE\">\n" +
      "                      <block type=\"lexical_variable_get\" id=\"189\">\n" +
      "                        <field name=\"VAR\">global score</field>\n" +
      "                      </block>\n" +
      "                    </value>\n" +
      "                  </block>\n" +
      "                </next>\n" +
      "              </block>\n" +
      "            </statement>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "        <statement name=\"ELSE\">\n" +
      "          <block type=\"component_method\" id=\"190\" inline=\"false\">\n" +
      "            <mutation component_type=\"Notifier\" method_name=\"ShowMessageDialog\" is_generic=\"false\" instance_name=\"Notificateur1\"></mutation>\n" +
      "            <field name=\"COMPONENT_SELECTOR\">Notificateur1</field>\n" +
      "            <value name=\"ARG0\">\n" +
      "              <block type=\"text\" id=\"191\">\n" +
      "                <field name=\"TEXT\">Il faut choisir une réponse pour passer à l'activité suivante !</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"ARG1\">\n" +
      "              <block type=\"text\" id=\"192\">\n" +
      "                <field name=\"TEXT\">Information</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "            <value name=\"ARG2\">\n" +
      "              <block type=\"text\" id=\"193\">\n" +
      "                <field name=\"TEXT\">Ok</field>\n" +
      "              </block>\n" +
      "            </value>\n" +
      "          </block>\n" +
      "        </statement>\n" +
      "      </block>\n" +
      "    </statement>\n" +
      "  </block>";
}
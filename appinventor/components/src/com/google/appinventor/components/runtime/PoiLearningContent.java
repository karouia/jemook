// -*- mode: java; c-basic-offset: 2; -*-
// Copyright 2009-2011 Google, All Rights reserved
// Copyright 2011-2012 MIT, All rights reserved
// Released under the Apache License, Version 2.0
// http://www.apache.org/licenses/LICENSE-2.0

package com.google.appinventor.components.runtime;

import com.google.appinventor.components.annotations.*;
import com.google.appinventor.components.common.ComponentCategory;
import com.google.appinventor.components.common.ComponentConstants;
import com.google.appinventor.components.common.PropertyTypeConstants;
import com.google.appinventor.components.common.YaVersion;

/**
 * A vertical arrangement of components
 * @author sharon@google.com (Sharon Perl)
 *
 */

@DesignerComponent(version = YaVersion.VERTICALARRANGEMENT_COMPONENT_VERSION,
    description = "<p>A formatting element in which to place components " +
    "that should be displayed one below another.  (The first child component " +
    "is stored on top, the second beneath it, etc.)  If you wish to have " +
    "components displayed next to one another, use " +
    "<code>HorizontalArrangement</code> instead.</p>",
    category = ComponentCategory.GLOBALRESOURCES)
@SimpleObject
public class PoiLearningContent extends HVArrangement {

  private String answer;
  private String successMessage;
  private String failMessage;
  private String successResult;
  private String failResult;

  public PoiLearningContent(ComponentContainer container) {
    super(container, ComponentConstants.LAYOUT_ORIENTATION_VERTICAL,
        ComponentConstants.NONSCROLLABLE_ARRANGEMENT);
  }

  /**
   * Getter pour la bonne réponse
   *
   * @return  bonne réponse
   */
  @SimpleProperty
  public String Answer() {
    return answer;
  }
  /**
   * Setter pour la bonne réponse
   *
   */
  @DesignerProperty(editorType = PropertyTypeConstants.PROPERTY_TYPE_TEXT,
      defaultValue = "")
  @SimpleProperty(category = PropertyCategory.APPEARANCE)
  public void Answer(String answer) {
    this.answer=answer;
  }

  // Message de succès
  @SimpleProperty( category = PropertyCategory.APPEARANCE,userVisible = false)
  public String AnswerSuccesMsg() {
    return successMessage;
  }
  @DesignerProperty(editorType = PropertyTypeConstants.PROPERTY_TYPE_TEXTAREA,
      defaultValue = "Bravo ! ...")
  @SimpleProperty(description = "Hello", userVisible = false)
  public void AnswerSuccesMsg(String answer) {
    this.successMessage = answer;
  }

  // Message d'échec
  @SimpleProperty( category = PropertyCategory.APPEARANCE,userVisible = false)
  public String FailMsg() {
    return failMessage;
  }
  @DesignerProperty(editorType = PropertyTypeConstants.PROPERTY_TYPE_TEXTAREA,
      defaultValue = "Dommage ...")
  @SimpleProperty(description = "Hello", userVisible = false)
  public void FailMsg(String answer) {
    this.failMessage = answer;
  }

  // Résultat au succès
  @SimpleProperty( category = PropertyCategory.APPEARANCE,userVisible = false)
  public String ResultForGoodAnswer() {
    return successResult;
  }
  @DesignerProperty(editorType = PropertyTypeConstants.PROPERTY_TYPE_TEXT,
      defaultValue = "Activité suivante")
  @SimpleProperty(description = "Hello", userVisible = false)
  public void ResultForGoodAnswer(String answer) {
    this.successResult=answer;
  }

  // Résultat à l'échec
  @SimpleProperty( category = PropertyCategory.APPEARANCE,userVisible = false)
  public String ResultToBadAnswer() {
    return failResult;
  }
  @DesignerProperty(editorType = PropertyTypeConstants.PROPERTY_TYPE_TEXT,
      defaultValue = "Rien")
  @SimpleProperty(description = "Hello", userVisible = false)
  public void ResultToBadAnswer(String answer) {
    this.failResult = answer;
  }
}
